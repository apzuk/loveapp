var sD = {
    cities: {},
    loadedUser: {},
};

sD.initApp = function () {
    window.WebSocket = false;

    var canRun = sD.canRun();

    if (canRun) {

        $("#no-support").remove();

        sD.initTab();
        sD.initPopupLink();
        sD.textareaLimit();
        sD.activeToolTip();
        sD.styler();
        sD.settingsDlg();
        sD.jsp();
        sD.loadLeader();
        sD.notificationLoad();
        sD.qBox();
        sD.photoUploader();
        sD.choose();
        sD.awardToolTip();
        sD.awardNotifications();
        sD.suggestionUsersView();

        $(document).on("click", ".flag", function () {
            $(".flag.active").removeClass("active");
            $(this).addClass("active");

            var country_id = $(this).find('input').val();
            sD.loadCities(country_id);
        });

        $(document).on('socketReady', function () {
            cf.init();
        });

        $(document).on('click', 'a[href=#]', function (event) {
            event.preventDefault();
        });
        $(document).on('click', '.ui-state-disabled', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
        });

        $("<div id='confirmation-dialog' class='d_none'></div>").appendTo($('#wrapper'));

        setInterval(sD.updateSuggestedUsers, 1000 * 60 * 15);
    } else {
        $(".p_loading, #wrapper").remove();
        $("#no-support").show();
    }
};

sD.showAward = function (index) {
    $.post('/api/award/getAwardView', {index: index}, function (data) {
        $(".award-container").html(data.htmlContent);
    });

    $("#award-dialog").dialog('open');
};

sD.sendMoney = function () {
    $("#sendMoney").dialog("open");
};

sD.awardToolTip = function () {
    $('.award').each(function () {
        $(this).
            tooltip({
                content: function () {
                    return $(this).prop('title');
                },
                position: {
                    at: 'left bottom',
                    my: 'left-320 bottom+1',
                    of: $(this)
                },
                tooltipClass: 'award-tooltip'
            });
    });
};

sD.awardNotifications = function () {

};

sD.canRun = function () {
    var canRun = true;

    if (jQuery == undefined || $ == undefined) {
        canRun = false;
    }
    for (var feature in Modernizr) {
        if (typeof Modernizr[feature] === "boolean" && Modernizr[feature] == false) {
            canRun = false;
            break;
        }
    }

    return canRun;
};

sD.photoUploader = function () {
    $("#fromPC").fileapi({
        url: '/api/IM/uploadPhoto',
        autoUpload: true,
        multiple: false,
        accept: 'image/*',
        data: {
            'csrf-token': $("meta[name=csrf-token]").attr('content')
        },
        onSelect: function () {
            $(".uploading-process").show();
        },
        onComplete: function (evt, uiEvt) {
            IM.photoUploaded(uiEvt);

            $(".uploading-process").hide();
        },
        elements: {
            progress: '#upload-photo .js-progress'
        }
    });
};

sD.editProfileForm = function () {
    $("#form").show();
    $("#patron").hide();
};

sD.qBox = function () {
    $("#box1, #box2").each(function () {

        $(this)
            .dialog({
                autoOpen: false,
                width: 351,
                height: 'auto'
            })
            .styledDialog({
                autoOpen: false,
                addClass: $(this).data('direction'),
                zIndex: 105,
                gender: $(this).data('gender'),
                buttons: {
                    button: {
                        value: "Ответить",
                        selector: ".answer"
                    }
                }
            }
        );
    });
};

sD.loadUser = function (user_id, callback) {
    return $.post('/api/user/get', {
        user_id: user_id,
        responseType: 'json'
    }, function (data) {
        callback(data.user);
    });
};

sD.notificationLoad = function () {
    $('.nfs').data('offset', 1);
    $('.nfs').bind(
        'jsp-scroll-y',
        function (event, scrollPositionY, isAtTop, isAtBottom) {
            if (!isAtBottom || $(this).data('inProcess') == 1) {
                return;
            }

            var $root = $(this),
                offset = $(this).data('offset'),
                view = $(this).data('jsp').getContentPane();

            if (!offset) {
                return;
            }

            $root.data('inProcess', 1);

            $.post('/api/notification/load', {
                offset: offset
            }, function (response) {

                $root.data('inProcess', 0);

                if ($.trim(response.htmlContent)) {
                    view.append(response.htmlContent);
                    $root.data('offset', ++offset);
                    $root.data('jsp').reinitialise();
                } else {
                    $root.data('offset', 0);
                }
            });
        }
    );
};

sD.jsp = function () {
    $('.jsp').jScrollPane({
        reinitialise: true
    });
};

sD.mosaic = function () {
    $('.mosaic-block').mosaic({
        animation: 'slide',
        hover_y: '-25px'
    })
};

sD.settingsDlg = function () {
    $(".colors > div").on("click", function () {
        $(".colors > div.active").removeClass("active");
        $(this).addClass("active");

        var index = $(this).attr('class').replace(/\D+/g, '');
        $(".settings .model, .settings .model > span, .settings_tt, .sd-avatar").attr("class", function (i, cls) {
            return cls.replace(/skin_\d+/, 'skin_' + index);
        })
    });

    $(".colors > div.active").trigger('click');
};

sD.styler = function () {
    $('input[type!=file]').styler();
    //$('input, select').addClass($(this).data('classes'));
};

sD.initTab = function () {
    var self = $("#menu");

    self.find(".menu_b").on("click", function () {
        var currentItem = $(".menu_b.current"), index = currentItem.index();
        var blurEventName = "MenuItem" + (index + 1) + "Blur";
        var eventName = "MenuItem" + ($(this).index() + 1) + "Click";

        currentItem.removeClass("current").trigger(blurEventName, ["Custom", "Event"]);
        $(".g_p").eq(index).addClass("d_none");

        $(this).addClass("current").trigger(eventName, ["Custom", "Event"]);
        $(".g_p").eq($(this).index()).removeClass("d_none");

        sD.jsp();
    });

    $(".users-rating .item").on("click", function () {
        if ($('.g_p2').hasClass("loading")) {
            return;
        }

        $(".users-rating .item.active").removeClass('active');
        $(this).addClass("active");

        $(".g_p2").addClass("loading");

        sD.loadRating($(this).index());
    });
    $(".users-rating .item").first().trigger("click");
};

sD.loadRating = function (index) {
    var offset = 0;

    $(".g_p2 .list .content").html("");
    return $.post('/api/user/ratingList', {index: index}, function (data) {
        $(".g_p2").removeClass("loading");
        $(".g_p2 .list .content").html(data.htmlContent);
        $(".others.jsp").jScrollPane();
        $(".others.jsp").data("offset", 1);
        offset++;

        $('.others.jsp').bind('jsp-scroll-y', function (event, scrollPositionY, isAtTop, isAtBottom) {
            if ($(this).data('jsp').getPercentScrolledY()  >= 0.8) {
                $.post('/api/user/ratingList', {
                    index: index,
                    offset: offset
                }, function (data) {
                    offset++;
                    $(".others.jsp").data("jsp").getContentPane().append(data.htmlContent);
                    $(".others.jsp").data("jsp").reinitialise();
                });
            }
        });
    });
};

sD.initDialog = function () {
    $(".jq-dialog").each(function () {
        var d = $(this), w = d.data("width"), h = d.data("height");
        d.dialog({
            width: w,
            height: h,
            resizable: false,
        });
    });
};

sD.initPopupLink = function () {
    $(document).on("click", ".popup_link", function () {
        if ($(this).hasClass('ui-state-disabled')) {
            return false;
        }

        var d = $(this).data("dialog");

        if ($(d).length > 0) {
            $(d).dialog("open");
        }
    });
};

sD.closeAllDialogs = function () {
    $(".ui-dialog-content").dialog("close");
};

sD.closeDialog = function (p) {
    if (typeof p == "string") {
        return $(p).dialog("close")
    }

    return $(p).closest('.ui-dialog-content').dialog('close');
};

sD.textareaLimit = function () {
    $(".limit").each(function () {
        var textarea = $(this), limit = textarea.data('limit');

        if (!limit) {
            return;
        }

        textarea.limit(limit, textarea.siblings('.charsLeft'));
        textarea.removeAttr('data-limit')
    });
};

sD.activeToolTip = function () {
    $('.hasToolTip').tooltip({
            position: {
                my: "center top+23", at: "center center"
            }
        }
    );
};

sD.insertToUserList = function (data, callback) {
    if (!data || !data.user || !data.ribbon) {
        return callback();
    }

    if ($(".ribbon_" + data.ribbon.id).length > 0) {
        return callback();
    }

    var s = $("#userRibbon").tmpl($.extend({}, data.user, data.ribbon));

    s.prependTo($('.block2 .extra-big'));
    $('.block3:first').animate({marginLeft: 0}, function () {
        $('.block3:eq(15)').remove();

        $(this).removeAttr('style');
        $(this).tooltip({
            position: {
                at: 'center top',
                my: 'center bottom-21',
                of: $(this)
            },
            tooltipClass: 'skin_' + data.user.skin + ' skin ' + data.user.gender + ' settings_tt'
        });

        return callback();
    });
};

sD.becomeLeader = function () {
    return $.post(
        '/api/leader/set',
        {
            message: $("#become-leader-dialog textarea").val()
        },
        function (response) {
            //$(document).trigger('leaderChanged', [response]);
            $("#become-leader-dialog textarea").val('');
            sD.closeDialog("#become-leader-dialog");

        }
    );
};

sD.toRibbon = function () {
    return $.post(
        '/api/ribbon/set',
        {
            text: $("#to-users-list-confirmation textarea").val()
        },
        function (response) {
            sD.closeDialog("#to-users-list-confirmation");
            $("#to-users-list-confirmation textarea").val('');
        }
    );
};

sD.loadLeader = function () {
    return $.post(
        '/api/leader/get',
        function (response) {
            $(document).trigger('leaderChanged', [response]);
        }
    )
};

sD.loadRibbon = function () {
    return $.post(
        '/api/ribbon/load',
        {},
        function (response) {
            var ribbons = response.ribbons.reverse();
            if (ribbons.length == 0) {
                return;
            }

            var i = 0;
            var ___iii = function () {
                var ribbon = ribbons[i];
                if (ribbon == 'undefined' || i > 15) {
                    return;
                }
                sD.insertToUserList(ribbon, function () {
                    i++;
                    ___iii();
                });
            };
            ___iii();
        }
    );
};

sD.profile = function (user_id) {
    var loading = $('<div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>');
    $("#profile").html(loading).dialog('open');

    return $.post('/api/user/get',
        {
            user_id: user_id
        },
        function (response) {
            $("#profile").dialog('option', 'title', response.title);
            $('#profile').html(response.htmlContent);

            sD.loadedUser = response.user;

            sD.activeToolTip();
            sD.jsp();
            sD.mosaic();
            sD.styler();

            var country_id = $("#userCountryId").val();
            $(".flag.country" + country_id).trigger("click");

            $(".cityIdSelected").select2({
                placeholder: "Введите название города"
            });
            $(".select2-chosen").text(cityname);
            $(".select2-drop").addClass(response.gender);
        }
    );
};

sD.becomeVip = function (mode) {
    var user_id = sD.loadedUser ? sD.loadedUser.user_id : null;

    return $.post(
        '/api/user/becomeVip',
        {
            mode: mode,
            user_id: user_id
        },
        function (response) {
            if (response.result) {
                $(document).trigger('userBecameVip', [response]);
            }

            sD.closeDialog('#become-vip');
        }
    );
};

sD.changeUserSkin = function (user_id, skin) {
    $('.user_' + user_id).each(function () {
        $(this).attr("class", function (i, cls) {
            return cls.replace(/skin_\d/, 'skin_' + skin);
        });

        $(this).find('[class*=skin]').attr("class", function (i, cls) {
            return cls.replace(/skin_\d/, 'skin_' + skin);
        });
    });
};

sD.setUserSkin = function (skin) {
    return $.post(
        '/api/user/setSkin',
        {
            skin: skin
        },
        function (response) {
            if (response.result) {
                sD.changeUserSkin(response.user.user_id, response.user.skin);
            }
        }
    );
};

sD.changeNotificationStatus = function (n_id, self) {
    if (!$(self).hasClass("new")) {
        return;
    }

    return $.post(
        '/api/notification/setGotStatus',
        {
            n_id: n_id
        },
        function (response) {
            $(self).removeClass('new').addClass('item');

            if (response.result) {
                sD.downgradeNum(2);
            }
        }
    );
};

sD.downgradeNum = function (num) {
    var n = $(".num_" + num), count = $(".nfs .n-item.new").length;
    n.removeClass('count' + (count + 1));
    n.text(count).addClass('count' + count);
};

sD.upgradeNum = function (num) {
    var n = $(".num_" + num), count = n.text().trim();
    n.removeClass('count' + count);
    n.text(++count).addClass('count' + count);
};

sD.deleteNotification = function (id, self) {
    sD.confirm("Вы действительно хотите удалить эту запись?", function () {
        return $.post(
            '/api/notification/delete',
            {
                n_id: id
            },
            function (response) {
                if (!response.result) {
                    return;
                }

                $(self).parent().slideUp(function () {
                    $(this).remove();
                })
            }
        );
    });
};

sD.confirm = function (text, callback1, callback2) {
    $("#confirmation-dialog").find('p').remove();
    $('<p>' + text + '</p>').appendTo($("#confirmation-dialog"));

    $("#confirmation-dialog")
        .dialog({
            title: 'Подтверждение',
            modal: true,
            draggable: false,
            resizeable: false,
            width: 350,
            height: 'auto',
            buttons: {
                Да: function () {
                    if (callback1) callback1();
                    $("#confirmation-dialog").dialog('close');
                },
                Нет: function () {
                    if (callback2) callback2();
                    $("#confirmation-dialog").dialog('close');
                }
            }
        })
        .dialog('open');
};

sD.updateSuggestedUsers = function () {
    return $.post('/api/user/suggested', {}, function (data) {
        $(".user-info-block").remove();

        $("#suggested-users").replaceWith(data.htmlContent);
    });
};

sD.like = function (user_id, self) {
    if ($(self).hasClass('active')) {
        return false;
    }

    return $.post(
        '/api/user/like',
        {
            target_user_id: user_id
        },
        function (response) {
            if (response.result) {
                $(self).removeAttr('title').attr('title', 'Вы уже проявили симпатию').addClass('active')
            }
        }
    );
};

sD.kiss = function (user_id, self) {
    return $.post('/api/user/kiss', {target_user_id: user_id}, function (data) {
        if (data.result) {
            $("#profile .kisses span").text(data.kisses);
            sD.effect2($("#profile .kisses span"));
        }
    });
};

sD.becomePatron = function (user_id, self) {
    return $.post('/api/user/becomeSponsor', {
        target_user_id: user_id
    }, function (response) {
        $(self).remove();
    });
};

sD.choose = function (choose) {
    return $.post('/api/user/choose', {
        choose: choose,
        user_id: $(".block5").data('userid')
    }, function (data) {
        $("#randomUserTmpl").tmpl(data.user).appendTo($(".block5").empty());
        $(".block5").data('userid', data.user.user_id);
    });
};

sD.updateNotifications = function () {
    return $.post('/api/notification/getUpdates', {}, function (data) {
        if (data.count > 0) {
            return;
        }

        $(".nfs .n-item.new").remove();

        var api = $(".jsp.nfs").data('jsp');
        api.getContentPane().prepend(data.htmlContent);

        // $(document).trigger('notificationUpgrade', [{item: 3, count: data.count}]);
    });
};

sD.toggleVisibility = function () {
    $.post('/api/user/toggleVisibility', {}, function (data) {

        if (data.visibility == 1) {
            $(".userVisibility").addClass('active');
        } else {
            $(".userVisibility").removeClass('active');
        }
    });
};

sD.effect1 = function (selector) {
    if (!selector) selector = "#effect1";

    $(selector).removeClass('d_none').addClass('effect1');
    setTimeout(function () {
        $(selector).removeClass('effect1').addClass('d_none')
    }, 450)
};
sD.effect2 = function a($el) {
    $el.addClass("pulse");
    setTimeout(function () {
        $el.removeClass("pulse");
    }, 500);
};

sD.suggestionUsersView = function () {
    $(document).on("click", "#suggested-users .item", function (data) {
        var user_id = $(this).data('id');

        showUser(user_id);
    });

    function showUser(user_id) {
        $.post('/api/user/get', {
            responseType: 'json',
            user_id: user_id
        }, function (data) {
            $(".user-info-block").remove();

            var ss = $("#suggested-users");
            $("#userInfoBlock").tmpl(data.user).insertAfter(ss);
        });
    }
};

sD.loadCities = function (country_id, term) {
    return $.post('/api/system/getCities', {
        country_id: country_id,
        term: term
    }, function (data) {
        $("select.cityIdSelected").empty();
        $.each(data.data, function (key, item) {
            $("select.cityIdSelected").append("<option value='" + item.id + "'>" + item.value + "</option>");
        });
    });
};

sD.setVKAvatar = function () {
    return $.post('/api/user/setvkavatar', {}, function (data) {
        $("#profile .mosaic-backdrop img").attr("src", data.user.avatar_orig);
    });
};

sD.sendMoneyTo = function () {
    return $.post("/api/user/sendMoney", {
        count: $("#moneyCount").val(),
        target_user_id: sD.loadedUser.user_id
    }, function (data) {
        $("#sendMoney").dialog("close");
        $("#moneyCount").val(10);
    });
};

$(function () {
    sD.initApp();
});

function log(v) {
    console.log(v);
}