/**
 * Global settings/functions
 */
var global = {

    init: function () {
        this.settings();
    },

    settings: function () {

        $.ajaxSetup({
            type: "POST",
            dataType: "json",
        });

        $(document).ajaxError(function (event, jqxhr, settings, exception) {
            console.log(jqxhr.responseText);
        });

        // Adding a csrf-token in the request
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            if ((originalOptions.type !== undefined &&
                originalOptions.type.toLowerCase() == 'post') ||
                (options.type !== undefined && options.type.toLowerCase() == 'post')) {
                var data = originalOptions.data;

                if (originalOptions.data !== undefined) {
                    if (Object.prototype.toString.call(originalOptions.data) === '[object String]') {
                        data = $.deparam(originalOptions.data);
                    }
                } else {
                    data = {};
                }

                try {
                    options.data = $.param($.extend(data, {
                        'csrf-token': $('meta[name="csrf-token"]').attr('content')
                    }));

                } catch (e) {

                }
            }
        });
    }
}

/**
 * Global initialization
 */
$(function () {
    global.init();
});