var em = {
    listen: function () {
        // Settings dialog opened
        em.settings();

        // Components loading finished
        em.loadingFinished();

        // Leader changed
        em.leaderChanged();

        //
        em.tabs();

        // User becomes VIP
        em.userVIPStatus();

        // New message
        em.messages();

        // New notification
        em.notifications();

        // Update awards
        em.updateAward();

        // User updates
        em.userUpdates();

        // Account viewed
        em.accountViewed();

        // Connection with server lost
        em.connectionLost();
    }
};

em.connectionLost = function () {
    $(document).on('connectionLost', function () {
        $('.nc').addClass('disconnected').removeClass('nc');

        $('#game_page').removeClass('show');
        if ($("#box1").length > 0) {
            $("#box1").dialog('close');
        }
        if ($("#box2").length > 0) {
            $("#box2").dialog('close');
        }
    });
};

em.onDialogOpen = function () {
    var dialogs = $(".ui-dialog-content");

    dialogs.each(function () {
        $('#' + this.id).on('dialogopen', function (event, ui) {
            if ($("#game_page").hasClass('show')) {
                $(".ui-widget-overlay").css("z-index", '106');
                $(this).parents('.ui-dialog').css("z-index", '106')
            }
        });
    });

    $("#profile").on("dialogclose", function (event, ui) {
        sD.loadedUser = null;
    });
    $("#become-vip").on("dialogopen", function (event, ui) {
        var u = {
            user_id: user_id,
            avatar: avatar,
            gender: gender,
            skin: skin
        };

        if (sD.loadedUser) {
            u = {
                user_id: sD.loadedUser.user_id,
                avatar: sD.loadedUser.avatar,
                gender: sD.loadedUser.gender,
                skin: sD.loadedUser.skin
            };
        }

        var d = $("#become-vip");
        d.parent().removeClass('male').removeClass('female').addClass(u.gender);
        d.find(".sd-avatar")
            .removeClass('male')
            .removeClass('female')
            .addClass(u.gender)
            .attr("class", function (i, cls) {
                return cls.replace(/skin_\d+/, 'skin_' + u.skin);
            });

        d.find(".sd-avatar img").attr("src", u.avatar);
    });
};

em.accountViewed = function () {
    $(document).on('accountViewed', function (data) {
        console.log(data);
        var s = $('p.username_label span'), views = s.text();
        s.text('(' + (++views) + ')').removeClass('d_none');
    })
};

em.userUpdates = function () {
    $(document).on('userUpdates', function (event, data) {
        // Balance
        var $balance = $("#my_balance"),
            $rating = $("#rating"),
            balance = parseInt($balance.text()),
            rating = parseInt($rating.text());

        balance = balance ? balance : 0;
        rating = rating ? rating : 0;

        if (balance != data.balance) {
            $balance.text(data.balance);
            a($(".val2"));
        }
        if (rating != data.rating) {
            $rating.text(data.rating);
            a($(".val1"));
        }

        function a($el) {
            $el.addClass("pulse");
            setTimeout(function () {
                $el.removeClass("pulse");
            }, 500);
        }

        // Rating
    });
};

em.updateAward = function () {
    $(document).on('updateAward', function (event, data) {
        console.log(data);
        var $award = $(".aw" + data.index);
        $award.find(".award").removeAttr('title').attr('title', data.tooltipTitle);
        $award.find(".percentage").text(data.percentage + "%");
        $award.find(".process .process_bar").css("width", data.percentage + "%");
    });
};

em.notifications = function () {
    $(document).on('notificationUpgrade', function (event, data) {
        sD.updateNotifications();

        var
            item = data.item ? data.item : 3,
            $item3 = $(".mi_item_" + item),
            num = data.count ? data.count : parseInt($item3.find('.num').text().trim());

        $item3.addClass('shake shake-hard shake-constant');

        $('.count' + num, $item3).removeClass('count' + num).addClass('count' + (++num));
        $item3.find('.num').text(num).show();

        setTimeout(function () {
            $item3.removeClass('shake shake-hard shake-constant')
        }, 1000);
    });

    $(document).on('notificationDowngrade', function (event) {
        var
            item = data.item ? data.item : 3,
            $item3 = $(".mi_item_" + item),
            num = parseInt($item3.find('.num').text().trim());

        $('.count' + num, $item3).removeClass('count' + num).addClass('count' + (--num >= 0 ? num : 0));
        $item3.find('.num').fadeOut(function () {
            if (num <= 0) num = 0;
            var self = $(this).text(num);
            if (num > 0) {
                self.fadeIn();
            }
        });
    });
};

em.messages = function () {
    $(document).on('newMessage', function (event, data) {

        if (data.action) {
            return IM[data.action](data);
        }

        return IM.loadCompanion(data.companionId, function () {
            IM.newMessage(data);
        });
    });
};

em.userVIPStatus = function () {
    $(document).on('userBecameVip', function (event, data) {
        if (data.result && data.self) {
            $(".vip-btn").addClass('d_none');

            $(".user_" + data.user.user_id).addClass("vip");
            $(".vip-end-block a").text(data.user.vip_end).parent().parent().removeClass('d_none');

            $(".ico-upload").removeClass("d_none");

            sD.changeUserSkin(data.user.user_id, data.user.skin);
        }
    });

    $(document).on('userVipExpired', function (event, data) {
        if (data.self) {
            $(".vip-btn").removeClass('d_none');

            $(".user_" + data.user.user_id).removeClass("vip");
            $(".vip-end-block a").text('').parent().parent().addClass('d_none');

            sD.changeUserSkin(data.user.user_id, 1);
        }
    })
};

em.leaderChanged = function () {
    $(document).on('leaderChanged', function (event, data) {

        if (data.only_bid) {
            return $(".bid").fadeOut(function () {
                $(this).text(data.current_bid).fadeIn();
            });
        }

        $("#userBlockTmpl").tmpl(data).css("top", "-150px").prependTo($("#leader-dialog"));
        $(".bid").text(data.current_bid);
        $("#leader-dialog").find('.bubblingG').remove();
        $("#leader-dialog > div").removeClass("d_none");

        $("#leader-dialog").parent().removeClass('male').removeClass('female').addClass(data.gender);

        if ($("#leader-dialog .user-block").length > 1) {
            $("#leader-dialog .user-block").last().css({top: -127}).animate({left: -150}, 400, function () {
                $(this).remove();
            });
        }
        $("#leader-dialog .user-block").first().animate({top: 0}, 550, function () {
            $(this).removeAttr("style");
        });

        $(".comment-box-1 p").fadeOut(function () {
            $(this).text(data.message).fadeIn();
        });

        if (!data.result) {
            $("#leader-dialog").addClass("no-leader");
            return;
        }

        $("#leader-dialog").removeClass("no-leader");

    });
};

em.settings = function () {
    $("#settings")
        .on('dialogopen', function (event, ui) {
            $("#settings .point")
                .one('mousemove', function (event) {
                    event.stopImmediatePropagation();
                })
                .tooltip({
                    tooltipClass: 'skin_' + skin + ' skin ' + gender + ' settings_tt'
                })
                .tooltip('open');
        })
        .on('dialogclose', function () {
            $("#settings .point").trigger('mouseout')
        });
};

em.loadingFinished = function () {
    $(document).on('loadingFinished', function () {
        $(".p_loading").remove();
        $("body").removeClass('p_loading_bgr');

        sD.loadRibbon();
        $(".awards.my").addClass('show');
    })
};

em.tabs = function () {
    $(document)
        .on("MenuItem1Blur", function () {
            if (game.model) {
                $("#game_page").removeClass('show');
                $("#box1, #box2").parents('.ui-dialog').addClass('top_hidden');
                $(".mi_item_1 time").fadeIn();
            }
            $("#question-dialog, #leader-dialog").dialog("close");
        })
        .on("MenuItem1Click", function () {
            var ms = 0;

            if (game.model) {
                $("#game_page").addClass('show');
                setTimeout(function () {
                    $("#box1, #box2").parents('.ui-dialog').removeClass('top_hidden');
                }, 280);

                $(".mi_item_1 time").fadeOut();
            }
            $(".awards").addClass('show');

            setTimeout(function () {
                $("#question-dialog, #leader-dialog").dialog("open");
            }, ms);
        });

    $(document)
        .on("MenuItem2Click", function () {
            $(".suggestion-users").addClass('show');

            if ($("#messages .sidebar figure.active").length == 0) {
                $("#messages .sidebar figure").eq(0).trigger('click');
            }
        })
        .on("MenuItem2Blur", function () {
            $(".suggestion-users").removeClass('show');
        });

    $(document)
        .on("MenuItem4Click", function () {
            $(".suggestion-users").addClass('show');

            if ($("#messages .sidebar figure.active").length == 0) {
                $("#messages .sidebar figure").eq(0).trigger('click');
            }
        })
        .on("MenuItem4Blur", function () {
            $(".suggestion-users").removeClass('show');
        });

    $(document)
        .on("MenuItem4Click", function () {
            $(".users-rating aside").addClass("active");
            $(".users-rating .tab").addClass("active");
        })
        .on("MenuItem4Blur", function () {
            $(".users-rating aside").removeClass("active");
            $(".users-rating .tab").removeClass("active");
        });

    $(document)
        .on("MenuItem2Click MenuItem3Click MenuItem4Click", function () {
            $(".awards").removeClass('show');
        });
};

$(function () {
    em.listen();
});