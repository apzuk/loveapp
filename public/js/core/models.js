var user = Backbone.Model.extend({
    gender: function (t) {
        if (this.gender == 2 || this.gender.toLowerCase() == 'male') {
            return !t ? 'male' : 'female';
        }

        if (this.gender == 1 || this.gender.toLowerCase() == 'female') {
            return !t ? 'female' : 'male';
        }

        return 'male';
    }
});

var leader = Backbone.Model.extend({
    user: new User()
});