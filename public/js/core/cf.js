var cf = {
    city_id: null,

    init: function () {
        // Subscribe to leader change channel
        if (!cf.city_id) {
            cf.city_id = $("#city_id").val();
        }
        cf.cityLeaderChannel(cf.city_id);

        // Subscribe to ribbon change channel
        cf.ribbonChannel();

        // new messages
        cf.newMessage();

        // user updates
        cf.userUpdates();
    }
};

cf.userUpdates = function () {
    var subscription = socket.subscribe("#userUpdates" + user_id, function (message) {
        var data = message.data;
console.log(data);
        if (data.eventToFire) {
            return $(document).trigger(data.eventToFire, [message.data]);
        }

        $(document).trigger("userUpdates", [message.data]);
    });
};

cf.newMessage = function () {
    var subscription = socket.subscribe("#messageChannel" + user_id, function (message) {
        $(document).trigger("newMessage", [message.data]);
    });
};

cf.cityLeaderChannel = function (city_id) {
    // Subscribe leader change channel
    var subscription = socket.subscribe("city" + city_id + "Leader", function (message) {
        $(document).trigger("leaderChanged", [message.data]);
    });
};

cf.ribbonChannel = function () {
    var subscription = socket.subscribe('ribbon', function (message) {
        message.data.user = $.parseJSON(message.data.user);
        message.data.ribbon = $.parseJSON(message.data.ribbon);

        console.log(message.data);
        sD.insertToUserList(message.data);
    })
};