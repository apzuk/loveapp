var game = {
    model: null,
    counterDownInterval: null,

    /**
     * Start playing
     * @param self
     * @returns {*}
     */
    play: function (self) {
        // If is waiting for playing - unplay!
        if ($(self).hasClass('playing')) {
            return game.unplay(self);
        }
        game._initialPlaying();

        // Send data to server
        return socket.send({
            method: 'play',
            module: 'game',
            question: $("#question-dialog .question").val()
        }, function (data) {
        });
    },

    /**
     * Unplay
     * @param self
     * @returns {*}
     */
    unplay: function (self) {
        return socket.send({
            method: 'unplay',
            module: 'game'
        }, function (data) {
            $(self).removeClass('playing').text("Задать вопрос");
            $('.q-box .overlay').addClass('d_none');

            game.model = null;

            // Stop time counter
            $("#runner").runner('stop');
        });
    },

    /**
     * The game starts playing
     * @param data
     */
    start: function (data) {
        // Create game mode if not exists
        if (!game.model) {
            game.model = new model();
        }

        // Initialize html
        if ($("#gameContent").length == 0) {
            $("#gameTmpl").tmpl({}).appendTo($("#game_page").empty());
        }

        // Set model data
        game.model.set(data);

        // Remove overlay
        $("#question-dialog .overlay").addClass('d_none');
        $("#question-dialog .question").val('');
        $(".question-leader").removeClass('playing').text('Задать вопрос');

        // Fill user data received from server
        game.fillUserInfo(function () {
            // Make jq dialogs
            sD.qBox();

            // Get prepared data
            var box = game.model.box(), item = game.model.getQuestion();

            // Append HTML data
            $("#gameBoxTmpl").tmpl({time: game.model.interval()}).appendTo(box.find('>div').empty());

            // Show dialog
            box.dialog('open');
            box.parents('.ui-dialog').css({top: (117 + item.index * 180)});

            // Start counter down
            if (game.counterDownInterval) {
                clearInterval(game.counterDownInterval);
            }
            game.startCounterDown();

            // Hide unneeded elements
            $("#box1:before").hide();
            $("#box2:after").hide();

            // Set question
            box.find('p').text(item.question);
        });
    },

    answers: function (data) {
        if (!game.model) {
            game.model = new model();
        }
        if ($("#gameContent").length == 0) {
            $("#gameTmpl").tmpl({}).appendTo($("#game_page").empty());
        }
        $("#game_page .badge").parent().removeClass('active');
        $("#box1, #box2").removeClass('waiting').parents('.ui-dialog').css({top: 111});

        game.model.set(data);
        game.fillUserInfo(function () {
            sD.qBox();

            $('#box1, #box2').dialog('close');
            if (game.counterDownInterval) {
                clearInterval(game.counterDownInterval);
            }
            var box = game.model.box(), meItem = game.model.getUserItem(user_id);

            console.log(game.model.getAnswers());
            var obj = _.extend({time: game.model.interval()}, game.model.getAnswers());

            box.find('>div').replaceWith($("#gameAnswersTmpl").tmpl(obj));
            $("#gameCommentStepTmpl").tmpl(obj).appendTo(box.find('>div'));
            $(".answerer").text(obj.firstname + " отвечает...");
            $("#raty").raty();
            box.find('textarea').focus().val('');
            game.startCounterDown();

            box.removeClass('com1 com2').addClass('com' + meItem['comments'].index);

            box.find('.sd-btn').removeAttr('onclick').attr('onclick', 'game.comment(this)');
            box.dialog('open');

            $("#box1, #box2").parents('.ui-dialog').css({top: 111});
            $("#box1, #box2").find('.c').jScrollPane();
        });
    },

    choose: function (data) {
        if (!game.model) {
            game.model = new model();
        }
        if ($("#gameContent").length == 0) {
            $("#gameTmpl").tmpl({}).appendTo($("#game_page").empty());
        }
        $("#game_page .badge").parent().removeClass('active');
        $("#box1, #box2").removeClass('waiting').parents('.ui-dialog').css({top: 111});
        $("#box1, #box2").find('>div').empty();

        game.model.set(data);
        game.fillUserInfo(function () {
            sD.qBox();

            if (game.counterDownInterval) {
                clearInterval(game.counterDownInterval);
            }

            var obj = _.extend({
                time: game.model.interval(),
                comments: game.model.myComments()
            });
            var box = game.model.box();

            box.find('>div').replaceWith($("#gameChooseStepTmpl").tmpl(obj));
            box.find('.sd-btn').remove();

            game.startCounterDown();

            $("#gameChooseTmpl").tmpl({
                players: game.model[genderOpposite + 's']()
            }).appendTo($("#gameContent"));

            $("#box1, #box2").parents('.ui-dialog').css({top: 111});
        });
    },

    selectUser: function (user_id, self) {
        if ($("#gameContent .active").length > 0) {
            return;
        }

        $(self).find('.avatar').addClass('active');

        socket.send({
            module: 'game',
            method: 'setChosen',
            to_user_id: user_id,
            iterator: game.model.iterator()
        }, function (data) {

        });
    },

    answer: function (self) {
        var ta = $(self).parents('.ui-dialog-content').find('textarea'), txt = ta.val().trim(),
            meItem = game.model.getUserItem(user_id);
        if (!txt) {
            return;
        }

        var item = game.model.getQuestion();
        var answer = {
            to: item.user.user_id,
            answer: txt
        };

        var socketData = {
            answer: answer,
            method: 'setAnswer',
            module: 'game',
            iterator: game.model.iterator()
        };
        socket.send(socketData, function (data) {
                if (!data.result) {
                    return false
                }

                var box = game.model.box();
                meItem['answers'].push(answer);

                if (game.model.step() == 1) {
                    item = game.model.getQuestion();
                    if (!item) {
                        return $("#box1, #box2").addClass('waiting').parents('.ui-dialog').animate({top: 111});
                    }

                    box.parents('.ui-dialog').animate({top: (117 + item.index * 180)});
                    box.find('p').text(item.question);

                }

                return ta.val('').focus();
            }
        );
    },

    comment: function (self) {
        var ta = $(self).parents('.ui-dialog-content').find('textarea'),
            txt = ta.val().trim(),
            answer = game.model.getAnswers(),
            comment = {
                to: answer.user_id,
                comment: txt,
                score: $("input[name=score]").val()
            };

        var socketData = {
            comment: comment,
            method: 'setComment',
            module: 'game',
            iterator: game.model.iterator()
        };

        socket.send(socketData, function (data) {
            if (!data.result) {
                return;
            }

            var box = game.model.box(), meItem = game.model.getUserItem(user_id);
            meItem.comments.push(answer);
            console.log('com' + meItem.comments.length);
            box.removeClass('com0 com1 com2').addClass('com' + meItem.comments.length);
            ta.val('').focus('');

            //if (game.model.step() == 2) {
            answer = game.model.getAnswers();
            if (!answer) {
                $(".answerer").text('');
                return $("#box1, #box2").addClass('waiting').parents('.ui-dialog').animate({top: 111});
            }

            var view = $("#gameCommentStepTmpl").tmpl(game.model.getAnswers());
            $(".answerer").text(answer.firstname + " отвечает...");

            box.find('.rmv').remove();
            box.find('>div').append(view);
            box.find('textarea').focus().val('');

            $("#raty").raty();

            $("#box1, #box2").find('.c').jScrollPane();
        });
    },

    finish: function (data) {
        if (game.counterDownInterval) {
            clearInterval(game.counterDownInterval);
        }

        game.model.set(data);

        $(".u." + gender).append("<div class='heartChain'></div>");
        $(".u." + genderOpposite).append("<div class='heartChain'></div>");

        $(".l .heartChain").append("<figure class='small_heart1'></figure>");
        $(".l .heartChain").append("<figure class='small_heart2'></figure>");
        $(".l .heartChain").append("<figure class='small_heart3'></figure>");
        $(".l .heartChain").append("<figure class='small_heart4'></figure>");
        $(".l .heartChain").append("<figure class='small_heart5'></figure>");

        $(".r .heartChain").append("<figure class='small_heart1'></figure>");
        $(".r .heartChain").append("<figure class='small_heart2'></figure>");
        $(".r .heartChain").append("<figure class='small_heart3'></figure>");
        $(".r .heartChain").append("<figure class='small_heart4'></figure>");
        $(".r .heartChain").append("<figure class='small_heart5'></figure>");

        var i = 1, success = _.once(game.showChosen);
        var interval = setInterval(function () {
            $(".heartChain").find('.small_heart' + i).fadeIn();
            if (i++ == 6) {
                clearInterval(interval);
                success();
            }
        }, 850);

        $('#box1, #box2').parents('.ui-dialog').remove();
        $('#box1, #box2').remove();

        $('.players').remove();
    },

    showChosen: function () {
        _.each([game.model.males(), game.model.females()], function (players) {
            _.each(players, function (value) {
                var selection = $("<div></div>").addClass("selection"),
                    u = $(".u_" + value.user.user_id),
                    img = "/images/no-chosen.png";

                u.find(".heartChain").append(selection);

                if (value.chosen) {
                    var chosen = game.model.getUserItem(value.chosen);
                    img = chosen.user.avatar;
                }

                $(".selection", u).append('<img class="sel" src="' + img + '" />');
            })
        });

        setTimeout(function () {
            $(".heartChain figure").fadeOut();
            $(".l .selection").animate({left: 0});
            $(".r .selection").animate({right: 0}, function () {
                _.once(function () {
                    $("#game_page").append("<button class='startNewGame'></button>");

                    if (game.model.hasOverlap()) {
                        game.showBingo();
                    }
                })();
            });
        }, 950);
    },

    showBingo: function () {
        var currentUser = game.model.getUserItem(user_id),
            overlap = game.model.hasOverlap();

        $("#bingo .item1").removeClass('female').removeClass('male');
        $("#bingo .item2").removeClass('female').removeClass('male');

        var tmplObject = {
            user_id_2: currentUser.user.user_id,
            user_id_1: overlap.user.user_id,

            gender_2: currentUser.user.gender,
            gender_1: overlap.user.gender,

            avatar_2: currentUser.user.avatar,
            avatar_1: overlap.user.avatar,

            skin_2: currentUser.user.skin,
            skin_1: overlap.user.skin,

            self_2: currentUser.user.user_id == user_id,
            self_1: overlap.user.user_id == user_id,

            is_vip_2: currentUser.user.is_vip,
            is_vip_1: overlap.user.is_vip
        };

        $("#bingoContent").html($("#bingoTmpl").tmpl(tmplObject));
        $("#bingo").dialog("open");

        delete game.model;
    },

    setMyTable: function (data) {
        var g = new model(data), item = g.getUserItem(user_id);
        $("#question-dialog .question").val(item.question);
        game._initialPlaying();
    },

    changeUserStatus: function (data) {
        $('.u_' + data.uId + ' .badge').parent().addClass('active');
    },

    startCounterDown: function () {
        var box = game.model.box();
        game.counterDownInterval = setInterval(function () {
            var s = box.find('time span');
            if (s.length == 0) {
                return clearInterval(game.counterDownInterval);
            }

            var time = parseInt(box.find('time span').text().trim());
            box.find('time span').text(--time);

            if (time == 0) {
                return clearInterval(game.counterDownInterval);
            }

            return true;
        }, 1000);
    },

    //------------ private -------------
    fillUserInfo: function (callback) {
        if ($("#game_page").hasClass('show')) {
            return callback();
        }

        var players1 = game.model[gender + 's'](), players2 = game.model[genderOpposite + 's']();
        var time = game.model.interval();

        gCounterDown(time);

        if ($('.line.l .u').length == 0) {
            _.each(players1, function (val, key) {
                $("#gameUserTmpl").tmpl(val.user).appendTo('.line.l');
            });
        }
        if ($('.line.r .u').length == 0) {
            _.each(players2, function (val, key) {
                $("#gameUserTmpl").tmpl(val.user).appendTo('.line.r');
            });
        }

        $("#game_page").addClass('show');
        setTimeout(function () {
            callback();
        }, 1800);
    },

    _initialPlaying: function () {
        // Validate question string, empty string is not allowed
        if (!$("#question-dialog .question").val().trim()) {
            return $("#question-dialog .question").focus();
        }

        // Time counter
        $("#runner").runner({
            autostart: true,
            format: function (value, object) {
                var sec = Math.floor(value / 1000);
                var min = Math.floor(sec / 60);
                sec = sec - min * 60;

                min = min < 10 ? "0" + min : min;
                sec = sec == 60 ? 0 : sec;
                sec = sec < 10 ? "0" + sec : sec;

                return min + ':' + sec;
            }
        });

        $('.question-leader.sd-btn').addClass('playing').text('Отменить');
        $('.q-box .overlay').removeClass('d_none');
    }
};

var model = function (gamedata) {
    /**
     * @type {model}
     */
    var root = this;

    /**
     * Model data
     * @type {null}
     */
    var data = null;

    /**
     * Set data model
     * @param d
     */
    this.set = function (d) {
        data = d;
    };

    /**
     * Get males items
     * @returns {male|*|{}}
     */
    this.males = function () {
        return data.players.male;
    };

    /**
     * Get female items
     * @returns {female|*|{}}
     */
    this.females = function () {
        return data.players.female;
    };

    /**
     * Get step interval
     * @returns {*}
     */
    this.interval = function () {
        return data.interval;
    };

    /**
     * Get game iterator
     * @returns {*}
     */
    this.iterator = function () {
        return data.key;
    };

    /**
     * Return user item from data
     * @param user_id
     * @returns {*}
     */
    this.getUserItem = function (user_id) {
        var d = null;
        if (data.players.male.hasOwnProperty('u_' + user_id)) {
            d = data.players.male['u_' + user_id];
        }

        if (data.players.female.hasOwnProperty('u_' + user_id)) {
            d = data.players.female['u_' + user_id];
        }

        if (!d) {
            return null;
        }

        if (!d.hasOwnProperty('answers')) {
            d['answers'] = [];
        }

        if (!d.hasOwnProperty('comments')) {
            d['comments'] = [];
        }

        return d;
    };

    /**
     * Get game step
     * @returns {*}
     */
    this.step = function () {
        return data.step;
    };

    this.checkMe = function (success, fail) {
        var iterator = root.iterator();

        socket.send({
            iterator: iterator,
            method: 'isTableExists',
            module: 'game'
        }, function (data) {
            if (!data.result) {
                if (success) success();
            } else {
                if (fail) fail();
            }
        });
    };

    /**
     * Get box by gender
     * @returns {*}
     */
    this.box = function () {
        if (gender == 'male') {
            return $("#box1");
        }
        if (gender == 'female') {
            return $("#box2");
        }
        return null;
    };

    /**
     * Get question for user
     * @returns {*}
     */
    this.getQuestion = function () {
        var item = root.getUserItem(user_id),
            index = item['answers'].length - 1,
            players = data.players[genderOpposite];

        if (index >= 3) {
            return false;
        }

        return _.extend(players[_.keys(players)[index]], {index: index});
    };

    /**
     * Get user answers
     * @returns {*}
     */
    this.getAnswers = function () {
        // My comments
        var meItem = this.getUserItem(user_id), index;
        if (!meItem.hasOwnProperty('comments')) {
            meItem['comments'] = [];
        }
        index = meItem['comments'].length;
        if (index >= 3) {
            return null;
        }

        // Answers
        var players = data.players[genderOpposite],
            player = players[_.keys(players)[index]],
            answers = [];

        if (!player.hasOwnProperty('answers') || player.answers.length == 0) {
            return {
                hasAnswers: false,
                user_id: player.user.user_id,
                firstname: player.user.firstname
            };
        }

        $(player.answers).each(function () {
            var item = root.getUserItem(this.to);

            answers.push({
                question: item.question,
                answer: this.answer,
                avatar: item.user.avatar
            });
        });

        return _.extend({answers: answers}, {
            user_id: player.user.user_id,
            hasAnswers: true,
            firstname: player.user.firstname
        });
    };

    /**
     * User comments
     * @returns {Array}
     */
    this.myComments = function () {
        var comments = [], players = game.model[genderOpposite + 's']();
        _.each(players, function (player) {
            if (typeof player.comments == 'undefined') {
                return;
            }

            _.each(player.comments, function (comment) {
                if (!$.trim(comment['comment'])) {
                    return;
                }

                if (comment.to == user_id) {
                    comments.push({
                        comment: comment['comment'],
                        avatar: player.user.avatar
                    });
                }
            });
        });

        return comments;
    };

    /**
     * Has current user overlap?!
     * @returns {*}
     */
    this.hasOverlap = function () {
        var userItem = game.model.getUserItem(user_id), chosen = userItem.chosen;
        if (!chosen) {
            return false;
        }

        var overlap = game.model.getUserItem(chosen);
        if (overlap.chosen != user_id) {
            return false;
        }

        return overlap;
    };

    //
    if (gamedata) {
        root.set(gamedata);
    }
};


$(function () {
    $(document).on('click', '.startNewGame', function () {
        $("#gameContent").remove();
        $(".startNewGame").remove();
        $("#game_page").empty().removeClass('show');
    });

    $(document).on('socketReady', function () {
        if (game.model) {
            game.model.checkMe(
                function () {
                }, function () {
                    delete game.model;
                    game.model = null;
                }
            );
        } else {
            $("textarea.question").val(username + " question");
            $(".question-leader.sd-btn").trigger('click');

            if (game.counterDownInterval) {
                clearInterval(game.counterDownInterval);
            }
        }
    });
});

var gCounterDown = function (t) {
    var time = t;

    if (typeof interval != "undefined") {
        clearInterval(interval);
    }
    interval = setInterval(function () {

        if (time == 0) {
            $(".mi_item_1 time").text("").hide();
            if (interval) {
                clearInterval(interval);
            }
        }

        f();

        --time;

    }, 1000);

    var f = function () {
        var min = Math.floor(time / 60);
        var sec = Math.floor(time % 60);

        min = min < 10 ? "0" + min : min;
        sec = sec < 10 ? "0" + sec : sec;

        $(".mi_item_1 time").text(min + ":" + sec);
    }

};