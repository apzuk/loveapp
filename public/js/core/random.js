var random = {
    list1: {},
    list2: {},

    inited: {
        index1: false,
        index2: false
    },

    init: function (index) {
        if (random.inited["index" + index]) {
            return $('#tagCloud' + index).tagcanvas("update");
        }

        $('#tagCloud' + index).tagcanvas({
            textColour: '#000000',
            outlineColour: '#000000',
            maxSpeed: 0.003,
            minSpeed: 0.003,
            depth: 0.1,
            zoom: 1.2,
            wheelZoom: false,
            initial: [0, 0.7],
            dragThreshold: 5,
            shape: "hcylinder",
            reverse: true,
            lock: "x",
            freezeActive: false,
            dragControl: true,
            outlineOffset: 5
        });

        return random.inited["index" + index] = true;
    },

    load: function () {
        return $.post('/api/user/getRandom', function (data) {
            random.list1 = data.list1;
            random.list2 = data.list2;

            random.startAppendingToList(random.list1, 1);
            random.startAppendingToList(random.list2, 2);

            $("#tagCloud1, #tagCloud2").find(">ul").empty();
        });
    },

    startAppendingToList: function (list, index) {
        if (list.length == 0) {
            random.init(index);
            return;
        }

        var first = list.shift();
        $("<img />").load(function () {
            $("#online_user_tmpl").tmpl(first).appendTo($("#tagCloud" + index + " > ul"));
            random.startAppendingToList(list, index);
        }).attr("src", first.avatar);
    }
};

$(function () {
    setTimeout(function () {
        random.load();
    }, 250);

    setInterval(function () {
        random.load();
    }, 1000 * 60 * 5);
});