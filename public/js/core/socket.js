var socket = new function () {

    var context = this,

        callbacks = {},

        sock,
        reConnectCounter = 0,

        response = {},
        timeout,
        connectionJsTimeout,
        delayBeforeConnectionError = 15000,

        totalAttempts = 0;
    this.url = 'http://10.0.0.105:8088/engine';

    this.init = function () {
        if (!context.url) {
            throw "SockJS url is empty";
        }

        sock = new SockJS(this.url);

        sock.onopen = function () {
            $('.disconnected').addClass('nc').removeClass('disconnected');

            clearTimeout(timeout); // stop attempts to reconnect
            clearTimeout(connectionJsTimeout);

            if (context.isConnected()) {
                context.send({
                    method: "connect",
                    module: "users"
                });
            }
            $(document).trigger('socketReady');

            sock.onmessage = function (e) {
                var json = $.parseJSON(e.data);
                if (json.triggerChannel) {
                    callbacks[json.channel](json);
                }

                if (typeof callbacks[json.method + '.' + json.module] == 'function') {
                    callbacks[json.method + '.' + json.module](json);
                    delete callbacks[json.method + '.' + json.module];
                } else {
                    if (json.method && json.module == 'game') {
                        game[json.method](json);
                    }
                }
            };
        };
        sock.onclose = function () {
            $(document).trigger("connectionLost");
            context.reConnect();
        };
    };

    this.isConnected = function () {
        return (typeof(sock.readyState) != 'undefined' && sock.readyState == 1) ? true : false;
    };

    this.subscribe = function (channel, callback) {
        this.send({
            method: "subscribe",
            module: "users",
            channel: channel,
            sid: sid
        });
        callbacks[channel] = callback;
    };

    this.send = function (m, callback) {
        if (!m || typeof(m) != 'object') {
            return false;
        }

        if (!context.isConnected()) {
            return false;
        }

        if (callback) {
            callbacks[m.method + '.' + m.module] = callback;
        }

        m['user_id'] = user_id;
        m['sid'] = sid;

        var data = JSON.stringify(m);
        console.log('Data sent: ' + data);

        sock.send(data);
    };

    this.reConnect = function () {

        console.log("reconnect");

        if (context.isConnected()) {
            //OxotaPmLog(('Socket already connected');
            clearTimeout(timeout);
            return;
        }


        timeout = setTimeout(function () {
            context.init();

        }, 3000);
    };

    this.dump = function () {
    };

    this.close = function () {
        sock.close();
    };

    $(function () {
        context.init();
    })
}();