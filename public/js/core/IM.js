var IM = {
    companion_id: null,

    select: function (u_id) {
        $("#messages .textarea-box").removeClass('d_none');

        $(".ls_user.active").removeClass('active');
        $(".ls_user_" + u_id).addClass('active');

        IM.companion_id = u_id;

        var boxID = user_id + '_dialog_' + u_id;

        if ($("#" + boxID).length == 0) {
            $("<div id='" + boxID + "'></div>").appendTo($(".dialogs"));

            $("#" + boxID)
                .data('userid', u_id)
                .jScrollPane({
                    autoReinitialise: true,
                    reinitialise: true,
                    animateScroll: true
                });

            //load old messages
            IM.load(u_id, 0, function (data) {
                var $selector = $("#" + boxID), api = $selector.data('jsp');
                api.getContentPane().prepend(data.htmlContent);
                api.reinitialise();
                api.scrollTo(0, 10000000);

                setTimeout(function () {
                    api.scrollTo(0, 10000000);
                    IM.registerScrollToListener($selector);
                }, 550);

                $("#ta_box").focus();
            })
        }

        $(".dialogs > div").addClass('hidden');
        $("#" + boxID).removeClass('hidden');
    },

    /**
     * Write to user preparation
     *
     * @param u_id
     */
    compose: function (u_id) {
        if (u_id == user_id) {
            return;
        }

        sD.closeDialog('#profile');
        $(".mi_item_2").trigger("click");

        IM.loadCompanion(u_id, function () {
            IM.select(u_id);
        });
    },

    /**
     * Send new message
     *
     * @returns {*}
     */
    send: function () {
        if (!IM.companion_id) {
            return false;
        }
        var txt = $("#ta_box").html().trim(), filename = $("#attachment").val();
        if (!txt) {
            return false;
        }

        IM.removeUploadedPhoto();
        $("#ta_box").html('').focus();

        return $.post('/api/IM/write', {
            to: IM.companion_id,
            message: txt,
            attachment: filename
        }, function (data) {
        });
    },

    /**
     * Append new message to dialog box
     *
     * @param u_id
     * @param html
     */
    appendToDialog: function (u_id, html) {
        var boxID = '#' + user_id + '_dialog_' + u_id;
        if ($(boxID).length == 0) {
            return;
        }

        api = $(boxID).data('jsp');
        api.getContentPane().append(html);
        setTimeout(function () {
            api.reinitialise();
            api.scrollTo(0, 10000000)
        }, 100);
    },

    /**
     * Load old messages
     *
     * @param u_id
     * @param offset
     * @param callback
     */
    load: function (u_id, offset, callback) {
        $.post('/api/IM/load', {
            to: u_id,
            offset: offset
        }, function (data) {
            callback(data);
        });
    },

    /**
     * Make message as read
     *
     * @param messageId
     * @param self
     * @returns {*}
     */
    makeAsRead: function (messageId, self) {
        if ($(self).data('owner') == user_id || !$(self).hasClass('active')) {
            return false;
        }

        // Downgrade messages count in message tab
        var $num = $(".mi_item_2 .num"), count = parseInt($num.text());
        count = !count || count < 0 ? 0 : count;

        $num.text(--count).attr("class", function (i, cls) {
            return cls.replace(new RegExp(/count\d/), 'count' + count);
        });

        // Downgrade leftside count
        var $numC = $(".ls_user.active .unread-num"), countC = parseInt($numC.text());
        countC = !countC || countC < 0 ? 0 : countC;
        $numC.text(--countC).attr("class", function (i, cls) {
            var regexp = new RegExp(/count\d+/);
            return cls.replace(regexp, 'count' + countC);
        });

        $(self).removeClass('active');
        return $.post('/api/IM/makeAsRead', {msgId: messageId});
    },

    /**
     * Change message status
     *
     * @param data
     */
    changeMessageStatus: function (data) {
        $("#msg_" + data.msg_id).removeClass('active');
    },

    /**
     *
     * @param data
     */
    photoUploaded: function (data) {
        sD.closeDialog('#upload-photo');

        $("#messages .ico-upload").addClass("ui-state-disabled");
        $("<img />")
            .attr("src", data.result.path_80)
            .attr("onclick", "IM.showOriginal('" + data.result.filename + "')")
            .appendTo($(".image-container").empty());

        $("#attachment").val(data.result.filename);
        $(".image-container").removeClass("d_none");
    },

    /**
     * Remove attached photo
     */
    removeUploadedPhoto: function () {
        $("#attachment").val('');
        $(".image-container").addClass("d_none").find('img').remove();
        $("#messages .ico-upload").removeClass("ui-state-disabled");
    },

    /**
     * Prepare photo shooting
     * @param webcam
     */
    prepareShooting: function (webcam) {
        $("#take-pic .sd-avatar").unbind('click').on('click', function (e) {
            if ($(this).hasClass('active')) {
                return;
            }

            $(this).addClass('active');

            var counter = 3;
            $("#take-pic .sd-avatar").find('span').remove();
            $("#take-pic .sd-avatar").append("<span>" + counter + "</span>");
            var i = setInterval(function () {
                if (--counter < 0) {
                    clearInterval(i);
                    $('#shut').fileapi('add', webcam.shot());
                    $("#take-pic .sd-avatar").find('span').remove();
                    /*$(".close-take-pic").text("Прикрепить").unbind('click').on('click', function () {
                     $("#shut .uploading-process").show();
                     $('#shut').fileapi('upload');
                     });*/

                    $("#shut .uploading-process").show();
                    $('#shut').fileapi('upload');

                    return $('.js-img').webcam('destroy');
                }
                $("#take-pic .sd-avatar").find('span').text(counter);
            }, 1000);
        });
        $("#take-pic").unbind("dialogclose").on("dialogclose", function (event, ui) {
            $("#take-pic .sd-avatar").css("top", '');
            $('.js-img').webcam('destroy');
            $('.js-img').remove();

            $(".js-preview").parent().append("<div class='js-img'></div>");
        });
    },

    /**
     * Delete message
     * @param message_id
     * @param obj
     */
    deleteMsg: function (message_id, obj) {
        sD.confirm('Вы действительно хотите удалить это сообщение?',
            function () {
                $(obj).parents('.msg-item').fadeOut(function () {
                    $(this).remove();
                });

                $.post('/api/IM/deleteMessage', {msg_id: message_id});
            }, function () {

            }
        );
    },

    /**
     * Show original photo in th dialog
     * @param originalName
     */
    showOriginal: function (originalName) {
        var originalPhotoPath = "/uploads/msg/" + originalName,
            $img = $("<img />").attr("src", originalPhotoPath);

        $("#photo-dialog").dialog("open");
        $("#photo-dialog .uploaded-photo").empty().append($img);
    },

    /**
     * New message arrive
     * @param data
     */
    newMessage: function (data) {
        //IM.select(u_id);
        IM.appendToDialog(data.companionId, data.messageHtml);

        if (user_id == data.user_id) {
            return;
        }
        // Upgrade messages count in message tab
        var $num = $(".mi_item_2 .num"), count = parseInt($num.text());
        $num.text(++count).attr("class", function (i, cls) {
            var regexp = new RegExp(/count\d+/);
            return cls.replace(regexp, 'count' + count);
        }).parents('.menu_b').addClass('shake shake-hard shake-constant');

        setTimeout(function () {
            $num.parents('.menu_b').removeClass('shake shake-hard shake-constant')
        }, 1000);

        // Upgrade messages count in companion tab
        var $item = $(".ls_user_" + data.companionId),
            $numC = $item.find(".unread-num"),
            countC = parseInt($numC.text());
        countC = !countC ? 0 : countC;

        $numC.text(++countC).attr("class", function (i, cls) {
            var regexp = new RegExp(/count\d+/);
            return cls.replace(regexp, 'count' + countC);
        });
    },

    //----------------------------------------->
    loadCompanion: function (user_id, callback) {
        var item = $(".ls_user_" + user_id);
        if (item.length != 0) {
            return callback();
        }

        var ___ii = function (user) {
            var sidebar = $(".sidebar.jsp").data('jsp').getContentPane();
            $("#lsUserTmpl").tmpl(user).prependTo(sidebar);

            callback();
        };
        sD.loadUser(user_id, ___ii);
        return true;
    },

    registerScrollToListener: function (selector) {
        selector.bind('jsp-scroll-y', function (event, scrollPositionY, isAtTop, isAtBottom) {
            if (scrollPositionY >= 150 || scrollPositionY == 0 || $(this).data('in_process') == 1) {
                return;
            }

            console.log('scrollPositionY: ' + scrollPositionY);

            var root = $(this), user_id = $(".ls_user.active").data('userid'), offset = 1;

            // Set in process to ignore text time scroll, until response will not arrive
            root.data('in_process', 1);

            // Initial offset
            if (!root.data('offset')) {
                root.data('offset', offset);
            } else {
                offset = root.data('offset');
            }

            // Load messages
            IM.load(user_id, $(this).data('offset'), function (data) {
                var api = selector.data('jsp');
                api.getContentPane().prepend(data.htmlContent);
                api.reinitialise();

                if ($.trim(data.htmlContent)) {
                    api.scrollTo(0, 200);
                    root.data('in_process', 0);
                }

                root.data('offset', ++offset);
            });
        });
    }
};

$(function () {
    $(".image-container").on("click", function (event) {
        if (event.target.nodeName === "IMG") {
            return;
        }
        IM.removeUploadedPhoto();
    });

    $('#shut').fileapi({
        url: '/api/IM/uploadPhoto',
        accept: 'image/*',
        autoUpload: false,
        data: {
            'csrf-token': $("meta[name=csrf-token]").attr('content')
        },
        elements: {
            preview: {
                el: '.js-preview',
                width: 420,
                height: 300
            },
            progress: '#shut .js-progress'
        },
        onComplete: function (evt, uiEvt) {
            IM.photoUploaded(uiEvt);
            sD.closeDialog('#take-pic');
            $("#shut .uploading-process").hide();
        }
    });

    $("#take-pic").on('dialogopen', function () {
        $("#take-pic").parents(".ui-dialog").show();
        $(".js-preview").find("canvas").remove();
        $(".js-preview").empty();
        $("#take-pic .sd-avatar").removeClass("active");

        $('.js-img').webcam({
            width: 520,
            height: 320,
            error: function (err) {
                console.log(err);
            },
            success: function (webcam) {
                $("#take-pic .sd-avatar").addClass("btn_hov cur_p").animate({top: "64%"}, function () {
                    IM.prepareShooting(webcam);
                });
            }
        });
    });
});

//################################### emojic ########################################
var emojic = new function () {
    var msg;

    (this.initial = function () {
        msg = jQuery('#ta_box');
        msg
            // make sure br is always the lastChild of contenteditable
            .on("keyup mouseup", function () {
                if (!this.lastChild || this.lastChild.nodeName.toLowerCase() != "br") {
                    //this.appendChild(document.createElement("br"));
                }
            })

            // use br instead of div div
            .on("keypress", function (e) {
                if (e.which == 13) {
                    if (window.getSelection) {
                        var selection = window.getSelection(),
                            range = selection.getRangeAt(0),
                            br = document.createElement("br");
                        range.deleteContents();
                        //range.insertNode(br);
                        //range.setStartAfter(br);
                        //range.setEndAfter(br);
                        //range.collapse(false);
                        selection.removeAllRanges();
                        selection.addRange(range);
                        return false;
                    }
                }
            });
    })();

    this.showEmotics = function (obj) {
        var x = document.createElement('img');
        x.src = $(obj).attr('src');
        _insertNodeOverSelection(x, msg[0]);
    };
    //============================================================================================================
    /**
     * @param ancestor
     * @param descendant
     * @returns {boolean}
     * @private
     */
    var _isOrContainsNode = function (ancestor, descendant) {
        var node = descendant;
        while (node) {
            if (node === ancestor) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    };

    /**
     * @param node
     * @param containerNode
     * @private
     */
    var _insertNodeOverSelection = function (node, containerNode) {
        var sel, range, html, str;

        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                if (_isOrContainsNode(containerNode, range.commonAncestorContainer)) {
                    range.deleteContents();
                    range.insertNode(node);
                } else {
                    containerNode.appendChild(node);
                }
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();
            if (_isOrContainsNode(containerNode, range.parentElement())) {
                html = (node.nodeType == 3) ? node.data : node.outerHTML;
                range.pasteHTML(html);
            } else {
                containerNode.appendChild(node);
            }
        }

        _placeCaretAtEnd(containerNode);
    };

    /**
     * @param el
     * @private
     */
    var _placeCaretAtEnd = function (el) {
        el.focus();
        if (window.getSelection) {
            if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);

                if (navigator.userAgent.indexOf("Firefox") != -1) {
                    range.setEndBefore($(el).find('br')[0]); //only for FF, fiddle does not have browser check
                }
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        }
    };
};