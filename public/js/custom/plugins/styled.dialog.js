(function ($) {

    $.fn.styledDialog = function (opts) {

        /**
         * Default options
         * @type {{}}
         */
        var defaults = {
            gender: 'male',
            addClass: false,
            autoOpen: true,
            zIndex: false,
            buttons: []
        };

        /**
         * Options
         * @type {extend|*}
         */
        var options = $.extend(defaults, opts);

        /**
         * Reserve this
         * @type {fn}
         */
        var self = $(this);

        if (options.zIndex) {
            $(this).parents('.ui-dialog').css({zIndex: options.zIndex});
        }

        /**
         * Make jquery dialog styled!
         */
        function make() {
            //already styled
            $(this).data('styled', 1);

            //add shadow to box
            self.parent()
                .addClass('sd-box')
                .css({
                    border: 'none',
                    borderTop: '1px solid #ccc',
                    padding: 0
                })
                .find('.ui-dialog-titlebar').remove();

            self.height(self.height() + 75)

            //wrap styled div
            self.css({
                padding: 0,
                overflow: 'hidden'
            }).wrap('<div class="sd-wrapper ' + options.gender + '"></div>');

            if (options.addClass) {
                self.parents('.sd-box').addClass(options.addClass);
            }

            //make sd button
            makeBtn();

            self
                .find('[class*=close]')
                .unbind('click')
                .on('click', function () {
                    self.dialog('close');
                });

            //show when done!
            self.dialog('option', 'resizable', false);

            if (options.autoOpen) {
                self.dialog('open');
            }
        }

        function makeBtn() {
            if (options.buttons.length == 0) {
                return
            }

            $.each(options.buttons, function (index) {
                $(this.selector)
                    .addClass('sd-btn')
                    .attr('value', this.value)
                    .val(this.value)
                    .text(this.value);

                if (this.callback) {
                    var cb = this.callback;
                    $(this.selector).unbind('click').on('click', function () {
                        eval(cb);
                    });
                }

                var btnWith = $(self).dialog("option", "width") / 2;

                if (options.buttons.length == 2) {
                    $(this.selector).css({
                        width: btnWith + 'px',
                        height: 37,
                        left: 0
                    });

                    //
                    var borderColor = userManager.getCurrentUser().isMale() ? '#7ACFE9' : '#d87b9b';

                    if (index == 0) {
                        $(this.btn).addClass('rBorder');

                        //$(this.btn).width($(this.btn).width() - 1);
                    }

                    if (index == 1) {
                        $(this.btn).css({
                            left: '50%'
                        });
                        $(this.btn).addClass('lBorder');

                        // $(this.btn).width($(this.btn).width() + 1);
                    }

                    $(this.btn).css({
                        width: '-=1'
                    })
                }
            });
        }

        if (!$(this).data('styled')) {
            if (!self.parent().hasClass('sd-box')) {
                make();
            }
        }
    }

})(jQuery);