<?php

defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

require dirname(__FILE__).'/../../vendor/autoload.php';

$configs = require_once __DIR__ . '/local/config.php';

unset($configs['defaultController']);
unset($configs['controllerMap']);
unset($configs['catchAllRequest']);

return $configs;