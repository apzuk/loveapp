<?php

$basePath = __DIR__ . '/../../protected';

/* Set aliases
-------------------------------------------------- */

Yii::setPathOfAlias('Cache', $basePath . DIRECTORY_SEPARATOR . 'extensions/cache');

return array(
    'id'                => '3qYcuvw7qhtRMnpfQHVVGpnfpZwyBi',
    'name'              => 'Love App',
    'basePath'          => $basePath,
    'defaultController' => 'index',
    'sourceLanguage'    => 'en',
    'language'          => 'ru',
    
    'import' => array(
        'application.models.*',
        'application.models.custom.*',
        'application.models.custom.awards.*',
        'application.components.*',
        'application.helpers.*',
        'application.widgets.*',
        'application.vendors.*',
    ),

    'preload' => array('log'),

    'components' => array(
        'urlManager' => array(
            'class'          => 'application.components.UrlManager',
            'urlFormat'      => 'path',
            'showScriptName' => false,

            'rules' => array(
            )
        ),

        'request' => array(
            'class'                  => 'HttpRequest',
            'enableCsrfValidation'   => true,
            'enableCookieValidation' => true,

            'csrfTokenName' => 'csrf-token',

            'noCsrfValidationRoutes' => array
            (
                'profile/media/upload',
            )
        ),

        'cacheFile' => array(
            'class' => 'system.caching.CFileCache',
        ),

        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),

        'errorHandler' => array(
            'errorAction' => YII_DEBUG ? null : 'index/error',
        ),

        'clientScript' => array(
            'class'                => 'ext.yii-EClientScript.EClientScript',
            'combineScriptFiles'   => true,
            'combineCssFiles'      => true,
            'optimizeScriptFiles'  => false,
            'optimizeCssFiles'     => false,
            'optimizeInlineScript' => false,
            'optimizeInlineCss'    => false,
            'coreScriptPosition'   => CClientScript::POS_END,

            'packages'  => array(
                'jquery' => array(
                    'baseUrl' => '../public/js/plugins/',
                    'js'      => array(
                        'jquery-2.1.1.min.js'
                    ),
                ),

                'admin' => array(
                    'baseUrl' => '/public/',
                    'js'      => array('js/plugins/jquery.form.min.js',
                        'js/plugins/underscore-min.js',
                        'js/admin/forms.js',
                        'js/admin/misc.js',
                        'js/admin/global.js'),
                    'css'     => array('css/admin/animate.css',
                        'css/admin/style.css'),
                    'depends' => array('jquery', 'bootstrap', 'nprogress')
                ),

                'nprogress' => array(
                    'baseUrl' => '/public/js/plugins/NProgress',
                    'js'      => array('nprogress.js'),
                    'css'     => array('nprogress.css'),
                ),

                'app' => array(
                    'baseUrl' => '../public/',
                    'js'      => array(
                        // Plugins
                        'js/plugins/jquery-ui-1.10.4.custom.js',
                        'js/plugins/jquery.limit-1.2.source.js',
                        'js/plugins/jquery.tmpl.min.js',
                        'js/plugins/underscore-min.js',
                        'js/plugins/jquery.mousewheel.js',
                        'js/plugins/mwheelIntent.js',
                        'js/plugins/jquery.jscrollpane.min.js',
                        'js/plugins/jquery.formstyler.min.js',
                        'js/plugins/jquery.runner-min.js',
                        'js/plugins/jquery.raty.js',
                        'js/plugins/sockjs.js',
                        'js/plugins/mosaic.1.0.1.min.js',
                        'js/plugins/modernizr.custom.54838.js',
                        'js/plugins/query.tagcanvas.js',
                        'js/plugins/select2.js',
                        //'js/plugins/jquery.countdown.min.js',

                        // Custom plugins
                        'js/custom/plugins/styled.dialog.js',

                        // core scripts
                        'js/core/em.js',
                        'js/core/cf.js',
                        'js/core/random.js',
                        'js/core/socket.js',
                        'js/core/game.js',
                        'js/core/IM.js',
                        'js/global.js',
                        'js/script.js',
                    ),
                    'css'     => array(),
                    'depends' => array('')
                ),

                'bootstrap' => array(
                    'baseUrl' => '/public/js/plugins/Bootstrap',
                    'js'      => array('bootstrap.min.js', 'bootstrap-datetimepicker.min.js'),
                    'css'     => array('bootstrap.min.css', 'bootstrap-datetimepicker.min.css'),
                    'depends' => array('jquery', 'jquery.ui'),
                ),

                'fileapi' => array(
                    'baseUrl' => '../public/js/plugins/',
                    'js'      => array('FileAPI.min.js', 'jquery.fileapi.min.js'),
                    'depends' => array('jquery'),
                ),
            ),
        ),

        'log' => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'              => 'CDbLogRoute',
                    'except'             => 'exception.CHttpException.404',
                    'levels'             => 'trace, info, error, warning',
                    'logTableName'       => 'logs',
                    'connectionID'       => 'db',
                    'autoCreateLogTable' => YII_DEBUG
                ),
            ),
        ),

        'authManager' => array(
            'class'           => 'CDbAuthManager',
            'connectionID'    => 'db',
            'assignmentTable' => 'authAssignment',
            'itemChildTable'  => 'authItemChild',
            'itemTable'       => 'authItem'
        ),
    ),

    /* Modules settings
    -------------------------------------------------- */

    'modules' => array(
        'api',
    ),

    /* Global settings
    -------------------------------------------------- */

    'params' => array()
)?>