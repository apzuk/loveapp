<?php

class Arr
{
    /**
     * Filter mask key
     * @param string $className
     * @param string $mask
     * @param array $haystack
     * @return array
     */
    public static function filterMaskKey($className, $mask)
    {
        $refl = new ReflectionClass($className);
        $haystack = $refl->getConstants();

        $arr = [];
        foreach ($haystack as $key => $val) {
            if (preg_match($mask, $key)) {
                $arr[] = ['key' => $key, 'value' => $val];
            }
        }
        return $arr;
    }

    /**
     * Make an associative array from values
     * @param array $array
     * @return array
     */
    public static function makeAssocFromValues($array)
    {
        return $array && count($array) ? array_combine(array_values($array), $array) : [];
    }

    /**
     * @param $array
     * @param $fieldValue
     * @param $fieldKey
     * @return array
     */
    public static function makeKeysAssocValue($array, $fieldValue, $fieldKey)
    {
        $returnData = [];

        foreach ($array as $key => $val) {
            $returnData[] = [
                $fieldValue => $val,
                $fieldKey => $key
            ];
        }

        return $returnData;
    }

    /**
     * @param $modelArray
     * @return array
     */
    public static function modelIdsAsArray($modelArray)
    {
        if (!is_array($modelArray)) {
            return [];
        }

        $idvals = [];

        foreach ($modelArray as $model) {
            $idvals[] = $model->id;
        }

        return $idvals;
    }

    /**
     * @param $values
     * @return string
     */
    public static function uniqueIntArray($values)
    {
        $newvals = [];

        foreach ((array)$values as $val) {
            $val = (int)preg_replace('/\D/', '', $val);

            if ($val) {
                $newvals[$val] = 1;
            }
        }

        if (count($newvals)) {
            return join(', ', array_keys($newvals));
        }

        return 'NULL';
    }

    /**
     * @param $models
     * @param $key
     * @return array
     */
    public static function modelKeyAsArray($models, $key)
    {
        foreach ($models as $model) {
            $returnData[] = $model->{$key};
        }

        return isset($returnData) ? $returnData : [];
    }
}
