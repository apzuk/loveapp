<?php

class Date
{
    /**
     * Validate MySQL DateTime
     * @param string $date
     * @return bool
     */
    public static function validateDateTime($date)
    {
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date)
            || preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/", $date)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Return current DateTime
     * @return string
     */
    public static function now()
    {
        date_default_timezone_set('Europe/Moscow');
        return date('Y-m-d H:i:s');
    }

    /**
     * @param $date
     * @param string $format
     * @return string
     */
    public static function dateWithLabel($date, $format = 'dd MMM')
    {
        return Format::date($date, $format);
    }

    /**
     * @param $date
     * @param $format
     * @return string
     */
    public static function dateTodayYesterday($date, $format = 'dd MMM')
    {
        if (date('Y-m-d', strtotime($date)) == date('Y-m-d')) {
            return 'Сегодня';
        }
        if (date('Y-m-d', strtotime($date . ' + 1 days')) == date('Y-m-d')) {
            return 'Вчера';
        }

        return self::dateWithLabel($date, $format);
    }

    public static function dateTodayYesterdayWithTime($date, $format = 'dd MMM')
    {
        if (date('Y-m-d', strtotime($date)) == date('Y-m-d')) {
            return 'Сегодня в ' . date('H:i', strtotime($date));
        }
        if (date('Y-m-d', strtotime($date . ' + 1 days')) == date('Y-m-d')) {
            return 'Вчера в ' . date('H:i', strtotime($date));
        }

        return self::dateWithLabel($date, $format);
    }

    /**
     * @param $count
     * @return mixed
     */
    public static function monthsFromCurrent($count)
    {
        setlocale(LC_ALL, 'ru_RU.UTF8', 'rus_RUS.UTF8', 'Russian_Russia.UTF8');

        $currentDate = date('Y-m-d');
        $returnData[$currentDate] = strftime('%B', strtotime($currentDate));

        foreach (range(1, $count - 1) as $iter) {
            $date = date('Y-m-d', strtotime("+" . $iter . " months", strtotime($currentDate)));
            $returnData[$date] = strftime('%B', strtotime($date));
        }

        return $returnData;
    }
}
