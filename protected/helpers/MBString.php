<?php

class MBString
{
    /**
     * @param $str
     * @param $start
     * @param $end
     * @param bool $withPoints
     * @return string
     */
    public static function cut($str, $start, $end, $withPoints = false)
    {
        $newString = mb_substr($str, $start, $end, mb_detect_encoding($str));

        if ($withPoints && mb_strlen($str) > ($end - $start)) {
            return $newString . '...';
        }

        return $newString;
    }
}