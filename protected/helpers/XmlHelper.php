<?php

class XmlHelper
{
    public static function paymentRates()
    {
        $path = Yii::app()->getRuntimePath() . '/paymentTariff.xml';
        if (!file_exists($path)) {
            throw new Exception('Application component not found');
        }

        $xml = simplexml_load_file($path);
        if (!$xml->vk) {
            return array();
        }

        foreach ($xml->vk->rate as $item) {
            $item->label = str_replace('{n}', $item->cost, $item->label);
            $orph = $item->cost == 1 ? '' : ($item->cost <= 3 ? 'а' : 'ов');
            $item->label = str_replace('{orph}', $orph, $item->label);
        }

        return $xml->vk->rate;
    }
} 