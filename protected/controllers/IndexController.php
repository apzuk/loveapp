<?php

class IndexController extends FrontController
{
    public function beforeAction($action)
    {
        $actions = ['parseImages', 'test', 'tmpl', 'gearman'];
        if (in_array($action->id, $actions)) {
            $this->noSocialInitialisation = true;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $user_id = $this->user()->user_id;

        // Notifications
        $nCount = Notification::model()->userId($user_id)->count('status="pending" AND t.create_ts <= NOW()');
        $notifications = Notification::model()
            ->with('tmpl')
            ->with('ownerUser')
            ->limit(13)
            ->offset(0)
            ->order('t.status="pending" DESC, t.create_ts', 'DESC')
            ->userId($this->user()->user_id)
            ->findAll('t.create_ts <= NOW()');

        // Messages
        $messages = ImMessage::model()
            ->with('receiverUser')
            ->with('senderUser')
            ->order('t.create_ts', 'desc')
            ->groupBy('conv_id')
            ->findAll(
                '(sender_user_id=:u AND sender_status!="remove") OR (receiver_user_id=:u AND receiver_status!="remove")',
                [
                    ':u' => $user_id
                ]
            );
        $mCount = ImMessage::model()->receiverUserId($user_id)->count('receiver_status="unread"');

        // Awards
        $award = new Award($this->user());
        $award->getAwards();

        // Rating history
        $ratings = UserRatingHistory::model()
            ->userId($this->user()->user_id)
            ->limit(13)
            ->offset(0)
            ->findAll('count!=0');

        $this->render(
            'index',
            [
                'notificationCount' => $nCount,
                'notifications' => $notifications,
                'messages' => $messages,
                'messagesCount' => $mCount,
                'award' => $award,
                'ratings' => $ratings
            ]
        );
    }

    public function actionTest()
    {
        $this->renderPartial('test');
    }

    public function actionTmpl()
    {
        $this->renderPartial('tmpl');
    }

    public function actionParseImages()
    {
        foreach (UserAccount::model()->findAll() as $user) {
            $user->setVkAvatar();
            echo $user->username . ': done! <br />';
            flush();
        }
    }
}