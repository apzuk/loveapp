<?php

class m140829_195929_refactor_user_leader extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE log_leader CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;");
	}

	public function down()
	{
		echo "m140829_195929_refactor_user_leader does not support migration down.\n";
		return false;
	}
}