<?php

class m141116_174005_refactor_user_stat extends CDbMigration
{
    public function up()
    {
        $this->addColumn('user_statistic', 'spent_money', 'decimal');
        $this->addColumn('user_statistic', 'leader_count', 'int(11)');
        $this->addColumn('user_statistic', 'ribbon_count', 'int(11)');
        $this->addColumn('user_statistic', 'messages_count', 'int(11)');
        $this->addColumn('user_statistic', 'kissed_count', 'int(11)');
        $this->addColumn('user_statistic', 'vip_time', 'decimal');
        $this->addColumn('user_statistic', 'curious_count', 'INT(11)');
    }

    public function down()
    {
        echo "m141116_174005_refactor_user_stat does not support migration down.\n";
        return false;
    }
}