<?php

class m141117_185126_create_user_award_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'user_award',
            array(
                'id' => 'pk',
                'award' => 'INT(11)',
                'step' => 'INT(11) NOT NULL DEFAULT 1',
                'is_viewed' => 'INT(11) NOT NULL DEFAULT 0',
                'user_id' => 'INT(11) NOT NULL'
            ),
            'ENGINE=InnoDB CHARSET=utf8'
        );

        $this->createIndex(
            'award_user_id_step_UNQUE',
            'user_award',
            'user_id,step,award',
            true
        );

        $this->addForeignKey(
            'FK_user_award_user_id_user_account_user_id',
            'user_award',
            'user_id',
            'user_account',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m141117_185126_create_user_award_table does not support migration down.\n";
        return false;
    }
}