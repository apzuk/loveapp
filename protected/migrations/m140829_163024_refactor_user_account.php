<?php

class m140829_163024_refactor_user_account extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user_account', 'socialScreenName', 'varchar(255)');
	}

	public function down()
	{
		echo "m140829_163024_refactor_user_account does not support migration down.\n";
		return false;
	}
}