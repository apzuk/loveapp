<?php

class m140922_122029_game_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'game',
            array(
                'game_id' => 'pk',
                'status' => 'INT(11) NOT NULL DEFAULT 0',
                'json' => 'LONGTEXT'
            ),
            'ENGINE=InnoDB CHARSET=utf8'
        );

        $this->createTable(
            'game_user',
            array(
                'id' => 'pk',
                'user_id' => 'INT(11) NOT NULL',
                'game_id' => 'INT(11) NOT NULL',
                'rating' => 'INT(11) NOT NULL DEFAULT 0'
            ),
            'ENGINE=InnoDB CHARSET=utf8'
        );

        $this->addForeignKey(
            'game_user_user_id_FK_user_account_user_id',
            'game_user',
            'user_id',
            'user_account',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'game_user_game_id_FK_game_game_id',
            'game_user',
            'game_id',
            'game',
            'game_id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m140922_122029_game_table does not support migration down.\n";
        return false;
    }
}