<?php

class m140829_153151_refactor_user_account extends CDbMigration
{
    public function up()
    {
        $this->addColumn('user_account', 'social', 'INT(11) NOT NULL DEFAULT ' . UserAccount::TYPE_VK . ' AFTER soc_id');
    }

    public function down()
    {
        echo "m140829_153151_refactor_user_account does not support migration down.\n";
        return false;
    }
}