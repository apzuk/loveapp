<?php

class m141121_132345_insertNewNotification extends CDbMigration
{
    public function up()
    {
        $this->insert(
            'notification_tmpl',
            array(
                'name' => 'award',
                'tmpl' => 'Вы получили награду <a href="sD.showAward([award])">"[awardTitle]"</a>'
            )
        );
    }

    public function down()
    {
        echo "m141121_132345_insertNewNotification does not support migration down.\n";
        return false;
    }
}