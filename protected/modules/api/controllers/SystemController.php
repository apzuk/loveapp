<?php

class SystemController extends ApiController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + getCities'
        ];
    }

    public function actionGetCities()
    {
        $country_id = app()->request->getPost('country_id');
        $term = app()->request->getPost('term');

        $cities = SysCities::model()
            ->countryId($country_id)
            ->search($term, 't.city')
            ->offset(0)
            ->order('id', 'ASC');

        if (!$term) {
            $cities = $cities->findAll('important=1');
        } else {
            $cities = $cities->limit(50)->findAll();
        }

        $data = [];
        foreach ($cities as $city) {
            $data[] = [
                'id' => $city->id,
                'value' => $city->city
            ];
        }

        $this->response(['data' => $data]);
    }
} 