<?php

class LeaderController extends ApiController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + becomeLeader'
        ];
    }

    public function actionGet()
    {
        // Current user
        $user = $this->user();

        $city_id = app()->request->getPost('city_id', $user->city_id);

        $cityLeader = UserLeader::model()->with('user')->cityId($city_id)->find();
        if (!$cityLeader) {
            $this->response(
                array_merge(
                    $user->publicData(),
                    [
                        'result' => false,
                        'current_bid' => 10,
                        'message' => "Попасть сюда"
                    ]
                )
            );
        }

        $this->response(
            array_merge(
                $cityLeader->user->publicData(),
                [
                    'result' => true,
                    'current_bid' => ++$cityLeader->current_bid * 10,
                    'message' => $cityLeader->message
                ]
            )
        );
    }

    public function actionSet()
    {
        // Current user
        $user = $this->user();

        // Selected city
        $city_id = $user->city_id;

        // Looking for current leader of the city
        $cityLeader = UserLeader::model()->cityId($city_id)->find();
        if (!$cityLeader) {
            $cityLeader = new UserLeader();
            $cityLeader->current_bid = 0;
        }

        // Withdraw for operation!
        $this->withdraw($user, ++$cityLeader->current_bid);

        $this->transaction(
            function () use ($user, $city_id, &$cityLeader) {
                $user->stat()->leader_count++;

                // Save withdraw
                $user->save();

                // Notify city leader about new leader
                if ($cityLeader->leader_id) {
                    Notification::set(7, $cityLeader->user_id, $user->user_id);
                }

                // Change leader
                $cityLeader->city_id = $city_id;
                $cityLeader->user_id = $user->user_id;
                $cityLeader->message = app()->request->getPost('message');

                // Set new leader
                $cityLeader->save();
            },
            function () {
            }
        );

        if (!$cityLeader->leader_id) {
            $this->errors($cityLeader);
        }


        $data = array_merge(
            $user->publicData(),
            [
                'result' => true,
                'current_bid' => ++$cityLeader->current_bid * 10,
                'message' => $cityLeader->message
            ]
        );
        app()->websocket->send('city' . $cityLeader->city_id . 'Leader', $data);

        $this->response(['result' => true]);
    }
} 