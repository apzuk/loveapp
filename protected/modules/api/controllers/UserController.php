<?php

class UserController extends ApiController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + becomeVip',
            'ajaxOnly + postOnly + setSkin',
            'ajaxOnly + postOnly + get',
            'ajaxOnly + postOnly + like',
            'ajaxOnly + postOnly + kiss',
            'ajaxOnly + postOnly + choose',
            'ajaxOnly + postOnly + becomeSponsor',
            'ajaxOnly + postOnly + toggleVisibility',
            'ajaxOnly + postOnly + suggested',
            'ajaxOnly + postOnly + setvkavatar',
            'ajaxOnly + postOnly + sendMoney',
            'ajaxOnly + postOnly + ratingList',
            'ajaxOnly + postOnly + getRandom',
        ];
    }

    public function actionGetRandom()
    {
        $users = UserRand::model()->with('user')->findAll('t.status="new"');

        $data = [];
        foreach ($users as $user) {
            $data[] = $user->user->publicData() + ['class_name' => '000'];
        }

        $chunks = array_chunk($data, 15);
        $this->response(['list1' => $chunks[0], 'list2' => $chunks[1]]);
    }

    public function actionRatingList()
    {
        $index = app()->request->getPost('index');
        $offset = app()->request->getPost('offset');
        $limit = 21;

        $users = UserAccount::getUsersOrderedBy($index, $offset * $limit, $limit);

        $htmlContent = $this->renderPartial(
            '//rating/list',
            [
                'users' => $users,
                'offset' => $offset
            ],
            true,
            false
        );

        $this->response(['htmlContent' => $htmlContent]);
    }

    public function actionSendMoney()
    {
        $user = $this->user();

        $target_user_id = app()->request->getPost('target_user_id');
        if (null === ($targetUser = (UserAccount::model()->with('stat')->findByPk($target_user_id)))) {
            $this->pageNotFound();
        }

        $count = (int)app()->request->getPost('count');
        if (!$count || $count < 10) {
            $count = 10;
        }

        $count = (float)($count / 10);
        $this->withdraw($user, $count);

        $result = $this->transaction(
            function () use (&$user, &$targetUser, &$count) {
                // Save user changes
                $user->save();

                // Add balance to target user
                $targetUser->balance += $count;
                $targetUser->save();

                // Notify
                Notification::set(
                    Notification::SEND_MONEY,
                    $targetUser->user_id,
                    $user->user_id,
                    ['count' => $count * 10]
                );

                return true;
            },
            function () {
                return false;
            }
        );

        $this->response(['result' => $result]);
    }

    public function actionBecomeVip()
    {
        $target_user = $user = $this->user();

        if ($user_id = app()->request->getPost('user_id')) {
            $target_user = UserAccount::model()->findByPk($user_id);
        }

        $mode = app()->request->getPost('mode', 3);
        $costs = [3, 10, 25];

        // VIP cost
        $cost = $costs[($mode - 1)];

        // Withdraw for operation!
        $this->withdraw($user, $cost);


        $result = $this->transaction(
            function () use ($user, $target_user, $mode) {
                // Make user VIP
                $target_user->makeVip($mode);

                $days = [1, 7, 30];
                $target_user->stat()->vip_time += $days[$mode];

                // Save users changes!
                $target_user->save();

                // Save balance
                $user->save();

                // Notify
                if ($target_user->user_id !== $user->user_id) {
                    $labels = ['t1' => 'На день', 't7' => 'На неделю', 't30' => "на месяц"];
                    $data = ['duration' => $labels['t' . $days[$mode]]];
                    Notification::set(Notification::DONATE_VIP, $target_user->user_id, $user->user_id, $data);
                }

                return true;
            },
            function () {
                return false;
            }
        );

        $this->response(
            [
                'result' => $result,
                'self' => $target_user->user_id == $user->user_id,
                'user' => $user->publicData()
            ]
        );
    }

    public function actionSetSkin()
    {
        // Current user
        $user = $this->user();

        // If user is not VIP, stop operation!
        if (!$user->isVip()) {
            $this->response(['result' => false]);
        }

        // Skin!
        $skin = app()->request->getPost('skin', 1);
        if ($skin > 5) {
            $skin = 5;
        }

        $user->color = $skin;
        $user->save(false);

        $this->response(['result' => true, 'user' => $user->publicData()]);
    }

    public function actionGet()
    {
        // Required user id
        $user_id = app()->request->getPost('user_id');

        // Response type html either json ( htlm by default )
        $type = app()->request->getPost('responseType', 'html');

        // Select required user from db!
        $user = UserAccount::model()
            ->with('retinues.target')
            ->with('stat')
            ->with('city')
            ->findByPk($user_id);
        if ($user === null) {
            $this->pageNotFound();
        }

        // Is self profile?!
        if (!($self = $user_id == $this->user()->user_id)) {
            // Is current user liked required user?!
            $user->liked = UserLike::model()
                ->targetUserId($user_id)
                ->userId($this->user()->user_id)
                ->find();
        }

        // Required user sponsor
        $user->sponsor = UserSponsor::model()
            ->with('user')
            //->sponsorUserId($user_id)
            ->targetUserId($user_id)
            ->find();

        if (!$self) {
            if (!$this->user()->visibility()) {
                app()->websocket->send(
                    '#userUpdates' . $user->user_id,
                    [
                        'eventToFire' => 'accountViewed',
                        'user' => $this->user()->publicData()
                    ]
                );
            }
            $this->user()->stat()->curious_count++;
            $this->user()->save();
        }

        if ($type == 'html') {
            $this->response(
                [
                    'htmlContent' => $this->renderPartial(
                        '//user/profile',
                        [
                            'user' => $user,
                            'self' => $self
                        ],
                        true,
                        false
                    ),
                    'user' => $user->publicData(),
                    'title' => $user->title(),
                    'gender' => $user->gender()
                ]
            );
        }

        $this->response(['user' => $user->publicData()]);
    }

    public function actionLike()
    {
        $owner = $this->user();

        $target_user_id = app()->request->getPost('target_user_id');
        if (null === ($user = (UserAccount::model()->with('stat')->findByPk($target_user_id)))) {
            $this->pageNotFound();
        }

        if ($user->liked) {
            $this->pageNotFound();
        }

        $this->withdraw($owner, 2);

        $result = $this->transaction(
            function () use (&$user, &$owner) {
                // Withdraw money
                $owner->save();

                // Upgrade likes count
                $user->stat->likes++;
                $user->save();

                // Set notification for like
                Notification::set(
                    Notification::LIKE,
                    $user->user_id,
                    $owner->user_id
                );

                // Add new like
                $newLike = new UserLike();
                $newLike->user_id = $owner->user_id;
                $newLike->target_user_id = $user->user_id;
                return $newLike->save();
            },
            function () {
                return false;
            }
        );

        $this->response(
            [
                'result' => $result,
                'likes' => $user->stat->likes
            ]
        );
    }

    public function actionKiss()
    {
        $owner = $this->user();

        $target_user_id = app()->request->getPost('target_user_id');
        if (null === ($user = (UserAccount::model()->with('stat')->findByPk($target_user_id)))) {
            $this->pageNotFound();
        }

        $this->withdraw($owner, 2);

        $result = $this->transaction(
            function () use (&$user, &$owner) {
                // Upgrade kisses count
                $user->stat()->kiss++;

                // Upgrade kissed count
                $owner->stat()->kissed_count++;

                // Save changes!
                $user->save();
                $owner->save();

                // Set notification for kiss
                Notification::set(
                    Notification::KISS,
                    $user->user_id,
                    $owner->user_id
                );

                // Add new like
                $newLike = new LogUserKiss();
                $newLike->user_id = $owner->user_id;
                $newLike->kissed_user_id = $user->user_id;
                return $newLike->save();
            },
            function () {
                return false;
            }
        );

        $this->response(
            [
                'result' => $result,
                'kisses' => $user->stat->kiss
            ]
        );
    }

    public function actionChoose()
    {
        // Set choose
        $user = $this->user();

        // params!
        $user_id = app()->request->getPost('user_id');
        $type = app()->request->getPost('choose', null);

        $type = $type ? ChosenType::model()->findByPk($type) : null;

        $result = $this->transaction(
            function () use ($user, $type, $user_id) {
                // Set chosen
                $newChosen = new Chosen();
                $newChosen->user_id = $user->user_id;
                $newChosen->chosen_user_id = $user_id;
                $newChosen->chosen_type_id = $type ? $type->id : null;
                $newChosen->save();

                if ($type) {
                    $data = [
                        'label' => $type->title
                    ];
                    Notification::set(18, $newChosen->chosen_user_id, $newChosen->user_id, $data);
                }
                return true;
            },
            function () {
                return false;
            }
        );

        // Get next!
        $u = $user->getRandomToChoose();

        // Response
        $this->response(['user' => $u->publicData(), 'result' => $result]);
    }

    public function actionBecomeSponsor()
    {
        $user = $this->user();

        $target_user_id = app()->request->getPost('target_user_id');
        if (null === ($target_user = (UserAccount::model()->with('stat')->findByPk($target_user_id)))) {
            $this->pageNotFound();
        }

        // Withdraw монеы!
        $this->withdraw($user, $target_user->cost);

        $result = $this->transaction(
            function () use ($target_user, $user) {
                // Save withdraw
                $user->save();

                // Upgrade cost!
                $target_user->cost++;
                $target_user->save();

                $newpatron = new UserSponsor();
                $newpatron->sponsor_user_id = $user->user_id;
                $newpatron->target_user_id = $target_user->user_id;
                $newpatron->save();

                // Check award, DO NOT MOVE THIS TO UserStatistics
                $award6 = new Award6($user->gender, ++$user->stat()->retinue_count);
                $user->stat()->checkAward($award6);

                return true;
            },
            function () {
                return false;
            }
        );

        $this->response(['result' => $result]);
    }

    public function actionToggleVisibility()
    {
        $user = $this->user();
        $user->visibility = $user->visibility == 0 ? 1 : 0;

        $this->response(['result' => $user->save(), 'visibility' => $user->visibility]);
    }

    public function actionSuggested()
    {
        $this->response(
            [
                'htmlContent' => $this->renderPartial('//messages/_sidebarRight', [], true, false)
            ]
        );
    }

    public function actionSetvkavatar()
    {
        $user = $this->user();
        $user->setVkAvatar();

        $this->response(['user' => $user->publicData()]);
    }
}