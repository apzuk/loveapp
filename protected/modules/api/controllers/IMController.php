<?php

class IMController extends ApiController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + write',
            'ajaxOnly + postOnly + load',
            'ajaxOnly + postOnly + makeAsRead',
            'ajaxOnly + postOnly + uploadPhoto',
            'ajaxOnly + postOnly + deleteMessage'
        ];
    }

    public function actionWrite()
    {
        $user = $this->user();

        $to = app()->request->getPost('to');
        $message = app()->request->getPost('message');
        $attachment = app()->request->getPost('attachment');

        if (!$user->isVip()) {
            $attachment = [];
        }

        $newMessage = new ImMessage();
        $newMessage->sender_user_id = $user->user_id;
        $newMessage->receiver_user_id = $to;
        $newMessage->message = preg_replace(
            '/<img src="\/images\/vk\/default\/vk(\d+).png"\/?>/',
            'vk$1.png',
            $message
        );
        $newMessage->message = preg_replace('/<br \/?>/', '$1', trim($newMessage->message));
        $newMessage->attachment = $attachment ? [1 => $attachment] : [];

        if ($newMessage->save()) {

            $user->stat()->messages_count++;
            $user->save();

            $data1 = array_merge(
                $user->publicData(),
                [
                    'companionId' => $newMessage->receiver_user_id,
                    'messageHtml' => $this->renderPartial(
                        '//messages/_msg',
                        ['msg' => $newMessage, 'user' => $user],
                        true,
                        false
                    ),
                    'messageJson' => json_encode($newMessage->attributes)
                ]
            );
            app()->websocket->send('#messageChannel' . $newMessage->sender_user_id, $data1);

            $data2 = array_merge(
                $user->publicData(),
                [
                    'companionId' => $newMessage->sender_user_id,
                    'messageHtml' => $this->renderPartial(
                        '//messages/_msg',
                        ['msg' => $newMessage, 'user' => $user],
                        true,
                        false
                    ),
                    'messageJson' => json_encode($newMessage->attributes)
                ]
            );
            app()->websocket->send('#messageChannel' . $newMessage->receiver_user_id, $data2);
        }
    }

    public function actionLoad()
    {
        $user = $this->user();

        $to = app()->request->getPost('to');

        $convId = ImMessage::getConvId($user->user_id, $to);
        if (!$convId) {
            $this->response(['htmlContent' => '']);
        }

        $offset = app()->request->getPost('offset');

        $messages = ImMessage::model()
            ->convId($convId)
            ->limit(25)
            ->offset($offset * 25)
            ->order('message_id', 'desc')
            ->findAll(
                '(sender_user_id=:u AND sender_status!="remove") OR (receiver_user_id=:u AND receiver_status!="remove")',
                [
                    ':u' => $user->user_id
                ]
            );

        $this->response(
            [
                'htmlContent' => $this->renderPartial(
                    '//messages/_msgList',
                    [
                        'messages' => array_reverse($messages)
                    ],
                    true,
                    false
                )
            ]
        );
    }

    public function actionMakeAsRead()
    {
        $msg_id = app()->request->getPost('msgId');

        $msg = ImMessage::model()->findByPk($msg_id);
        if (null == $msg) {
            $this->pageNotFound();
        }

        if ($msg->receiver_user_id != $this->user()->user_id) {
            $this->accessDenied();
        }

        $msg->receiver_status = "read";
        $msg->save();

        app()->websocket->send(
            '#messageChannel' . $msg->sender_user_id,
            ['msg_id' => $msg_id, 'action' => 'changeMessageStatus']
        );
        //  app()->websocket->send('#messageChannel' . $msg->receiver_user_id, ['msg_id' => $msg_id, 'action' => 'changeMessageStatus']);
    }

    public function actionUploadPhoto()
    {
        if (!isset($_FILES['files'])) {
            $this->response(['result' => false]);
        }

        $file = $_FILES['files'];

        $path = WEBROOT . '/public/uploads/msg';
        if (!is_dir($path)) {
            mkdir($path, 0775);
        }

        $filename = md5(time() . $file['name'][0]) . '.jpg';
        $fullPath = $path . '/' . $filename;
        $isMoved = move_uploaded_file($file['tmp_name'][0], $fullPath);
        if ($isMoved) {
            $i = new Imagick();
            try {
                $i->pingImage($fullPath);
                $i = new Imagick($fullPath);
            } catch (ImagickException $e) {
                unset($fullPath);
                $this->response(['result' => false]);
            }

            $i->resizeimage(1024, 840, Imagick::FILTER_LANCZOS, 1, true);
            $i->writeimage($fullPath);

            $this->response(
                [
                    'filename' => $filename,
                    'path_80' => '/uploads/msg/' . Files::resize($filename, 80, 80)
                ]
            );
        }
    }

    public function actionDeleteMessage()
    {
        $msg_id = app()->request->getPost('msg_id');

        if (($msg = ImMessage::model()->findByPk($msg_id)) === null) {
            $this->pageNotFound();
        }

        if ($msg->isSender($this->user()->user_id)) {
            $msg->sender_status = 'remove';
        }
        if ($msg->isReceiver($this->user()->user_id)) {
            $msg->receiver_status = 'remove';
        }

        $msg->save(false);
    }
}