<?php

class AwardController extends ApiController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + getAwardView'
        ];
    }

    public function actionGetAwardView()
    {
        // Award index
        $index = app()->request->getPost('index');

        $oClass = new ReflectionClass ('Award');
        if (!in_array($index, $oClass->getConstants())) {
            $this->pageNotFound();
        }

        $award = new Award($this->user());
        $award = $award->getAward($index);

        $this->response(
            [
                'htmlContent' => $this->renderPartial('//index/_awardView', ['award' => $award], true, false)
            ]
        );
    }
}