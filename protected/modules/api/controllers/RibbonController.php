<?php

class RibbonController extends FrontController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + set',
            'ajaxOnly + postOnly + load',
        ];
    }

    public function actionSet()
    {
        $user = $this->user();

        // Comment!
        $text = app()->request->getPost('text');

        // Withdraw money
        $this->withdraw($user, 2);

        // To the ribbon list!
        $ribbon = $this->transaction(
            function () use ($user, $text) {
                $user->stat()->ribbon_count++;

                // Save user!
                $user->save();

                $newRibbon = new UserRibbon();
                $newRibbon->user_id = $user->user_id;
                $newRibbon->comment = $text;
                $newRibbon->save();

                return $newRibbon;
            },
            function () {
                return false;
            }
        );

        // If failed
        if (!$ribbon) {
            $this->response(['result' => false]);
        }

        app()->websocket->send(
            'ribbon',
            [
                'result' => true,
                'ribbon' => json_encode($ribbon->attributes),
                'user' => json_encode($user->publicData())
            ]
        );

        // Success response
        $this->response(
            [
                'result' => true
            ]
        );
    }

    public function actionLoad()
    {
        $ribbons = UserRibbon::model()
            ->with('user')
            ->limit(15)
            ->offset(0)
            ->order('id', 'DESC')
            ->findAll();

        $data = [];
        foreach ($ribbons as $ribbon) {
            $data[] = [
                'ribbon' => $ribbon->attributes,
                'user' => $ribbon->user->publicData()
            ];
        }

        $this->response(['ribbons' => $data]);
    }
} 