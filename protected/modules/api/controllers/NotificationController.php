<?php

class NotificationController extends FrontController
{
    public function filters()
    {
        return [
            'ajaxOnly + postOnly + delete',
            'ajaxOnly + postOnly + load',
            'ajaxOnly + postOnly + setGotStatus',
            'ajaxOnly + postOnly + getUpdates'
        ];
    }

    public function actionLoad()
    {
        $user = $this->user();
        $offset = app()->request->getPost('offset');

        $notifications = Notification::model()
            ->with('tmpl')
            ->with('ownerUser')
            ->limit(13)
            ->offset($offset * 13)
            ->order('t.status="pending" DESC, t.create_ts', 'DESC')
            ->userId($user->user_id)
            ->findAll();

        $html = $this->renderPartial(
            '//notifications/_notificationsList',
            [
                'notifications' => $notifications
            ],
            true,
            false
        );

        $this->response(['htmlContent' => $html]);
    }

    public function actionDelete()
    {
        $n_id = app()->request->getPost('n_id');

        $n = Notification::model()->userId($this->user()->user_id)->findByPk($n_id);

        if ($n === null) {
            $this->pageNotFound();
        }

        $this->response(['result' => $n->delete()]);
    }

    public function actionSetGotStatus()
    {
        $n_id = app()->request->getPost('n_id');

        $n = Notification::model()->userId($this->user()->user_id)->findByPk($n_id);

        if ($n === null) {
            $this->pageNotFound();
        }

        if ($n->status == 'got') {
            $this->response(['result' => false]);
        }

        $n->status = 'got';
        $this->response(['result' => $n->save()]);
    }

    public function actionGetUpdates()
    {
        $user = $this->user();

        $notifications = Notification::model()
            ->with('tmpl')
            ->with('ownerUser')
            ->order('t.create_ts', 'DESC')
            ->userId($user->user_id)
            ->findAll('t.status="pending" AND t.create_ts <= NOW()');

        $html = $this->renderPartial(
            '//notifications/_notificationsList',
            [
                'notifications' => $notifications,
                'count' => count($notifications)
            ],
            true,
            false
        );

        $this->response(['htmlContent' => $html]);
    }
}