<?php

class UserLike extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_like';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, target_user_id', 'required'),
            array('user_id, target_user_id', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'targetUser' => array(self::BELONGS_TO, 'UserAccount', 'target_user_id'),
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'target_user_id' => 'Target User',
        );
    }

    /**
     * @param string $className active record class name.
     * @return UserLike the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function targetUserId($user_id)
    {
        if ($user_id !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.target_user_id = :target_user_id',
                    'params' => [':target_user_id' => $user_id],
                ]
            );
        }

        return $this;
    }
}
