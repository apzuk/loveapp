<?php

class UserStatistic extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_statistic';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'game_count, rating, kiss, likes, dislikes, retinue_count, user_id, kiss',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'statistic_id, game_count, rating, kiss, likes, dislikes, retinue_count, user_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'statistic_id' => 'Statistic',
            'game_count' => 'Game Count',
            'rating' => 'Rating',
            'kiss' => 'Kiss',
            'likes' => 'Likes',
            'dislikes' => 'Dislikes',
            'retinue_count' => 'Retinue Count',
            'user_id' => 'User',
        );
    }

    /**
     * @param string $className active record class name.
     * @return UserStatistic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind()
    {
        $this->attrs = $this->attributes;
    }

    public function afterSave()
    {
        if ($this->hasChanged('game_count')) {
            $award = new Award1($this->user->gender, $this->game_count);
        }
        if ($this->hasChanged('kiss')) {
            $award = new Award10($this->user->gender, $this->kiss);
        }
        if ($this->hasChanged('kissed_count')) {
            $award = new Award9($this->user->gender, $this->kissed_count);
        }
        if ($this->hasChanged('spent_money')) {
            $award = new Award3($this->user->gender, $this->spent_money);
        }
        if ($this->hasChanged('curious_count')) {
            $award = new Award12($this->user->gender, $this->curious_count);
        }
        if ($this->hasChanged('ribbon_count')) {
            $award = new Award5($this->user->gender, $this->ribbon_count);
        }
        if ($this->hasChanged('game_count')) {
            $award = new Award1($this->user->gender, $this->game_count);
        }
        if ($this->hasChanged('leader_count')) {
            $award = new Award4($this->user->gender, $this->leader_count);
        }
        if ($this->hasChanged('likes')) {
            $award = new Award2($this->user->gender, $this->likes);
        }
        if ($this->hasChanged('messages_count')) {
            $award = new Award7($this->user->gender, $this->messages_count);
        }
        if ($this->hasChanged('vip_time')) {
            $award = new Award11($this->user->gender, $this->vip_time);
        }
        if ($this->hasChanged('kissed_count')) {
            $award = new Award10($this->user->gender, $this->kissed_count);
        }
        if ($this->hasChanged('kiss')) {
            $award = new Award9($this->user->gender, $this->kiss);
        }
        if ($this->hasChanged('rating')) {
            $award = new Award8($this->user->gender, $this->rating);
        }


        if (isset($award)) {
            $this->checkAward($award);
        }
    }

    /**
     * Check if award ready
     * @param AwardBase $award
     */
    public function checkAward(AwardBase $award)
    {
        $className = get_class($award);
        $className = (int)preg_replace('/\D+/', '', $className);

        if (!in_array($className, range(1, 12))) {
            return;
        }

        if ($award->isReady()) {
            $this->setAward($className, 2, $award->getTitle());
        } else {
            if ($award->isStep2()) {
                $this->setAward($className, 1, $award->getTitle());
            }
        }

        $awardData = [
            'tooltipTitle' => $award->getTooltipTitle(),
            'title' => $award->getTitle(),
            'percentage' => $award->percentage(),
            'index' => $award->index()
        ];

        app()->websocket->send(
            '#userUpdates' . $this->user->user_id,
            ['eventToFire' => 'updateAward'] + $awardData
        );
    }

    /**
     * Save user award
     * @param $award
     * @param $step
     */
    public function setAward($award, $step, $awardTitle)
    {
        $sql = [
            "INSERT INTO user_award SET award=:award, step=:step, user_id=:user_id",
            "ON DUPLICATE KEY UPDATE user_id=user_id"
        ];

        $sql = join(' ', $sql);
        app()->db->createCommand($sql)
            ->bindValue(':award', $award)
            ->bindValue(':step', $step)
            ->bindValue(':user_id', $this->user_id)
            ->execute();

        // Set notification
        $data = ['award' => $award, 'awardTitle' => $awardTitle];
        Notification::set(19, Application::$user_id, null, $data);
    }
}
