<?php

class Chosen extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chosen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, chosen_user_id', 'required'),
			array('user_id, chosen_user_id, chosen_type_id', 'numerical', 'integerOnly'=>true),
			array('id, user_id, chosen_user_id, chosen_type_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'chosenType' => array(self::BELONGS_TO, 'ChosenType', 'chosen_type_id'),
			'chosenUser' => array(self::BELONGS_TO, 'UserAccount', 'chosen_user_id'),
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'chosen_user_id' => 'Chosen User',
			'chosen_type_id' => 'Chosen Type',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return Chosen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
