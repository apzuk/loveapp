<?php

class UserRatingHistory extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_rating_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('count', 'numerical', 'integerOnly' => true),
            array('user_id', 'length', 'max' => 45),
            array('type', 'length', 'max' => 1),
            array('rating_type', 'length', 'max' => 7),
            array('id, user_id, count, type, rating_type, create_ts', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'count' => 'Count',
            'type' => 'Type',
            'rating_type' => 'Rating Type',
            'create_ts' => 'Create Ts',
        );
    }

    /**
     * @param string $className active record class name.
     * @return userRatingHistory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function type()
    {
        return $this->type == '-' ? 'negative' : 'positive';
    }

    public function label()
    {
        if ($this->rating_type == 'game') {
            return 'Рейтинг от игры';
        }
        if ($this->rating_type == 'refill') {
            return 'Рейтинг от пополнения кошелька';
        }
        if ($this->rating_type == 'retinue') {
            return 'Рейтинг от свиты';
        }
        if ($this->rating_type == 'patron') {
            return 'Рейтинг от покровителя';
        }
        return 'Рейтинг в подарок';
    }
}
