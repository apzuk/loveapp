<?php

class Files
{
    public static function resize($file, $w, $h)
    {
        if (!$file) {
            return '';
        }

        $path = WEBROOT . '/public/uploads/msg/' . $file;
        $resize = WEBROOT . '/public/uploads/msg/' . $w . 'x' . $h . '_' . $file;

        if (file_exists($resize)) {
            return $w . 'x' . $h . '_' . $file;
        }
        $i = new Imagick($path);
        $i->resizeimage($w, $h, Imagick::FILTER_LANCZOS, 1, true);
        $i->writeimage($resize);

        return $w . 'x' . $h . '_' . $file;
    }
}