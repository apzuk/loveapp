<?php

class UserAccount extends Model
{
    public $showCV = false;

    public $liked = null;
    public $sponsor = null;

    const TYPE_VK = 1;

    const ADMIN_ID = 1000;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_account';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'gender, online, city_id, visibility, cost, ref_id, continuously_visit, is_ready, is_fake, invitations, tourCount',
                'numerical',
                'integerOnly' => true
            ),
            array('username, soc_id, sid', 'length', 'max' => 255),
            array('avatar, avatar_orig', 'length', 'max' => 50),
            array('balance', 'length', 'max' => 5),
            array('color', 'length', 'max' => 100),
            array('show_profile', 'length', 'max' => 6),
            array('hash', 'length', 'max' => 45),
            array('status', 'length', 'max' => 7),
            array('birth_date, last_visit_date, vip_end, create_ts, tourCount, socialScreenName', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return [
            // User city
            'city' => array(self::BELONGS_TO, 'SysCities', 'city_id'),
            // User referral
            'ref' => array(self::BELONGS_TO, 'UserAccount', 'ref_id'),
            // User statistics
            'stat' => array(self::HAS_ONE, 'UserStatistic', 'user_id'),
            // Retinues
            'retinues' => array(self::HAS_MANY, 'UserSponsor', 'sponsor_user_id'),
            // awards
            'awards' => array(self::HAS_MANY, 'UserAward', 'user_id'),
            // Junk
            'chosens' => array(self::HAS_MANY, 'Chosen', 'chosen_user_id'),
            'chosens1' => array(self::HAS_MANY, 'Chosen', 'user_id'),
            'imMessages' => array(self::HAS_MANY, 'ImMessage', 'receiver_user_id'),
            'imMessages1' => array(self::HAS_MANY, 'ImMessage', 'sender_user_id'),
            'imReports' => array(self::HAS_MANY, 'ImReport', 'user_id'),
            'logLeaders' => array(self::HAS_MANY, 'LogLeader', 'user_id'),
            'logUserVips' => array(self::HAS_MANY, 'LogUserVip', 'user_id'),
            'logVkNotifs' => array(self::HAS_MANY, 'LogVkNotif', 'user_id'),
            'notifications' => array(self::HAS_MANY, 'Notification', 'owner_user_id'),
            'notifications1' => array(self::HAS_MANY, 'Notification', 'user_id'),
            'userAccounts' => array(self::HAS_MANY, 'UserAccount', 'ref_id'),
            'userBlacklists' => array(self::HAS_MANY, 'UserBlacklist', 'blocked_user_id'),
            'userBlacklists1' => array(self::HAS_MANY, 'UserBlacklist', 'user_id'),
            'userHasGroups' => array(self::HAS_MANY, 'UserHasGroup', 'user_id'),
            'userLeaders' => array(self::HAS_MANY, 'UserLeader', 'user_id'),
            'userRibbons' => array(self::HAS_MANY, 'UserRibbon', 'user_id'),
            'userSponsors' => array(self::HAS_MANY, 'UserSponsor', 'sponsor_user_id'),
            'userSponsors1' => array(self::HAS_MANY, 'UserSponsor', 'target_user_id'),
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'ID',
            'username' => 'Имя и фамилия',
            'avatar' => 'Аватар',
            'avatar_orig' => 'Avatar Orig',
            'gender' => 'Пол',
            'birth_date' => 'Дата рождения',
            'soc_id' => 'Soc',
            'online' => 'Online',
            'city_id' => 'Город',
            'balance' => 'Баланс',
            'sid' => 'Sid',
            'color' => 'Color',
            'visibility' => 'Visibility',
            'cost' => 'Cost',
            'show_profile' => 'Show Profile',
            'vip_end' => 'Vip End',
            'create_ts' => 'Create Ts',
            'ref_id' => 'Ref',
            'last_visit_date' => 'Last Visit Date',
            'continuously_visit' => 'Continuously Visit',
            'is_ready' => 'Is Ready',
            'is_fake' => 'Is Fake',
            'invitations' => 'Invitations',
            'hash' => 'Hash',
            'status' => 'Status',
            'tourCount' => 'Tour Count',
        );
    }

    /**
     * @param string $className active record class name.
     * @return UserAccount the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {

            if (!$this->city_id) {
                $this->city_id = 1;
            }

            $this->stat()->save();

            return true;
        }

        return false;
    }

    public function afterFind()
    {
        $this->attrs = $this->attributes;
    }

    public function afterSave()
    {
        if ($this->hasChanged('balance') || $this->stat->hasChanged('rating')) {
            app()->websocket->send(
                '#userUpdates' . $this->user_id,
                [
                    'balance' => $this->balance(),
                    'rating' => $this->stat->rating
                ]
            );
        }
    }

    public function movePhotoFiles()
    {
        // Check avatar
        if (!$this->avatar || !$this->avatar_orig || preg_match('/(deactivated|camera)/', $this->avatar)) {
            $this->avatar = $this->avatar_orig = '';
            return;
        }

        $newAvatar = md5($this->avatar . time() . rand(0, 15000)) . '.jpg';
        $newAvatarOriginal = md5($this->avatar_orig . time() . rand(0, 15000)) . '.jpg';

        $path = WEBROOT . '/public/uploads/avatar/';

        if (!file_exists($path . $newAvatar)) {
            @copy($this->avatar, $path . $newAvatar);
        }
        if (!file_exists($path . $newAvatarOriginal)) {
            @copy($this->avatar_orig, $path . $newAvatarOriginal);
        }

        $this->avatar = $newAvatar;
        $this->avatar_orig = $newAvatarOriginal;
    }

    public function avatar()
    {
        return $this->getAvatar('/uploads/avatar/' . $this->avatar);
    }

    public function avatar_orig()
    {
        return $this->getAvatar('/uploads/avatar/' . $this->avatar_orig);
    }

    public function getAvatar($path)
    {
        $fileFullPath = WEBROOT . '/public' . $path;

        if (file_exists($fileFullPath)) {
            return $path;
        }

        return $this->gender == 1 ? '/images/girl.png' : '/images/boy.png';
    }

    public function balance()
    {
        return $this->balance * 10;
    }

    public function isVip()
    {
        return strtotime(Date::NOW()) < strtotime($this->vip_end);
    }

    public function gender()
    {
        return $this->gender == 1 ? 'female' : 'male';
    }

    public function oppositeGender()
    {
        return $this->gender == 1 ? 'male' : 'female';
    }

    public function stat()
    {
        if ($this->stat instanceof CActiveRecord) {
            return $this->stat;
        }

        $stat = $this->stat = new UserStatistic();
        $stat->user_id = $this->user_id;
        $stat->save();

        return $stat;
    }

    /**
     * @return array
     */
    public function publicData()
    {
        $data = preg_split('/\s+/', $this->username);

        $fname = isset($data[0]) ? $data[0] : '';
        $lname = isset($data[1]) ? $data[1] : '';

        return [
            'username' => $this->username,
            'firstname' => $fname,
            'lastname' => $lname,
            'user_id' => (int) $this->user_id,
            'skin' => (int) $this->skin(),
            'age' => (int) $this->age(),
            'is_vip' => $this->isVip(),
            'avatar' => $this->avatar(),
            'vip_end' => Format::date($this->vip_end, "dd, MMMM"),
            'gender' => $this->gender(),
            'title' => $this->title(false),
            'titleFull' => $this->title(),
            'city' => $this->city->city,
            'liked' => $this->liked
        ];
    }

    /**
     * @return int
     */
    public function skin()
    {
        return $this->isVip() ? ($this->color ?: 1) : 1;
    }

    public function visibility()
    {
        return $this->isVip() ? $this->visibility : 0;
    }

    /**
     * @param $mode
     */
    public function makeVip($mode)
    {
        // Start date
        $startDate = Date::now();
        if (strtotime($this->vip_end) > strtotime(Date::now())) {
            $startDate = $this->vip_end;
        }

        // How many days to VIP
        $days = [1, 7, 30];
        $day = $days[$mode - 1];

        // Calculate VIP end date
        $this->vip_end = date('Y-m-d H:i:s', strtotime($startDate . " + " . $day . " days"));
        $this->setVIPStatusExpireNotification();
    }

    public function setVIPStatusExpireNotification()
    {
        if ($n = Notification::model()->tmplId(6)->status('pending')->find()) {
            $n->delete();
        }

        return Notification::set(6, $this->user_id, UserAccount::ADMIN_ID, [], $this->vip_end);
    }

    /**
     * @param bool $flag
     * @return mixed|string
     */
    public function title($flag = true)
    {
        $title = '';

        if ($flag) {
            $title = $this->username;
        }
        if ($this->city) {
            $title .= $flag ? ', ' : '' . $this->city->city;
        }
        if ($this->age()) {
            $title .= ', ' . $this->age() . ' лет';
        }

        return $title;
    }

    public function age()
    {
        $date = date('Y-m-d', strtotime($this->birth_date));

        $from = new DateTime($date);
        $to = new DateTime('today');

        return $from->diff($to)->y;
    }

    /**
     * @param $count
     */
    public function addRating($count)
    {
        $this->stat->rating += (int)$count;

        // Set rating history!
        $newHistory = new UserRatingHistory();
        $newHistory->count = $count;
        $newHistory->user_id = $this->user_id;
        $newHistory->rating_type = 'game';
        $newHistory->save();
    }

    public function setVisit()
    {
        if (!$this->last_visit_date) {
            $this->last_visit_date = Date::now();
        }

        $ld = date('Y-m-d', strtotime($this->last_visit_date));
        $td = date('Y-m-d');
        if ($ld == $td) {
            $this->last_visit_date = Date::now();
            return $this->save(false);
        }

        $this->showCV = true;

        $last_visit_date = date('Y-m-d', strtotime($this->last_visit_date));
        if ($last_visit_date == date('Y-m-d', strtotime("-1 days"))) {
            $this->continuously_visit++;
        } else {
            $this->continuously_visit = 1;
        }

        if ($this->continuously_visit >= 5) {
            $this->continuously_visit = 0;
            $earn = 2;
        } else {
            $earn = 0.5;
        }

        $this->last_visit_date = Date::now();
        $this->balance += $earn;

        return $this->save(false);
    }

    // SQL
    /**
     * @param $soc
     * @return $this
     */
    public function soc($soc)
    {
        if ($soc == 'vk') {
            $soc = UserAccount::TYPE_VK;
        }

        if ($soc !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.social = :social',
                    'params' => [':social' => $soc],
                ]
            );
        }

        return $this;
    }

    /**
     * @param $socId
     * @return $this
     */
    public function socId($socId)
    {
        if ($socId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.soc_id = :soc_id',
                    'params' => [':soc_id' => $socId],
                ]
            );
        }

        return $this;
    }

    public function cv()
    {
        if ($this->continuously_visit == 0) {
            return 4;
        }

        return $this->continuously_visit - 1;
    }

    public function getRandomToChoose()
    {
        // User chosen ids
        $ids = $this->getUserChosen();
        $ids[] = UserAccount::ADMIN_ID;

        // Find
        if (!$user = UserAccount::model()->find($this->getChooseCriteria($ids))) {
            $user = UserAccount::model()->find($this->getChooseCriteria($ids, false));
        }

        return $user ?: new UserAccount();
    }

    public function getChooseCriteria($ids, $flag = true)
    {
        $criteria = new CDbCriteria();

        // Main criteria
        $criteria->addCondition('gender=' . (int)($this->gender == 1 ? 2 : 1));

        if ($flag) {
            // From my city
            $criteria->addCondition('city_id=' . (int)$this->city_id);
        }

        // Exclude already chosen
        $criteria->addNotInCondition('user_id', $ids);

        return $criteria;
    }

    public function getUserChosen()
    {
        $chosen = Chosen::model()->userId($this->user_id)->findAll();
        return array_map(
            function ($c) {
                return $c->chosen_user_id;
            },
            $chosen
        );
    }

    public function getAward()
    {

    }

    public function setVkAvatar()
    {
        $user = Application::$wrapper->apiGetUser($this->soc_id);

        if ($user) {
            $this->avatar = $user->avatar;
            $this->avatar_orig = $user->avatar_orig;
            $this->save();
        }
    }

    public function socProfile()
    {
        return "http://vk.com/" . ($this->socialScreenName ? $this->socialScreenName : 'id' . $this->soc_id);
    }

    /**
     * @param $index
     * @param $offset
     * @param $limit
     */
    public static function getUsersOrderedBy($index, $offset, $limit)
    {
        $users = UserAccount::model()->with("stat")->offset($offset)->limit($limit);

        switch ($index) {
            case 0:
                $users = $users->order('stat.rating', 'DESC');
                break;
        }

        return $users->findAll();
    }
}
