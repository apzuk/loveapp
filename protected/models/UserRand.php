<?php

class UserRand extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_rand';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>3),
			array('id, user_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'status' => 'Status',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return UserRand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
