<?php

class ImMessage extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'im_message';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $purifier = new CHtmlPurifier();
        $purifier->options = [
            'HTML.Allowed' => 'br',
            'Attr.AllowedFrameTargets' => ['_blank'],
            'AutoFormat.RemoveEmpty' => true,
        ];


        return array(
            array('message', 'required'),
            array('message', 'filter', 'filter' => array($purifier, 'purify')),
            array('message', 'filter', 'filter' => 'trim'),
            array('sender_user_id, receiver_user_id', 'numerical', 'integerOnly' => true),
            array('sender_status, receiver_status', 'length', 'max' => 6),
            array('conv_id', 'length', 'max' => 125),
            array('message, attachment', 'safe'),
            array(
                'message_id, sender_user_id, receiver_user_id, message, sender_status, receiver_status, attachment, create_ts, conv_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'receiverUser' => array(self::BELONGS_TO, 'UserAccount', 'receiver_user_id'),
            'senderUser' => array(self::BELONGS_TO, 'UserAccount', 'sender_user_id'),
            'imReports' => array(self::HAS_MANY, 'ImReport', 'message_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'message_id' => 'Message',
            'sender_user_id' => 'Sender User',
            'receiver_user_id' => 'Receiver User',
            'message' => 'Message',
            'sender_status' => 'Sender Status',
            'receiver_status' => 'Receiver Status',
            'attachment' => 'Attachment',
            'create_ts' => 'Create Ts',
            'conv_id' => 'Conv',
        );
    }

    /**
     * @param string $className active record class name.
     * @return ImMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {

            if ($this->isNewRecord) {
                $this->create_ts = Date::now();
            }

            $convId = $this->getConversationId();
            $this->conv_id = $convId ?: $this->generateConversationId();

            if (is_array($this->attachment)) {
                $this->attachment = json_encode($this->attachment);
            }

            return true;
        }

        return false;
    }

    /**
     * @param null $userId
     * @return $this
     */
    public function senderUserId($userId = null)
    {
        if ($userId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.sender_user_id = :sender_user_id',
                    'params' => [':sender_user_id' => $userId],
                ]
            );
        }

        return $this;
    }

    /**
     * @param null $userId
     * @return $this
     */
    public function receiverUserId($userId = null)
    {
        if ($userId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.receiver_user_id = :receiver_user_id',
                    'params' => [':receiver_user_id' => $userId],
                ]
            );
        }

        return $this;
    }

    /**
     * @param null $convId
     * @return $this
     */
    public function convId($convId = null)
    {
        if ($convId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.conv_id = :conv_id',
                    'params' => [':conv_id' => $convId],
                ]
            );
        }

        return $this;
    }

    public function companion()
    {
        if (Model::$user_id == $this->senderUser->user_id) {
            return $this->receiverUser;
        }

        return $this->senderUser;
    }

    public function getConversationId()
    {
        return self::getConvId($this->sender_user_id, $this->receiver_user_id);
    }

    public function generateConversationId()
    {
        return md5($this->sender_user_id . $this->receiver_user_id);
    }

    public function message()
    {
        $msg = preg_replace('/vk(\d+).png/', '<img src="/images/vk/default/vk$1.png" />', $this->message);

        return str_replace('&nbsp;', '<br />', $msg);
    }

    public function date()
    {
        $today = strtotime(date('Y-m-d'));
        $pubDate = strtotime(date('Y-m-d', strtotime($this->create_ts)));

        if ($today === $pubDate) {
            return Format::date($this->create_ts, "HH:mm"); // . ' сегодня';
        }
        if ($today === ($pubDate + 24 * 60 * 60)) {
            return Format::date($this->create_ts, "HH:mm, ") . ' вчера';
        }

        return $this->create_ts;
    }

    public static function getConvId($u1, $u2)
    {
        $condition = '(sender_user_id=:u1 AND receiver_user_id=:u2) OR (sender_user_id=:u2 AND receiver_user_id=:u1)';

        $msg = ImMessage::model()
            ->find(
                $condition,
                [
                    ':u1' => $u1,
                    ':u2' => $u2
                ]
            );

        if ($msg) {
            return $msg->conv_id;
        }

        return null;
    }

    public static function icons()
    {
        $path = WEBROOT . '/public/images/vk/icondef.xml';
        $xml = simplexml_load_file($path);

        return $xml;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function isSender($user_id)
    {
        return $user_id == $this->senderUser->user_id;
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function isReceiver($user_id)
    {
        return $user_id == $this->receiverUser->user_id;
    }
}
