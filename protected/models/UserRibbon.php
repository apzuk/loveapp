<?php

class UserRibbon extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_ribbon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('comment', 'length', 'max'=>145),
			array('id, user_id, date, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'date' => 'Date',
			'comment' => 'Comment',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return UserRibbon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
