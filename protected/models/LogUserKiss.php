<?php

class LogUserKiss extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_user_kiss';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, kissed_user_id', 'required'),
			array('user_id, kissed_user_id', 'numerical', 'integerOnly'=>true),
			array('log_id, user_id, kissed_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'kissedUser' => array(self::BELONGS_TO, 'UserAccount', 'kissed_user_id'),
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'user_id' => 'User',
			'kissed_user_id' => 'Kissed User',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return LogUserKiss the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
