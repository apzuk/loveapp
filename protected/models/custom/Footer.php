<?php

class Footer
{
    private static $soc = 'vk';

    public static function setSoc($s)
    {
        self::$soc = $s;
    }

    public static function getItems()
    {
        if (self::$soc == 'vk') {
            return self::vk();
        }

        return [];
    }

    private static function vk()
    {
        return [
            'Поделиться' => [
                'class' => 'share'
            ],
            'FAQ' => [
                'class' => 'faq'
            ],
            'Правила' => [
                'class' => 'rules'
            ]
        ];
    }
}