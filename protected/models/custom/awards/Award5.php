<?php

class Award5 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return 'Звезда';
        }

        return $this->isMale() ? 'Известный' : 'Известная';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 75);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 200);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно еще <b>%d</b> раз попасть в ленту пользователей",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' famous';
    }

    public function index()
    {
        return 5;
    }
}