<?php

abstract class AwardBase
{
    protected $val;
    protected $gender;

    protected $step2max;
    protected $step1max;

    public function __construct($gender, $val)
    {
        $this->gender = $gender;
        $this->setValue($val);
    }

    public function setValue($val)
    {
        $this->val = (int)$val;
    }

    public function isStep1()
    {
        return $this->val < $this->getStep1Max();
    }

    public function isStep2()
    {
        return $this->val >= $this->getStep1Max();
    }

    public function getStep()
    {
        return $this->isStep2() ? 2 : 1;
    }

    public function isReady()
    {
        return $this->val >= $this->getStep2Max();
    }

    public function getClass()
    {
        $class[] = 'step' . $this->getStep();
        if ($this->isStep2()) {
            $class[] = 'active';
        }

        return join(' ', $class);
    }

    public function percentage()
    {
        $p = 0;

        if ($this->isStep1()) {
            $p = ($this->val * 100) / ($this->step1max != 0 ? $this->step1max : 1);
        }
        if ($this->isStep2()) {
            $p = ($this->val * 100) / ($this->step2max != 0 ? $this->step2max : 1);
        }

        $p = round($p, 1);

        return $p <= 100 ? $p : 100;
    }

    public function countTillStepEnd()
    {
        if ($this->isStep1()) {
            return $this->step1max - $this->val;
        }
        return $this->step2max - $this->val;
    }

    public function isMale()
    {
        return $this->gender == 2;
    }

    public function isFemale()
    {
        return $this->gender == 1;
    }

    abstract public function getStep1Max();

    abstract public function getStep2Max();
}