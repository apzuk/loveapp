<?php

class Award9 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Зацелованный' : 'Зацелованная';
        }

        return $this->isMale() ? 'Зацелованный' : 'Зацелованная';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 100);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 100);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно еще <b>%d</b> поцелуев",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' kissed';
    }

    public function index()
    {
        return 9;
    }
}