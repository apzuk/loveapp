<?php

class Award1 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return 'Бог флирта';
        }

        return 'Флиртер';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 250);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 2000);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно сыграть еще <b>%d</b> игр",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' flirt';
    }

    public function index()
    {
        return 1;
    }
}