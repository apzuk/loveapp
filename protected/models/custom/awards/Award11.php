<?php

class Award11 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'VIP' : 'VIP';
        }

        return $this->isMale() ? 'VIP' : 'VIP';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 1000);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 10000);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно еще <b>%d</b> дней быть под статусум VIP",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' avip';
    }

    public function index()
    {
        return 11;
    }
}