<?php

class Award12 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Любопытный' : 'Любопытный';
        }

        return $this->isMale() ? 'Любопытный' : 'Любопытная';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 10000);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 50000);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно чаще просмотреть чужие анкеты",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' curious';
    }

    public function index()
    {
        return 12;
    }
}