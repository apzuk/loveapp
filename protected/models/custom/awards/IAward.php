<?php

interface IAward
{
    public function setValue($val);

    public function getTitle();

    public function getClass();

    public function getTooltipTitle();

    public function percentage();
} 