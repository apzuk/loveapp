<?php

class Award10 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Бабник' : '';
        }

        return $this->isMale() ? 'Бабник' : 'Лидерша';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 250);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 250);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно поцеловать <b>%d</b> раз",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' womanizer';
    }

    public function index()
    {
        return 10;
    }
}