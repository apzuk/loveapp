<?php

class Award6 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Шейх' : 'Королева';
        }

        return $this->isMale() ? 'Лорд' : 'Принцесса';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 20);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 60);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно еще <b>%d</b> свит",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' lord';
    }

    public function index()
    {
        return 6;
    }
}