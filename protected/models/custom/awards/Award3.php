<?php

class Award3 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return 'Oлигарх';
        }

        return 'Богатый';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 2500);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 17500);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно потратить еще <b>%d</b> монет",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' rich';
    }

    public function index()
    {
        return 3;
    }
}