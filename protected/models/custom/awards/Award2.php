<?php

class Award2 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Симпатичный' : 'Симпатичная';
        }

        return $this->isMale() ? 'Красавчик' : 'Красавица';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 75);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 325);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно получить еще <b>%d</b> симпатий",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' cute';
    }

    public function index()
    {
        return 2;
    }
}