<?php

class Award8 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Автаритет' : 'Автаритет';
        }

        return $this->isMale() ? 'Уважаемый' : 'Уважаемая';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 2000);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 6000);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно набрать еще <b>%d</b> рейтинга",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' respected';
    }

    public function index()
    {
        return 8;
    }
}