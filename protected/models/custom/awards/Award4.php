<?php

class Award4 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Вождь' : 'Вождь';
        }

        return $this->isMale() ? 'Лидер' : 'Лидер';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 75);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 200);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно еще <b>%d</b> раз стать лидером любого города",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' leader';
    }

    public function index()
    {
        return 4;
    }
}