<?php

class Award7 extends AwardBase implements IAward
{
    public function getTitle()
    {
        if ($this->isStep2()) {
            return $this->isMale() ? 'Король социума' : 'Королева Социума';
        }

        return $this->isMale() ? 'Общительный' : 'Общительная';
    }

    public function getStep1Max()
    {
        return ($this->step1max = 50000);
    }

    public function getStep2Max()
    {
        return ($this->step2max = 500000);
    }

    public function getTooltipTitle()
    {
        return sprintf(
            "Чтоб получить награду <b>%s</b> нужно больше общаться с людьми",
            $this->getTitle(),
            $this->countTillStepEnd()
        );
    }

    public function getClass()
    {
        return parent::getClass() . ' sociable';
    }

    public function index()
    {
        return 7;
    }
}