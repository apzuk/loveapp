<?php

class Award
{
    private $state = null;
    private $user = null;

    const AWARD_FLIRTER = 1;
    const AWARD_CUTE = 2;
    const AWARD_RICH = 3;
    const AWARD_LEADER = 4;
    const AWARD_FAMOUS = 5;
    const AWARD_LORD = 6;
    const AWARD_SOCIABLE = 7;
    const AWARD_RESPECTED = 8;
    const AWARD_KISSED = 9;
    const AWARD_WOMANIZER = 10;
    const AWARD_VIP = 11;
    const AWARD_CURIOUS = 12;

    public function __construct(UserAccount $user)
    {
        $this->state = $user->stat();
        $this->user = $user;
    }

    public function getAwards()
    {
        $awards = [];

        $oClass = new ReflectionClass ('Award');
        foreach ($oClass->getConstants() as $item) {
            $awards[] = $this->getAward($item);
        }

        $awards = array_reverse($awards);
        usort(
            $awards,
            function ($item1, $item2) {
                if($item1->percentage() == $item2->percentage()) {
                    return 0;
                }

                return $item1->percentage() <= $item2->percentage() ? 1 : -1;
            }
        );

        return $awards;
    }

    /**
     * @param $item
     * @return null
     */
    public function getAward($item)
    {
        $currentClass = "Award" . $item;

        if (@class_exists($currentClass)) {
            return new $currentClass($this->user->gender, $this->getValue($item));
        }

        return null;
    }

    /**
     * @param $award_type
     * @return int|mixed|null|void
     */
    public function getValue($award_type)
    {
        if ($award_type == Award::AWARD_FLIRTER) {
            return $this->state->game_count;
        }
        if ($award_type == Award::AWARD_CUTE) {
            return $this->state->likes;
        }
        if ($award_type == Award::AWARD_RICH) {
            return $this->state->spent_money;
        }
        if ($award_type == Award::AWARD_LEADER) {
            return $this->state->leader_count;
        }
        if ($award_type == Award::AWARD_FAMOUS) {
            return $this->state->ribbon_count;
        }
        if ($award_type == Award::AWARD_LORD) {
            return $this->state->retinue_count;
        }
        if ($award_type == Award::AWARD_SOCIABLE) {
            return $this->state->messages_count;
        }
        if ($award_type == Award::AWARD_RESPECTED) {
            return $this->state->rating;
        }
        if ($award_type == Award::AWARD_KISSED) {
            return $this->state->kiss;
        }
        if ($this->user->gender == 1) {
            if ($award_type == Award::AWARD_WOMANIZER) {
                return $this->state->kissed_count;
            }
        }
        if ($award_type == Award::AWARD_CURIOUS) {
            return $this->state->curious_count;
        }

        return 0;
    }
}