<?php

class UserAward extends Model
{
    public $awardTitle;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_award';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('award, step, is_viewed, user_id', 'numerical', 'integerOnly' => true),
            array('id, award, step, is_viewed, user_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'award' => 'Award',
            'step' => 'Step',
            'is_viewed' => 'Is Viewed',
            'user_id' => 'User',
        );
    }

    /**
     * @param string $className active record class name.
     * @return UserAward the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
