<?php

class SysCities extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sys_cities';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, country_id, city, region', 'required'),
            array('id, country_id, rawoffset, important', 'numerical', 'integerOnly' => true),
            array('city, state, region', 'length', 'max' => 255),
            array('id, country_id, city, state, region, rawoffset, important', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'cityLeaderLog' => array(self::HAS_MANY, 'LogLeader', 'city_id'),
            'country' => array(self::BELONGS_TO, 'SysCountries', 'country_id'),
            'userAccounts' => array(self::HAS_MANY, 'UserAccount', 'city_id'),
            'userLeaders' => array(self::HAS_MANY, 'UserLeader', 'city_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'country_id' => 'Country',
            'city' => 'City',
            'state' => 'State',
            'region' => 'Region',
            'rawoffset' => 'Rawoffset',
            'important' => 'Important',
        );
    }

    /**
     * @param string $className active record class name.
     * @return SysCities the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function countryId($countryId)
    {
        if ($countryId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.country_id = :country_id',
                    'params' => [':country_id' => $countryId],
                ]
            );
        }

        return $this;
    }
}
