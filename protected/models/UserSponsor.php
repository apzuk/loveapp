<?php

class UserSponsor extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_sponsor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('sponsor_user_id, target_user_id', 'numerical', 'integerOnly'=>true),
			array('id, sponsor_user_id, target_user_id', 'safe')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'sponsor_user_id'),
			'target' => array(self::BELONGS_TO, 'UserAccount', 'target_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sponsor_user_id' => 'Sponsor User',
			'target_user_id' => 'Target User',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return userSponsor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Target user id
     * @param $user_id
     * @return $this
     */
    public function targetUserId($user_id)
    {
        if ($user_id !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.target_user_id = :target_user_id',
                    'params' => [':target_user_id' => $user_id],
                ]
            );
        }

        return $this;
    }

    /**
     * Sponsor user id
     * @param $user_id
     * @return $this
     */
    public function sponsorUserId($user_id)
    {
        if ($user_id !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.sponsor_user_id = :sponsor_user_id',
                    'params' => [':sponsor_user_id' => $user_id],
                ]
            );
        }

        return $this;
    }
}
