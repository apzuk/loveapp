<?php

class LogLeader extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_leader';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, city_id, current_bid', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>255),
			array('status', 'length', 'max'=>6),
			array('log_id, user_id, city_id, current_bid, message, update_date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
			'city' => array(self::BELONGS_TO, 'SysCities', 'city_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'user_id' => 'User',
			'city_id' => 'City',
			'current_bid' => 'Current Bid',
			'message' => 'Message',
			'update_date' => 'Update Date',
			'status' => 'Status',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return LogLeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
