<?php

class GameUser extends Model
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, game_id', 'required'),
			array('user_id, game_id', 'numerical', 'integerOnly'=>true),
			array('id, user_id, game_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'game_id' => 'Game',
		);
	}

	/**
	 * @param string $className active record class name.
	 * @return gameUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
