<?php

class Notification extends Model
{
    const LIKE = 2;
    const KISS = 17;
    const SEND_MONEY = 9;
    const DONATE_VIP = 8;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'notification';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('tmpl_id, user_id, owner_user_id', 'numerical', 'integerOnly' => true),
            array('status', 'length', 'max' => 7),
            array('data', 'safe'),
            array('id, tmpl_id, create_ts, status, user_id, owner_user_id, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'logVkNotifs' => array(self::HAS_MANY, 'LogVkNotif', 'notif_id'),
            'ownerUser' => array(self::BELONGS_TO, 'UserAccount', 'owner_user_id'),
            'tmpl' => array(self::BELONGS_TO, 'NotificationTmpl', 'tmpl_id'),
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'tmpl_id' => 'Tmpl',
            'create_ts' => 'Create Ts',
            'status' => 'Status',
            'user_id' => 'User',
            'owner_user_id' => 'Owner User',
            'data' => 'Data',
        );
    }

    /**
     * @param string $className active record class name.
     * @return Notifications the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Notification date
     * @return string
     */
    public function date()
    {
        $today = strtotime(date('Y-m-d'));
        $pubDate = strtotime(date('Y-m-d', strtotime($this->create_ts)));

        if ($today === $pubDate) {
            return Format::date($this->create_ts, "HH:mm, ") . ' сегодня';
        }
        if ($today === ($pubDate + 24 * 60 * 60)) {
            return Format::date($this->create_ts, "HH:mm, ") . ' вчера';
        }

        return Format::date($this->create_ts, "HH:mm, dd.MM.yyyy");
    }

    /**
     * Notification text
     * @return mixed
     */
    public function tmplText()
    {
        $end = $this->ownerUser->gender == 1 ? 'а' : '';
        $data = ['end' => $end];

        $data = array_merge($data, json_decode($this->data, true));

        return $this->merge($this->tmpl->tmpl, $data);
    }

    /**
     * Merge given string with given parameters
     * @param $string
     * @param $replacements
     * @return mixed
     */
    private function merge($string, $replacements)
    {
        return preg_replace_callback(
            '/\[([^\]]+)\]/',
            function ($match) use ($replacements) {
                $tag = $match[1];

                // matched tag is in our replacement array, use it
                if (isset ($replacements[$tag])) {
                    return $replacements[$tag];
                }

                // no match in replacement array, leave tag as-is
                return '[' . $tag . ']';
            },
            $string
        );
    }

    /**
     * @param null $userId
     * @return $this
     */
    public function tmplId($tmplId = null)
    {
        if ($tmplId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.tmpl_id = :tmpl_id',
                    'params' => [':tmpl_id' => $tmplId],
                ]
            );
        }

        return $this;
    }

    /**
     * @param null $status
     * @return $this
     */
    public function status($status = null)
    {
        if ($status !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.status = :status',
                    'params' => [':status' => $status],
                ]
            );
        }

        return $this;
    }

    /**
     * Text for vk notification
     * @return mixed
     */
    public function getNotificationTextForVK()
    {
        switch ($this->tmpl_id) {
            case 1:
                $txt = $this->tmplText();
                break;
            case 2:
                $txt = $this->tmplText();
                break;
            case 3:
                $txt = $this->tmplText();
                break;
            case 4:
                $txt = $this->tmplText();
                break;
            case 6:
                $txt = $this->tmplText();
                break;
            case 7:
                $txt = $this->tmplText();
                break;
            case 8:
                $txt = $this->tmplText();
                break;
            case 17:
                $txt = $this->tmplText();
                break;
            case 18:
                $txt = $this->tmplText();
                break;
            default:
                $txt = null;
        }

        return $txt;
    }

    public function afterSave()
    {
        $root = $this;

        app()->controller->tryMe(
            function () use ($root) {
                // send notification to VK if user offline
                if ($root->user->online) {
                    VKWrapper::apiNotify($root->user_id, $root->getNotificationTextForVK());
                }

                if ($root->status == "pending") {
                    app()->websocket->send(
                        '#userUpdates' . $root->user_id,
                        ['eventToFire' => 'notificationUpgrade'] + $root->attributes
                    );
                }
            },
            function () {
            }
        );
    }

    /**
     * Set new notification
     * @param $tmpl
     * @param $user_id
     * @param $actor_user_id
     * @param array $data
     * @param null $date
     * @return bool
     */
    public static function set(
        $tmpl,
        $user_id,
        $actor_user_id,
        $data = [],
        $date = null
    ) {
        if ($actor_user_id === null) {
            $actor_user_id = UserAccount::ADMIN_ID;
        }

        // Set date
        if (!$date) {
            $date = Date::now();
        }

        // Encode data array
        if (!is_array($data)) {
            $data = [];
        }
        $data = json_encode($data);

        if ($user_id === $actor_user_id) {
            return true;
        }

        // Set notification
        $n = new Notification();
        $n->tmpl_id = $tmpl;
        $n->user_id = $user_id;
        $n->owner_user_id = $actor_user_id;
        $n->data = $data;
        $n->create_ts = $date;

        return $n->save();
    }
}
