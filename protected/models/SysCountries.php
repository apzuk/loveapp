<?php

class SysCountries extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sys_countries';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('country_id, name, currency_code', 'required'),
            array('country_id', 'numerical', 'integerOnly' => true),
            array('name, currency', 'length', 'max' => 255),
            array('currency_code', 'length', 'max' => 5),
            array('important, short', 'length', 'max' => 45),
            array('country_id, name, currency_code, currency, important, short', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'sysCities' => array(self::HAS_MANY, 'SysCities', 'country_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'country_id' => 'Country',
            'name' => 'Name',
            'currency_code' => 'Currency Code',
            'currency' => 'Currency',
            'important' => 'Important',
            'short' => 'Short',
        );
    }

    /**
     * @param string $className active record class name.
     * @return SysCountries the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function image()
    {
        if ($this->short == "RU") {
            return 'Russia.png';
        }
        if ($this->short == "UA") {
            return 'Ukraine.png';
        }
        if ($this->short == "BY") {
            return 'Belarus.png';
        }
        if ($this->short == "KZ") {
            return 'Kazakhstan.png';
        }
        if ($this->short == "AZ") {
            return 'Azerbaijan.png';
        }
        if ($this->short == "AM") {
            return 'Armenia.png';
        }
        if ($this->short == "GE") {
            return 'Georgia.png';
        }
        if ($this->short == "IL") {
            return 'Israel.png';
        }
        if ($this->short == "US") {
            return 'United-States.png';
        }
        if ($this->short == "CA") {
            return 'Canada.png';
        }
        if ($this->short == "KG") {
            return 'Kyrgyzstan.png';
        }
        if ($this->short == "LV") {
            return 'Latvia.png';
        }
        if ($this->short == "LT") {
            return 'Lithuania.png';
        }
        if ($this->short == "EU") {
            return 'Estonia.png';
        }
        if ($this->short == "MD") {
            return 'Moldova.png';
        }
        if ($this->short == "TJ") {
            return 'Tajikistan.png';
        }
        if ($this->short == "TM") {
            return 'Turkmenistan.png';
        }
        if ($this->short == "UZ") {
            return 'Uzbekistan.png';
        }
        if ($this->short == "DE") {
            return 'Germany.png';
        }
    }
}
