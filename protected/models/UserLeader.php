<?php

class UserLeader extends Model
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_leader';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('message', 'filter', 'filter' => 'strip_tags'),
            array('user_id, city_id, current_bid', 'numerical', 'integerOnly' => true),
            array('message', 'length', 'max' => 140),
            array('leader_id, user_id, city_id, current_bid, message, update_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'city' => array(self::BELONGS_TO, 'SysCities', 'city_id'),
            'user' => array(self::BELONGS_TO, 'UserAccount', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'leader_id' => 'Лидер',
            'user_id' => 'Пользователь',
            'city_id' => 'Город',
            'current_bid' => 'Ставка',
            'message' => 'Текст',
            'update_date' => 'Дата',
        );
    }

    public function beforeSave()
    {
        if (parent::beforeSave()) {
            $this->update_date = Date::now();

            $newLog = new LogLeader();
            $newLog->attributes = $this->attributes;
            $newLog->save();

            return true;
        }

        return false;
    }

    /**
     * @param string $className active record class name.
     * @return UserLeader the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param $city_id
     * @return $this
     */
    public function cityId($city_id)
    {
        if ($city_id !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.city_id = :city_id',
                    'params' => [':city_id' => $city_id],
                ]
            );
        }

        return $this;
    }
}
