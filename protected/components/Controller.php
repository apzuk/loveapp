<?php

class Controller extends CController
{
    public function init()
    {
        parent::init();

        Yii::app()->clientScript->registerMetaTag(
            Yii::app()->getRequest()->getCsrfToken(),
            Yii::app()->getRequest()->csrfTokenName
        );
    }

    /**
     * Wrapper for try catch
     * @param Closure $func
     * @param Closure $failure function in case of error
     * @return mixed
     */
    public function tryMe(Closure $func, Closure $failure = null)
    {
        try {
            $result = $func->bindTo($this)->__invoke();
        } catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

            if ($failure !== null) {
                return $failure->__invoke();
            } else {
                return false;
            }
        }

        return $result;
    }

    /**
     * Wrapper for transaction
     * @param Closure $func
     * @param Closure $failure function in case of error
     * @return mixed
     */
    public function transaction(Closure $func, Closure $failure = null)
    {
        $transaction = Yii::app()->db->beginTransaction();

        try {
            $result = $func->bindTo($this)->__invoke();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();

            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

            if ($failure !== null) {
                return $failure->__invoke();
            } else {
                return false;
            }
        }

        return $result;
    }

    /**
     * Triggers a 404 (Page Not Found) error.
     * @param string $msg
     * @throws CHttpException when invoked.
     */
    public function pageNotFound($msg = 'Страница не найдена')
    {
        throw new CHttpException(404, $msg);
    }

    /**
     * Triggers a 403 (Access Denied) error.
     * @param string $msg
     * @throws CHttpException when invoked.
     */
    public function accessDenied($msg = 'Доступ закрыт')
    {
        throw new CHttpException(403, $msg);
    }

    /**
     * Triggers a 400 (Bad Request) error.
     * @param string $msg
     * @throws CHttpException when invoked.
     */
    public function badRequest($msg = 'Неверный запрос')
    {
        throw new CHttpException(400, $msg);
    }

    /**
     * Response validation errors
     * @param CActiveRecord $model
     * @return void
     */
    public function errors($model)
    {
        $this->response([
                'errors' => $model->getErrors(),
                'model'  => get_class($model)
            ]);
    }

    /**
     * Response
     * @param mixed $data
     * @params bool $isModel if true — CJSON::encode
     * @return void
     */
    public function response($data, $isModel = false)
    {
        echo $isModel ? CJSON::encode($data) : json_encode($data);
        Yii::app()->end();
    }
}