<?php

/**
 * Model is the customized base model class.
 * All model classes for this application should extend from this base class.
 */
class Model extends CActiveRecord
{
    public $attrs = null;

    public static $user_id;

    public static $_defaultScopeDisabled = false;

    public $isNew = false;

    protected $fetchMode = PDO::FETCH_CLASS;

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->isNew = true;
            }

            return true;
        }

        return false;
    }

    /**
     * Validator to call methods on models
     * This is the 'callValidator' validator as declared in rules().
     */
    public function callValidator($attribute, $params)
    {
        $model = new $params[0];
        $model->{$params[1]}($attribute, $params, $this);
    }

    public function scopes()
    {
        return [
            'owner' => [
                'condition' => '`t`.userId = ' . (int)Yii::app()->user->id
            ]
        ];
    }

    public function order($column, $ordermode = 'ASC')
    {
        if ($column !== null) {
            $this->getDbCriteria()->order = $column . ' ' . $ordermode;
        }

        return $this;
    }

    public function groupBy($column)
    {
        if ($column !== null) {
            $this->getDbCriteria()->group = $column;
        }

        return $this;
    }

    public function search($key = null, $columns = 't.title')
    {
        if ($key !== null) {

            if (!is_array($columns)) {
                $columns = [$columns];
            }

            $conditions = [];

            foreach ($columns as $column) {
                $column = Yii::app()->db->quoteColumnName($column);
                $conditions[] = $column . ' LIKE :key';
            }

            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => implode(' OR ', $conditions),
                    'params' => [':key' => '%' . $key . '%'],
                ]
            );
        }

        return $this;
    }

    public function userId($userId = null)
    {
        if ($userId !== null) {
            $this->getDbCriteria()->mergeWith(
                [
                    'condition' => 't.user_id = :userId',
                    'params' => [':userId' => $userId],
                ]
            );
        }

        return $this;
    }

    public function limit($limit)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'limit' => $limit
            ]
        );

        return $this;
    }

    public function offset($offset)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'offset' => $offset
            ]
        );

        return $this;
    }

    public function hasChanged($attr)
    {
        if (!isset($this->attrs[$attr]) || !isset($this->attributes[$attr])) {
            return false;
        }

        if ($this->attrs[$attr] == $this->attributes[$attr]) {
            return false;
        }

        return true;
    }

    public static function user()
    {
        return app()->session['user'];
    }
}