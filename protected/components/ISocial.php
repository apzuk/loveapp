<?php 

interface ISocial {
    public function apiGetUser($soc_id);
    public function getSocialId();
}