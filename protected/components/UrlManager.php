<?php

/**
 * Manages the URLs of Yii Web applications
 */
class UrlManager extends CUrlManager
{
    public function createUrl($route, $params = [], $ampersand = '&')
    {
        $url = parent::createUrl($route, $params, $ampersand);

        return $url;
    }
}