<?php

class VKWrapper implements ISocial
{
    public static $vk_id = 3761544;
    public static $vk_secret = 'mtiFi4PQUs6mN4cNrTne';

    public function getSocialId()
    {
        if (!isset($_REQUEST['viewer_id']) && !app()->session['user_id']) {
            throw new Exception('Invalid data');
        }

        if (app()->request->isAjaxRequest) {
            return app()->session['user_id'];
        }

        return app()->session['user_id'] = $_REQUEST['viewer_id'];
    }

    /**
     * @param $user_id
     * @return array
     */
    public function apiGetUser($user_id)
    {
        // Create new vk api object
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        // Prepare request data
        $params = array(
            'user_ids' => $user_id,
            'fields' => 'nickname, screen_name, sex, bdate, city, country, timezone, photo_100, photo_200_orig, city'
        );
        // Make request
        $uData = $vk->api('users.get', $params);
        if (!isset($uData['response'][0])) {
            return false;
        }
        $user = $uData['response'][0];

        // Fill response to our model!
        $newUser = new UserAccount();
        $newUser->username = $user['first_name'] . ' ' . $user['last_name'];
        $newUser->gender = $user['sex'];

        // If birth date is available
        if (isset($user['bdate'])) {
            if ($d = DateTime::createFromFormat('d.m.Y', $user['bdate'])) {
                $newUser->birth_date = $d->format('Y-m-d');
            }
        }
        $newUser->socialScreenName = $user['screen_name'];
        $newUser->avatar = $user['photo_100'];
        $newUser->avatar_orig = $user['photo_200_orig'];
        $newUser->movePhotoFiles();

        if ($user['city']) {
            $city = SysCities::model()->search($user['city'], 't.city')->find();
            if ($city) {
                $newUser->city_id = $city->id;
            }
        }

        $newUser->soc_id = $user_id;
        $newUser->social = UserAccount::TYPE_VK;

        return $newUser;
    }

    /**
     * @param $ids
     * @param $msg
     */
    public static function apiNotify($ids, $msg)
    {
        $vk = new VKApi(self::$vk_id, self::$vk_secret);

        if (!$accessToken = $vk->accessToken()) {
            return false;
        }

        $data = $vk->api(
            'secure.sendNotification',
            array(
                'user_ids' => $ids,
                'message' => $msg,
                'client_secret' => $accessToken
            )
        );

        return $data;
    }
}
