<?php

class HttpRequest extends CHttpRequest
{
    public $noCsrfValidationRoutes = [];

    protected function normalizeRequest()
    {
        //attach event handlers for CSRFin the parent
        parent::normalizeRequest();
        //remove the event handler CSRF if this is a route we want skipped

        if (Yii::app() instanceof CConsoleApplication == false) {
            if ($this->enableCsrfValidation) {
                foreach ($this->noCsrfValidationRoutes as $route) {
                    if (strpos($_SERVER['REQUEST_URI'], $route) === 0) {
                        Yii::app()->detachEventHandler('onBeginRequest', [$this, 'validateCsrfToken']);
                    }
                }
            }
        }
    }
}