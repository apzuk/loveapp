<?php

class Application
{
    private static $soc;
    public static $wrapper;

    public static $user_id;

    public static function run()
    {
        // Get social type
        if (!isset($_REQUEST['soc']) && !app()->session['soc']) {
            throw new Exception('No social identity passed');
        }

        // Store type
        if (app()->request->isAjaxRequest) {
            self:: $soc = app()->session['soc'];
        } else {
            self:: $soc = app()->session['soc'] = $_REQUEST['soc'];
        }

        // Social wrapper
        $socWrapper = strtoupper(self:: $soc) . 'Wrapper';
        if (!class_exists($socWrapper)) {
            throw new Exception('"' . self:: $soc . '" not supported');
        }
        self :: $wrapper = new $socWrapper();

        $user = self::getUser();
        $user->setVisit();

        self::$user_id = $user->user_id;

        // Return user
        return $user;
    }

    public static function getUser()
    {
        $wrapper = self::$wrapper;
        $user = UserAccount::model()
            ->with('stat')
            ->with('city.country')
            ->socId($wrapper->getSocialId())
            ->soc(self:: $soc)->find();

        $user->awards = UserAward::model()->userId($user->user_id)->find('is_viewed=0');
        if (!$user->awards) {
            $user->awards = [];
        }

        app()->session['user'] = $user;

        if (!$user) {
            // Get user from social
            $user = $wrapper->apiGetUser($wrapper->getSocialId());
        }

        // Generate security id
        if (!app()->request->isPostRequest) {
            $user->sid = md5($user->user_id . time() . rand(0, 588878));
            $user->save(false);
        }

        return $user;
    }
}