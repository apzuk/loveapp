<?php

class FrontController extends Controller
{
    private $u = null;
    protected $noSocialInitialisation = false;

    public function init()
    {
        parent::init();
    }

    public function beforeAction($action)
    {
        if (!$this->noSocialInitialisation) {
            $this->u = Application::run();
            Model::$user_id = $this->u->user_id;
        }

        return true;
    }

    public function user()
    {
        if (app()->request->isAjaxRequest && app()->session['user']) {
            app()->session['user'];
        }
        return $this->u;
    }

    public function noEnoughMoney()
    {
        $this->response(
            [
                'result' => false,
                'reason' => 'no.balance'
            ]
        );
    }

    public function withdraw(&$user, $count, $withSave = false)
    {
        if ($user->balance < $count) {
            return $this->noEnoughMoney();
        }

        $user->balance = $user->balance - $count;
        $user->stat()->spent_money += $count * 10;

        if ($withSave) {
            return $user->save();
        }

        return true;
    }
}