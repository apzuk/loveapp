<?php

class Websocket extends CApplicationComponent
{
    public $url = '10.0.0.105:9615';
    public $projectId;
    public $secretKey;

    public function prepareData($channel, $data)
    {
        return $params = array_merge(
            [
                'method' => 'publish',
                'module' => 'users',
                'channel' => $channel
            ],
            $data
        );
    }

    /**
     * Publish to channel
     * @param string $channel
     * @param array $data
     * @return bool|array
     */
    public function send($channel, $data)
    {
        $self = $this;

        $data = $this->prepareData($channel, $data);

        return Yii::app()->controller->tryMe(
            function () use ($self, $channel, $data) {

                $options = array(
                    'http' => array(
                        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method' => 'GET',
                    ),
                );

                $context = stream_context_create($options);
                $result = @file_get_contents('http://10.0.0.105:9615?' . http_build_query($data), false, $context);

                if ($result === false) {
                    return false;
                }
            },
            function () {
                return false;
            }
        );
    }

    /**
     * Generate token
     * @param $userId
     * @param $timestemp
     * @return string
     */
    public function generateToken($userId, $timestemp)
    {
        return hash_hmac('md5', $this->projectId . $userId . $timestemp, $this->secretKey);
    }

    public function connectJsCode($user)
    {
        $timestemp = strtotime(Date::now());
        $options = [
            'url' => (string)$this->url . '/connection',
            'project' => (string)$this->projectId,
            'user' => (string)$user->user_id,
            'timestamp' => (string)$timestemp,
            'token' => (string)$this->generateToken($user->user_id, $timestemp)
        ];

        $s = CJavaScript::encode($options);

        Yii::app()->clientScript
            ->registerScript(
                'centrifuge_' . uniqid(),
                'centrifuge = new Centrifuge(' . $s . '); centrifuge.connect(); userId=' . $user->user_id,
                CClientScript::POS_END
            );
    }
}