<?php

class FinishGameCommand extends CConsoleCommand
{
    /**
     * Provides the command description.
     * @return string the command description.
     */
    public function getHelp()
    {
        return <<<EOD
DESCRIPTION
Finish game
EOD;
    }

    public function run($args)
    {
        if (count($args) != 1) {
            echo "game_id required \n";
            Yii::app()->end();
        }

        $game_id = $args[0];
        $game = Game::model()->with('users.user.stat')->findByPk($game_id);

        if ($game === null) {
            echo "A game with specified id: \"" . $game_id . "\" can not be found \n";
            Yii::app()->end();
        }

        if ($game->status == 1) {
            echo "The game have already finished";
            Yii::app()->end();
        }
        $game->status = 1;

        $transaction = Yii::app()->db->beginTransaction();

        try {
            $game->save();
            $transaction->commit();

            foreach ($game->users as $user) {
                $user->user->stat->game_count++;
                $user->user->addRating($user->rating);
                $user->user->save();

                echo "User " . $user->user->username . " processed \n";
            }
        } catch (Exception $e) {
            $transaction->rollback();
        }
    }
}