$(function () {
    $('.jq-dialog').each(function () {
        var $dialog = $(this), prop = $dialog.data('prop');

        if (typeof prop == 'undefined') {
            return;
        }

        prop["autoOpen"] = parseInt(prop["autoopen"]);
        prop["closeOnEscape"] = false;

        if (prop.hasOwnProperty('position')) {
            prop['position']['of'] = $(prop['position']['of']);
        }

        if(parseInt(prop["width"]) != prop["width"]) {
            prop["width"] = "auto";
        }
        if(parseInt(prop["height"]) != prop["height"]) {
            prop["height"] = "auto";
        }

        if (prop.hasOwnProperty('buttons')) {
            var buttons = [];

            $.each(prop.buttons['button'], function () {
                var button = this;

                var btn = {
                    text: this.label
                };

                btn.click = function () {
                    eval(button.callback);
                };

                buttons.push(btn);
            });

            prop.buttons = buttons;
        }

        if (prop.styledHeader) {
            var url = prop.styledHeaderImg ? prop.styledHeaderImg : avatar;
            url = url.replace(/\[gender\]/, gender);

            $(this).prepend("<div class='sd-header'></div>");

            if (prop.styledHeaderImg) {
                $(this).prepend("<div class='sd-avatar circle male f_left'><img style='margin: -1px;width: 81px;' src='" + url + "'/></div>");
            }else {
                $(this).prepend("<div class='sd-avatar circle male radius_4 brd brd_3 skin_5 f_left'><img style='margin: -1px;width: 81px;' src='" + url + "'/></div>");
            }

            $('.sd-avatar', $dialog).css({
                left: (prop['width'] - $(this).width()) / 2 - 40
            })

        }

        $dialog.dialog(prop);

        if (prop.hasOwnProperty('sd')) {
            if (prop['sd']['gender'] == "auto") {
                prop['sd']['gender'] = gender;
            }

            prop['sd']['autoOpen'] = prop['autoOpen'];
            $dialog.styledDialog(prop['sd']);
        }
    });

    // On dialog open
    em.onDialogOpen();
});
