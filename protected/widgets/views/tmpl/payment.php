<div class="payment">
    <div class="header">
        <span class="l-row1 font_16">Ваш кошелек</span><br/>
        <span class="l-row2 ff_ssb font_17">7 365 монет</span>
    </div>

    <?php $spec = null ?>
    <div class="rates p1">
        <?php foreach ($xml = XmlHelper::paymentRates() as $rate) : ?>
            <?php if (!$rate->type == 'spec') : ?>
                <figure class="f_left rate clearfix btn_hov">
                    <div class="row1 p_rel clearfix _btn">
                        <div class="col1 f_left f_bold ff_ssb"><?= $rate->value ?></div>
                        <div class="col2 f_left btn2 cur_p font_15"><?= $rate->label ?></div>
                    </div>
                    <div class="row2 clearfix m_5">
                        <div class="col1 f_left">+<?= $rate->bonus->rating ?></div>
                        <div class="col2 f_left">+<?= $rate->bonus->money ?></div>
                    </div>
                </figure>
            <?php else : ?>
                <?php $spec = $rate; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <?php if ($spec) : ?>
        <div class="block4 cur_p">
            <div class="col1 f_bold ff_ssb f_left"><?= $spec->value ?></div>
            <div class="col2 f_left"><?= $spec->label ?></div>
            <div class="col3 f_left"></div>
        </div>
    <?php endif; ?>
</div>