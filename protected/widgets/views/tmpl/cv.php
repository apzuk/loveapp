<?php $items = [5, 5, 5, 5, 20] ?>

<div class="cv">
    <p class="al_center">Награждение за ежедневное посещение</p>

    <div class="clearfix">
        <?php foreach ($items as $key => $item) : ?>
            <?php $class = $key == $user->cv() ? 'active' : ''; ?>
            <figure class="f_left al_center p_rel <?= $class ?>"><?= $item ?></figure>
        <?php endforeach; ?>
    </div>

    <p class="al_center">
        Посещайте каждый день и получайте 5 монет.
        После 5 дней посещений получайте 20 монет.
    </p>
</div>