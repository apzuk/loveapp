<div>
    <figure>
        <figcaption class="ff_ssb f_bold al_center m_57">
            Стать VIP
        </figcaption>

        <div class="row ico1 clearfix"><span>Игра без очереди</span></div>
        <div class="row ico2 clearfix"><span>Выбор цвета и ярлыка</span></div>
        <div class="row ico3 clearfix"><span>Добавление фотографии в сообщениях и возможность делать снимки</span></div>
        <div class="row ico4 clearfix"><span>Режим невидимки</span></div>

        <div class="btn2 btn_hov cur_p" onclick="sD.becomeVip(1);">Купить на день (3 голос)</div>
        <div class="btn2 btn_hov cur_p" onclick="sD.becomeVip(2);">Купить на неделю (10 голос)</div>
        <div class="btn2 btn_hov cur_p" onclick="sD.becomeVip(3);">Купить на месяц (25 голос)</div>
    </figure>
</div>