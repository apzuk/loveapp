<div class="f_left">
    <div class="m_22_-22 p_abs">
        <div class="<?= $user->gender() ?> radius_4 brd brd_3 skin_<?= $user->skin(
        ) ?> f_left p_rel circle user_<?= $user->user_id ?>">
            <img src="<?= $user->avatar(); ?>" class="radius_4 w_100"/>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="online">
        <span class="female-count d_block al_center clr_white font_17 ff_ssb f_left">2587</span>
        <span class="male-count d_block al_center clr_white font_17 ff_ssb f_left">9874</span>
    </div>
</div>

<div class="f_left m_10_21 q-box p_rel disconnected">
    <textarea class="question" placeholder="Ваш вопрос"></textarea>

    <div class="clearfix"></div>

    <div class="f_left">
        <span class="ico ico-rand-question hasToolTip" title="Случайный вопрос"></span>
    </div>
    <div class="f_left ml_5">
        <input type="checkbox" class="male ico-play-unlimit ico f_left" id="play-unlimit" value="1"/>
        <span class="label-unlimit">Играть без очереди</span>
    </div>
    <div class="overlay d_none">
        <div class="bubblingG">
            <span id="bubblingG_1"></span>
            <span id="bubblingG_2"></span>
            <span id="bubblingG_3"></span>
        </div>

        <span class="clr_white search-label">поиск игры...</span>

        <div id="runner" class="clr_white ff_ssb">00:00</div>
    </div>

    <div class="connecting">
        <div class="connecting_process">
            <div class="cloud1 connecting_cloud1"></div>
            <div class="cloud2 connecting_cloud2"></div>
        </div>

        <p>Соединяюсь с сервером...</p>
    </div>
</div>
