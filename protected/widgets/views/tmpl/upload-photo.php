<div class="uploader_btns">

    <div class="btn2 btn_hov cur_p" id="fromPC">
        <div class="uploading-process" style="display: none;">
            <div class="js-progress"></div>
        </div>
        Фото с компьютера
        <input type="file" accept="image/*"/>
    </div>

    <div class="btn2 btn_hov cur_p p_rel popup_link" data-dialog="#take-pic">Снимок из веб камеры</div>
</div>