<div class="settings">
    <div class="al_center m1 font_14">
        <span>Выберите цвет</span>

        <div class="colors">
            <?php foreach (range(1, 5) as $range) : ?>
                <?php $class = $range == $user->skin() ? 'active' : ''; ?>
                <div class="skin_<?= $range; ?> circle bgr f_left cur_p <?= $class ?>"
                     onclick="sD.setUserSkin(<?= $range; ?>)"></div>
            <?php endforeach; ?>
        </div>

        <br/>

        <div>Режим невидимки <span class="bold"><?= !$user->visibility ? 'выключено' : 'включено'; ?></span></div>
        <div class="userVisibility <?= $user->visibility ? 'active' : ''; ?>" onclick="sD.toggleVisibility();"></div>
    </div>

    <div class="<?= $user->gender(); ?> radius_4 brd brd_3 skin_1 f_left vip p_rel cur_p model">
        <span class="trans2 radius_4 hover skin_1"></span>
        <img src="<?= $user->avatar(); ?>" class="radius_4">
    </div>

    <div class="hasToolTip skin_1 point" title="Ваш текст">000</div>
</div>