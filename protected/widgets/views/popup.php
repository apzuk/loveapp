<div id="<?= $this->id ?>" class="<?= $this->class ?> jq-dialog" data-prop='<?= json_encode($data); ?>'>
    <?php if ($this->view) : ?>
        <?php $this->render('tmpl/' . $this->view, ['user' => $this->user]); ?>
    <?php endif; ?>

    <?php if (isset($data['sd']))  : ?>
        <?php foreach ($data['sd']['buttons'] as $button) : ?>
            <button class="<?php echo $this->btnClass; ?> <?= ltrim($button['selector'],'.'); ?>" <?php foreach ($this->btnHtml5data as $key => $d) : ?> data-<?= $key ?>="<?= $d ?>" <?php endforeach; ?>></button>
        <?php endforeach; ?>
    <?php endif; ?>
</div>