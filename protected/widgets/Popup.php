<?php

class Popup extends CWidget
{
    private static $xml = null;

    public $id = null;

    public $class = '';

    public $view = null;

    public $btnHtml5data = [];

    public $btnClass="";

    public $user = null;

    public function init()
    {
        if ($this->id == null) {
            throw new Exception('Please set DOM id');
        }

        // Add dialog maker script
        $path = Yii::getPathOfAlias('application.widgets.assets');
        Yii::app()->getClientScript()->registerScriptFile(
            '/' . $path . '/popups/jqDialogMaker.js',
            CClientScript::POS_END
        );

        // Get required dialog properties
        $data = $this->struct();
        if (isset($data['@attributes'])) {
            unset($data['@attributes']);
        }

        // Render It
        $data['id'] = $this->id;
        $this->render('popup', ['data' => $data]);
    }

    /**
     * @return array|null
     */
    public function struct()
    {
        // Load xml file
        if (!$xml = self::$xml) {
            $path = Yii::app()->getRuntimePath() . '/popupInfo.xml';
            if(!file_exists($path)) {
                throw new Exception('Application component not found');
            }

            $xml = self::$xml = simplexml_load_file($path);
        }

        // Look up for needed
        foreach ($xml as $item) {
            if ($item->attributes()->id == $this->id) {
                return $this->xml2array($item);
            }
        }

        return null;
    }

    /**
     * @param $xmlObject
     * @param array $out
     * @return array
     */
    public function xml2array($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node) {
            $out[$index] = (is_object($node) || is_array($node)) ? $this->xml2array($node) : $node;
        }

        return $out;
    }
}