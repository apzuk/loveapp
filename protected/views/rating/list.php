<?php if($offset) : ?>
    <?php foreach($users as $index => $user) : ?>
        <div class="item f_left">
            <div onclick="sD.profile(<?= $user->user_id; ?>)"
                 class="<?= $user->gender(); ?> radius_3 brd brd_3 skin_<?= $user->skin(); ?> f_left <?= $user->isVip() ? 'vip' : ''; ?> p_rel cur_p user-block user_<?= $user->user_id; ?>">
                <span class="trans2 radius_4 hover skin_<?= $user->skin(); ?>"></span>
                <img src="<?= $user->avatar(); ?>" alt="">

                <div class="circle-pos"><?= ($index + $offset * 21 + 1); ?></div>

                <div class="value"><?= $user->stat->rating; ?></div>
            </div>
        </div>
    <?php endforeach; ?>
<?php else : ?>

    <?php $top = array_splice($users, 0, 3); ?>
    <?php $topUsers = [$top[1],$top[0],$top[2]]; ?>
    <?php $poss = [2, 1, 3] ?>

    <div id="rating-list">
        <div class="top">
            <?php foreach ($topUsers as $index => $user) : ?>
                <div class="top-user f_left top<?= $index ?>">
                    <div onclick="sD.profile(<?= $user->user_id; ?>)"
                         class="<?= $user->gender(); ?> radius_3 brd brd_3 skin_<?= $user->skin(); ?> f_left <?= $user->isVip() ? 'vip' : ''; ?> p_rel cur_p user-block user_<?= $user->user_id; ?>">
                        <span class="trans2 radius_4 hover skin_<?= $user->skin(); ?>"></span>
                        <img src="<?= $user->avatar(); ?>" alt="">

                        <div class="value"><?= $user->stat->rating; ?></div>
                    </div>

                    <div class="pos p_rel">
                        <div class="circle-pos circle"><?= $poss[$index]; ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="others jsp">
            <?php foreach($users as $index => $user) : ?>
            <div class="item f_left">
                <div onclick="sD.profile(<?= $user->user_id; ?>)"
                     class="<?= $user->gender(); ?> radius_3 brd brd_3 skin_<?= $user->skin(); ?> f_left <?= $user->isVip() ? 'vip' : ''; ?> p_rel cur_p user-block user_<?= $user->user_id; ?>">
                    <span class="trans2 radius_4 hover skin_<?= $user->skin(); ?>"></span>
                    <img src="<?= $user->avatar(); ?>" alt="">

                    <div class="circle-pos"><?= ($index + 4); ?></div>

                    <div class="value"><?= $user->stat->rating; ?></div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>