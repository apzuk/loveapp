<script id="userBlockTmpl" type="text/template">
    <div onclick="sD.profile(${user_id})"
         class="${gender} radius_3 brd brd_3 skin_${skin} f_left {{if is_vip}}vip{{/if}} p_rel cur_p user-block user_${user_id}">
        <span class="trans2 radius_4 hover skin_${skin}"></span>
        <img src="${avatar}" alt="">
    </div>
</script>

<script id="userRibbon" type="text/template">
    <div style="margin-left: -80px;"
         title="${comment}"
         onclick="sD.profile(${user_id})"
         class="${gender} radius_4 brd brd_3 skin_${skin} f_left {{if is_vip}}vip{{/if}} p_rel cur_p block3 ribbon_${id} ribbonToolTip">
        <span class="trans2 radius_4 hover skin_${skin}"></span>
        <img src="${avatar}" class="radius_4">
    </div>
</script>

<script id="gameUserTmpl" type="text/template">
    <div class="u ${gender} u_${user_id} p_rel">
        <div class="badge"></div>

        <div onclick="sD.profile(${user_id})"
             class="${gender} radius_3 brd brd_3 skin_${skin} f_left {{if is_vip}}vip{{/if}} p_rel cur_p user-block user_${user_id}">
            <span class="trans2 radius_4 hover skin_${skin}"></span>
            <img src="${avatar}" alt="">
        </div>

        <p>${firstname}, ${cityname}</p>
    </div>
</script>

<script id="gameTmpl" type="text/template">
    <div class="line_<?= $this->user()->gender(); ?> l line"></div>

    <div id="gameContent">
        <div id="box1" data-gender="female" data-direction="r">
            <div></div>
            <button class="answer" onclick="game.answer(this);"></button>
        </div>
        <div id="box2" data-gender="male" data-direction="r">
            <div></div>
            <button class="answer" onclick="game.answer(this);"></button>
        </div>
    </div>

    <div class="line_<?= $this->user()->oppositeGender(); ?> r line"></div>
</script>

<script id="gameBoxTmpl" type="text/template">
    <label class="ff_ssb">Вопрос:</label>
    <p></p>

    <time>Осталось <span>${time}</span></time>

    <textarea class="q_answer"></textarea>
</script>

<script id="gameAnswersTmpl" type="text/template">
    <div>
        <label class="ff_ssb answerer"></label>
        <time>Осталось <span>${time}</span></time>
    </div>
</script>

<script id="gameCommentStepTmpl" type="text/template">
    <div class="c rmv" style="width: 344px;">

        {{if hasAnswers == false}}
        <img src="/images/no-answer.png" style="margin: 79px 0 0 13px;width: 95%;"/>
        {{/if}}
        <div>
            {{each(index, value) answers}}
            <div class="answers_c">
                <i class="d_block">
                    <img src="${avatar}" alt=""/>${value.question}
                </i>
                <span class="d_block a">${value.answer}</span>
            </div>
            {{/each}}
        </div>
    </div>

    <div class="rmv">
        <div>
            <div class="f_left c_label">Ваш комментарий</div>
            <div data-number="5" id="raty"></div>
        </div>

        <textarea class="q_answer"></textarea>
    </div>
</script>

<script id="gameChooseStepTmpl" type="text/template">
    <div>
        <p class="myComments">Комментарии</p>
        <time>Осталось <span>${time}</span></time>
        <br/>

        <div id="myCommentsBox">
            {{each(index, value) comments}}
            <div class="commentItem clearfix f_left">
                <table>
                    <tr>
                        <td><img src="${value.avatar}" alt=""/></td>
                        <td style="width: 350px;">
                            <span style="margin-bottom: 7px;display: block;">${value.comment}</span>
                        </td>
                    </tr>
                </table>
            </div>
            {{/each}}
        </div>
    </div>
</script>

<script id="gameChooseTmpl" type="text/template">
    <div class="<?= $this->user()->oppositeGender(); ?> players ">
        {{each(index, value) players}}
        <figure class="f_left cur_p" onclick="game.selectUser(${value.user.user_id}, this);">
            <div class="avatar p_rel">
                <img src="${value.user.avatar}"/>
            </div>
            <p class="playerName t_shd1">${value.user.firstname}</p>
        </figure>
        {{/each}}
    </div>
</script>

<script id="randomUserTmpl" type="text/template">
    <div class="f_left">
        <div
            class="${gender} radius_3 brd brd_3 skin_${skin} f_left {{if is_vip}}vip{{/if}} p_rel cur_p user-block user_${user_id}">
            <span class="trans2 radius_4 hover skin_${skin}"></span>
            <img src="${avatar}" alt="">
        </div>

        <div class="labels al_left">
            <div class="label1">${firstname}, ${age} год</div>
            <div class="label2 addr">${city}</div>
        </div>
    </div>
    <div class="f_left">
        <div class="item btn_hov coffee" onclick="sD.choose(1);">Пригласить на кофе</div>
        <div class="item btn_hov walk" onclick="sD.choose(2);">Прогуляться</div>
        <div class="item btn_hov meet" onclick="sD.choose(3);">Встретиться</div>
        <div class="item btn_hov skip" onclick="sD.choose();">Пропустить</div>
    </div>
</script>

<script id="bingoTmpl" type="text/template">
    <div class="item1 ${gender_1}">
        <div onclick="sD.profile(${user_id_1})"
             class="${gender_1} radius_3 brd brd_3 skin_${skin_1} f_left {{if is_vip_1}}vip{{/if}} p_rel cur_p user-block user_${user_id_1}">
            <span class="trans2 radius_4 hover skin_${skin_1}"></span>
            <img src="${avatar_1}" alt="">
        </div>

        <button class="compose btn5 {{if self_1}}ui-state-disabled{{/if}}">Написать сообщение</button>
        <button class="profile btn5 {{if self_1}}ui-state-disabled{{/if}}">Перейти в профиль</button>
    </div>
    <div class="item2 ${gender_2}">
        <div onclick="sD.profile(${user_id_2})"
             class="${gender_2} radius_3 brd brd_3 skin_${skin_2} f_left {{if is_vip_2}}vip{{/if}} p_rel cur_p user-block user_${user_id_2}">
            <span class="trans2 radius_4 hover skin_${skin_2}"></span>
            <img src="${avatar_2}" alt="">
        </div>

        <button class="compose btn5 {{if self_2}}ui-state-disabled{{/if}}">Написать сообщение</button>
        <button class="profile btn5 {{if self_2}}ui-state-disabled{{/if}}">Перейти в профиль</button>
    </div>
</script>

<script id="online_user_tmpl" type="text/template">
    <li>
        <a href='#' onclick='sD.profile(${user_id}); return false;'>
            <img src="${avatar}" width="45" height="45" class="${class_name} xleft"/>
        </a>
    </li>
</script>