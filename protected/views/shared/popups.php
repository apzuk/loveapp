<?php $this->widget(
    'Popup',
    [
        'id' => 'leader-dialog',
        'class' => 'leader-block sd-dialog no-leader',
        'view' => 'leader',
        'btnClass' => 'popup_link',
        'btnHtml5data' => [
            'dialog' => '#become-leader-dialog'
        ]
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'wallet',
        'class' => 'sd-dialog',
        'view' => 'payment'
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'settings',
        'class' => 'sd-dialog',
        'view' => 'settings',
        'user' => $this->user()
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'become-vip',
        'class' => 'sd-dialog',
        'view' => 'become-vip'
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'question-dialog',
        'class' => 'question-block sd-dialog',
        'view' => 'question',
        'user' => $this->user()
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'become-leader-dialog',
        'view' => 'leader-become',
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'to-users-list-confirmation',
        'view' => 'to-user-list'
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'profile',
        'view' => 'profile'
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'upload-photo',
        'view' => 'upload-photo'
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'bingo',
        'view' => 'bingo',
    ]
); ?>
<?php $this->widget(
    'Popup',
    [
        'id' => 'take-pic',
        'view' => 'take-pic',
    ]
); ?>

<?php $this->widget(
    'Popup',
    [
        'id' => 'photo-dialog',
        'view' => 'photo-dialog',
    ]
); ?>

<?php $this->widget(
    'Popup',
    [
        'id' => 'award-dialog',
        'view' => 'award-dialog'
    ]
); ?>

<?php $this->widget(
    'Popup',
    [
        'id' => 'sendMoney',
        'view' => 'send-money'
    ]
); ?>

<?php if ($this->user()->showCV) : ?>
    <?php $this->widget(
        'Popup',
        [
            'id' => 'cv',
            'view' => 'cv',
            'user' => $this->user()
        ]
    ); ?>
<?php endif; ?>
