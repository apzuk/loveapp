<?php foreach ($ratings as $rating) : ?>
    <div class="r-item p_rel cur_p">
        <div class="rating <?= $rating->type() ?>">
            <p class="text1 ff_ssb">+ <?= $rating->count; ?></p>

            <p class="text2 ff_ssb">Рейтинг от игры</p>
        </div>
        <time><?= date("d.m.Y", strtotime($rating->create_ts)); ?></time>
    </div>
<?php endforeach; ?>