<?php $class = $notification->status == 'pending' ? 'new' : 'item'; ?>

<div class="n-item <?= $notification->ownerUser->gender(); ?> <?= $class ?> p_rel"
     onmouseover="sD.changeNotificationStatus(<?= $notification->id; ?>, this)">
    <div class="f_left wrapper1 cur_p">
        <div class="f_left" onclick="sD.profile(<?= $notification->ownerUser->user_id; ?>)">
            <img src="<?= $notification->ownerUser->avatar(); ?>" width="35" height="35"/>
        </div>
        <div class="f_left m2">
            <label class="bold">
                <a href="#" onclick="sD.profile(<?= $notification->ownerUser->user_id; ?>)">
                    <?= $notification->ownerUser->username ?>
                </a>
            </label>
            <label><?= $notification->tmplText(); ?></label>
        </div>
    </div>

    <time><?= $notification->date(); ?></time>
    <a href="javascript:;" class="ico ico5" onclick="sD.deleteNotification(<?= $notification->id; ?>, this);"></a>
</div>