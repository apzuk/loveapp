<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="/css/style.css" type="text/css"/>

    <title></title>
</head>
<body>

<div class="p_loading">
    <div class="box">
        <div class="rotate_anime">
            <img src="/images/p_ico1.png"/>
        </div>

        <div>
            <p class="ff_ssb load_components">Загрузка скриптов...</p>
        </div>
    </div>
</div>

<div id="wrapper" class="m_c p_rel">

    <header>
        <menu id="menu">
            <div class="menu_b mi_item_1 f_left m_r_1 current  p_rel">
                <span class="ico d_block m_12_17"></span>
            </div>
            <div class="menu_b mi_item_2 f_left m_r_1  p_rel">
                <span class="ico d_block m_14_10"></span>
                <span class="num num_1 p_abs ff_ese circle al_center">8</span>
            </div>
            <div class="menu_b mi_item_3 f_left p_rel">
                <span class="ico d_block m_12_5"></span>
                <span class="num num_2 p_abs ff_ese circle al_center">8</span>
            </div>

            <div class="f_right male u_profile p_rel">
                <div class="radius_4 male brd brd_3 skin_1 f_left vip p_rel">
                    <img src="/images/boy.png" class="radius_4"/>

                    <div class="vip-btn al_center radius_2 popup_link" data-dialog="#become-vip">Стать vip</div>
                </div>

                <div class="f_left ml_5">
                    <div class="mb_3">
                        <p class="font_13">Арам Петросян</p>
                    </div>
                    <div class="clearfix"></div>

                    <div>
                        <p class="val val1 font_13 f_left ff_ssb">1258</p>

                        <p class="font_11 f_left p_top5">рейтинг</p>
                    </div>
                    <div class="clearfix"></div>

                    <div>
                        <p class="val val2 font_13 f_left ff_ssb cur_p">
                            <a href="#" class="popup_link" data-dialog="#wallet">1258</a>
                        </p>

                        <p class="font_11 f_left p_top5">кредитов</p>
                    </div>
                    <div class="clearfix"></div>

                    <div>
                        <p class="val val3 font_13 f_left ff_ssb cur_p">
                            <a href="#" title="Дата окончания VIP" class="hasToolTip popup_link"
                               data-dialog="#settings">02,
                                май</a>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </menu>
    </header>

    <section class="p_rel">
        <div class="g_p">
            <div id="leader-label" class="stroke">Лидер вашего города</div>

            <div class="block5 male">
                <div class="f_left">
                    <div class="male radius_3 brd brd_3 skin_1 f_left vip p_rel cur_p user-block">
                        <span class="trans2 radius_4 hover skin_1"></span>
                        <img src="/images/boy.png" alt="">
                    </div>

                    <div class="labels al_left">
                        <div class="label1">Алла, 21 год</div>
                        <div class="label2">Санкт Петербург</div>
                    </div>
                </div>
                <div class="f_left">
                    <div class="item btn_hov coffee">Пригласить на кофе</div>
                    <div class="item btn_hov walk">Прогуляться</div>
                    <div class="item btn_hov meet">Встретиться</div>
                    <div class="item btn_hov skip">Пропустить</div>
                </div>
            </div>

            <div class="block1">
                <a class="btn1 d_block trans1 cur_p p_rel f_left">
                    <p class="v_text p_abs ff_ssb popup_link" data-dialog="#to-users-list-confirmation">Попасть сюда</p>
                </a>

                <div class="p_abs block2">
                    <div class="extra-big">
                        <div class="male radius_4 brd brd_3 skin_2 f_left vip p_rel cur_p block3">
                            <span class="trans2 radius_4 hover skin_2"></span>
                            <img src="/images/boy.png" class="radius_4">
                        </div>
                        <div class="male radius_4 brd brd_3 skin_1 f_left vip p_rel cur_p block3">
                            <span class="trans2 radius_4 hover skin_1"></span>
                            <img src="/images/boy.png" class="radius_4">
                        </div>
                        <div class="male radius_4 brd brd_3 skin_3 f_left vip p_rel cur_p block3">
                            <span class="trans2 radius_4 hover skin_3"></span>
                            <img src="/images/boy.png" class="radius_4">
                        </div>
                    </div>
                </div>
            </div>

            <div class="d_none">
                <div id="dialog" class="jq-ui-dlg" data-width="550" data-height="450">
                    Текст контент
                </div>

                <?php $this->renderPartial('//shared/popups', []); ?>
            </div>
        </div>
        <div class="g_p d_none">
            <?php $this->renderPartial('//messages/index', []); ?>
        </div>
        <div class="g_p d_none n">
            <div class="f_left">

                <div id="all-notifications" class="stroke font_17">Все уведомления</div>

                <div class="jsp nfs">
                    <?php $this->renderPartial('//notifications/_notif', ['class' => 'new', 'gender' => 'female']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => 'new', 'gender' => 'male']); ?>
                    <div class="height1"></div>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                    <?php $this->renderPartial('//notifications/_notif', ['class' => '', 'gender' => 'male']); ?>
                </div>
            </div>
            <div class="f_left">
                <div id="rating-history" class="stroke font_17">История рейтинга</div>
                <div class="ratings jsp">
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>          
                        <time>21.11.2012</time>          
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div> 
                        <time>21.11.2013</time>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>    
                        <time>21.11.2014</time>                     
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>    

                        <time>21.08.2015</time>                   
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating positive">
                            <p class="text1 ff_ssb">+ 400</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                    <div class="r-item p_rel">
                        <div class="rating negative">
                            <p class="text1 ff_ssb">- 200</p>
                            <p class="text2 ff_ssb">Рейтинг от игры</p>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>

    </section>

    <footer>
        <ul>
            <?php foreach (Footer::getItems() as $label => $item) : ?>
                <li class="<?= $item['class'] ?> hasToolTip" title="<?= $label ?>"></li>
            <?php endforeach; ?>
        </ul>
    </footer>
</div>

<script>
    window.onload = function () {
        $(".load_components").html("Загрузка компонентов... <span>0</span>%");

        var i = 0;
        var interval = setInterval(function () {
            $(".load_components span").text(i);

            if (i >= 100) {
                clearInterval(interval);
                $(document).trigger('loadingFinished');
            }
            i += 25;
        }, 10);
    }
</script>
</body>
</html>