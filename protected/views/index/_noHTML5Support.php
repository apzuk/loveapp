<?php $isIE = preg_match('/(?i)msie [1-8]/', $_SERVER['HTTP_USER_AGENT']) ?>

    <div id="no-support"
         style="display:<?= $isIE ? 'block' : 'none'; ?>; z-index: 1000000; background: url(/images/html5Support/bg.png) -1px 0 #EB99BA;position: absolute;top: 0;left: 0;right: 0;bottom: 0;background-size: 100%;">
        <style>
            #no-support .sd-btn:hover {
                text-decoration: none;
                text-align: center;
                padding-top: 8px;
            }

            #no-support .sd-btn {
                width: 100%;
                height: 32px;
                position: absolute;
                bottom: -3px;
                left: 0;
                font-size: 12px;
                font-weight: 700;
                outline: none;
                padding-top: 8px;;
            }

            #no-support .ico-chrome {
                width: 117px;
                height: 126px;
                background: url(/images/html5Support/graphics.png);
                display: block;
                background-position: -62px -103px;
                margin: 36px 144px
            }

            .html5-ico {
                background: url(/images/html5Support/graphics.png)
            }

            .ico-no-html-heart-left {
                background-position: -6px -1px;
                width: 110px;
                height: 104px;
            }

            .ico-no-html-heart-right {
                background-position: -133px -1px;
                width: 110px;
                height: 104px;
            }

            .ico-no-html-heart-left {
                display: block;
                position: absolute;
                left: -84px;
                top: -99px;
            }

            .ico-no-html-heart-right {
                display: block;
                position: absolute;
                right: -86px;
                top: -99px;
            }
        </style>

        <div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front sd-box" tabindex="-1" role="dialog"
             aria-describedby="popup" aria-labelledby="ui-id-1"
             style="border-style: solid none none; border-top-width: 1px; border-top-color: rgb(204, 204, 204); padding: 0px; height: auto; width: 407px; top: 226px; left: 194px; display: block;">
            <div class="sd-wrapper female">
                <div id="popup" class="ui-dialog-content ui-widget-content"
                     style="padding: 0px; overflow: hidden; width: auto; min-height: 0px; max-height: none; height: 325px;">
                    <a
                        class="download-chrome sd-btn al_center" href="https://www.google.ru/intl/ru/chrome/browser/"
                        target="_blank"
                        value="Сказать Google Chrome">Сказать Google Chrome</a><a
                        class="html5-ico ico-no-html-heart-left"></a><a class="html5-ico ico-no-html-heart-right"></a>

                    <p class="text" style="padding: 18px 33px 0;">
                        К сожалению, браузер, используемый вами в настоящий момент не поддерживает язык HTML5 или иные
                        современные
                        технологии. <br/><br/>
                        Скачайте и установите новый браузер.
                    </p>
                    <span class="ico-chrome"></span></div>
            </div>
        </div>
    </div>

<?php if ($isIE) :
    echo '</body></html>';
    exit;
endif; ?>