<div style="width: 350px; text-align: center;">
    Это шаблон детка!
    <hr/>
    <br/>

    <div></div>
</div>


<script src="/js/plugins/jquery-2.1.1.min.js"></script>
<script src="/js/plugins/jquery.tmpl.min.js"></script>


<script id="tmpl" type="text/template">
    {{each(index, value) names}}
    <div>Привет ${value.name}!</div>
    {{/each}}
</script>

<script>
    $(function () {
        $("#tmpl").tmpl({
            names: {
                1: {name: "мир"},
                2: {name: "суки"}
            }
        }).appendTo($("div>div").empty());
    });
</script>