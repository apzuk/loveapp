<div class="awards aw<?= $award->index(); ?>">
    <figure class="award <?= $award->getClass(); ?>" title="<?= $award->getTooltipTitle() ?>"
            onclick="sD.showAward(<?= $award->index(); ?>);">
        <figcaption></figcaption>
        <p class="award_label"><?= $award->getTitle(); ?></p>

        <div class="process">
            <div class="process_bar" style="width: <?= $p = $award->percentage(); ?>%"></div>
        </div>

        <div class="percentage">
            <?php if ($p < 100) : ?>
                <?= $p ?>%
            <?php endif; ?>
        </div>
    </figure>
</div>