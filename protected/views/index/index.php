<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">

    <link rel="stylesheet" href="/css/style.css" type="text/css"/>

    <title></title>
</head>
<body>

<?php $this->renderPartial('_noHTML5Support'); ?>

<div class="p_loading">
    <div class="box">
        <div class="loading_anim">
            <img src="/images/p_ico1.png"/>

            <div id="loading_process">
                <img src="/images/p_ico1_1.png"/>
            </div>
        </div>

        <div>
            <p class="ff_ssb load_components">Загрузка скриптов...</p>
        </div>
    </div>
</div>

<div id="wrapper" class="m_c p_rel">

    <header>
        <menu id="menu">
            <div class="menu_b mi_item_1 f_left m_r_1 current  p_rel">
                <span class="ico d_block m_12_17"></span>
                <time>2:00</time>
            </div>
            <div class="menu_b mi_item_2 f_left m_r_1  p_rel">
                <span class="ico d_block m_14_10"></span>
            <span class="num num_1 p_abs ff_ese circle al_center count<?= $messagesCount; ?>">
                <?= $messagesCount <= 99 ? $messagesCount : '99'; ?>
            </span>
            </div>
            <div class="menu_b mi_item_3 f_left p_rel">
                <span class="ico d_block m_12_5"></span>
            <span class="num num_2 p_abs ff_ese circle al_center count<?= $notificationCount; ?>">
                <?= $notificationCount; ?>
            </span>
            </div>
            <div class="menu_b mi_item_4 f_left p_rel">
                <span class="ico d_block m_12_5"></span>
            </div>

            <div class="f_right <?= $this->user()->gender(); ?> u_profile p_rel">
                <?php $class = ($this->user()->isVip() ? 'vip' : '') . ' user_' . $this->user()->user_id; ?>
                <div class="radius_4 <?= $this->user()->gender(); ?> brd brd_3 skin_<?= $this->user()->skin(
                ) ?> f_left <?= $class ?> p_rel">
                    <img src="<?= $this->user()->avatar(); ?>" onclick="sD.profile(<?= $this->user()->user_id ?>)"
                         class="radius_4 cur_p" width="83" height="83"/>

                    <?php $class = $this->user()->isVip() ? 'd_none' : ''; ?>
                    <div class="vip-btn al_center radius_2 popup_link <?= $class ?>" data-dialog="#become-vip">Стать vip
                    </div>
                </div>

                <div class="f_left ml_5 male">
                    <div class="mb_3">
                        <p class="font_13 to1 username_label"><?= $this->user()->publicData()['firstname']; ?> <span
                                class="d_none">(0)</span></p>
                    </div>
                    <div class="clearfix"></div>

                    <div>
                        <p class="val val1 font_13 f_left ff_ssb" id="rating"><?= $this->user()->stat()->rating; ?></p>

                        <p class="font_11 f_left p_top5">рейтинг</p>
                    </div>
                    <div class="clearfix"></div>

                    <div>
                        <p class="val val2 font_13 f_left ff_ssb cur_p popup_link" data-dialog="#wallet">
                            <a href="#" id="my_balance"><?= $this->user()->balance(); ?></a>
                        </p>

                        <p class="font_11 f_left p_top5 popup_link  " data-dialog="#wallet">кредитов</p>
                    </div>
                    <div class="clearfix"></div>

                    <?php $class = !$this->user()->isVip() ? 'd_none' : 'vip-settings'; ?>
                    <div class="<?= $class ?> vip-end-block">
                        <p class="val val3 font_12_1 f_left  cur_p">
                            <a href="#" title="Дата окончания VIP" class="hasToolTip popup_link "
                               data-dialog="#settings">
                                <?= Format::date($this->user()->vip_end, "dd, MMMM") ?>
                            </a>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </menu>
    </header>

    <section class="p_rel">
        <div class="g_p">
            <div id="leader-label" class="t_shd1 cur_d">Лидер вашего города</div>

            <div id="randomUsers">
                <canvas width="300" class="xleft" height="250" id="tagCloud1" style="left: -25px;">
                    <ul></ul>
                </canvas>

                <canvas width="300" class="xleft" height="250" id="tagCloud2" style="left: 212px;">
                    <ul></ul>
                </canvas>
            </div>

            <div class="block5 male">

            </div>

            <div class="block1">
                <a class="btn1 d_block trans1 cur_p p_rel f_left">
                    <p class="v_text p_abs ff_ssb popup_link" data-dialog="#to-users-list-confirmation">попасть сюда</p>
                </a>

                <div class="p_abs block2">
                    <div class="extra-big">

                    </div>
                </div>
            </div>

            <div class="d_none">
                <div id="dialog" class="jq-ui-dlg" data-width="550" data-height="450">
                    Текст контент
                </div>

                <?php $this->renderPartial('//shared/popups', []); ?>
                <?php $this->renderPartial('//shared/templates', []); ?>
            </div>
        </div>
        <div class="g_p g_p1 d_none">
            <?php $this->renderPartial('//messages/index', ['messages' => $messages]); ?>
        </div>
        <div class="g_p d_none n">
            <div class="f_left">

                <div id="all-notifications" class="stroke font_17">Все уведомления</div>

                <div class="jsp nfs">
                    <?php $this->renderPartial(
                        '//notifications/_notificationsList',
                        [
                            'notifications' => $notifications,
                        ]
                    ); ?>
                </div>
            </div>
            <div class="f_left">
                <div id="rating-history" class="stroke font_17">История рейтинга</div>
                <div class="ratings jsp">
                    <?php $this->renderPartial(
                        '//notifications/_ratingsList',
                        [
                            'ratings' => $ratings
                        ]
                    ); ?>
                </div>
            </div>
        </div>
        <div class="g_p g_p2 d_none">
            <?php $this->renderPartial("//rating/index"); ?>
        </div>

        <div id="effect1" class="d_none"></div>

        <div id="game_page"></div>

        <input type="hidden" id="city_id" value="<?= $this->user()->city_id; ?>"/>

        <aside class="awards my male jsp">
            <p class="t_shd1 cur_d">Мои награды</p>

            <?php $this->renderPartial('_awards', ['awards' => $award->getAwards()]); ?>
            <?php $this->renderPartial('_awardsNotifications', ['awards' => $this->user()->awards]); ?>

        </aside>

        <aside class="suggestion-users">
            <?php $this->renderPartial('//messages/_sidebarRight'); ?>
        </aside>
    </section>

    <footer>
        <ul>
            <?php foreach (Footer::getItems() as $label => $item) : ?>
                <li class="<?= $item['class'] ?> hasToolTip" title="<?= $label ?>"></li>
            <?php endforeach; ?>
        </ul>
    </footer>
</div>

<script>
    window.onload = function () {
        $(".load_components").html("Загрузка компонентов... <span>0</span>%");

        var i = 0;
        var interval = setInterval(function () {
            $(".load_components span").text(i);

            if (i >= 100) {
                clearInterval(interval);
                $(document).trigger('loadingFinished');
            }
            i += 25;
        }, 10);
    };

    var user_id = <?= (int)$this->user()->user_id; ?>,
        avatar = '<?= $this->user()->avatar(); ?>',
        skin = '<?= $this->user()->skin(); ?>',
        sid = "<?= $this->user()->sid; ?>",
        gender = "<?= $this->user()->gender(); ?>",
        username = "<?= $this->user()->username; ?>",
        cityname = "<?= $this->user()->city->city; ?>",
        genderOpposite = "<?= $this->user()->oppositeGender(); ?>";
</script>
</body>
</html>