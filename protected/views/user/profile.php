<div class="wrapper">
    <div class="f_left <?= $user->gender(); ?>">
        <div class="avatar <?= $user->gender(); ?> brd brd_3 skin_1 f_left p_rel mosaic-block bar">
            <?php if ($user->user_id == $this->user()->user_id) : ?>
                <div class="mosaic-overlay">
                    <div class="details">
                        <a href="#" class="tool tool-item1 edit p_rel" onclick="sD.editProfileForm();">Редактировать</a>
                        <a href="#" class="tool tool-item2 vk-avatar p_rel" onclick="sD.setVKAvatar();">Поставить аватар ВК</a>
                    </div>
                </div>
            <?php endif; ?>

            <div class="mosaic-backdrop">
                <figure class="kisses ico hasToolTip" title="Поцелуи">
                    <span><?= $user->stat->kiss; ?></span>
                </figure>
                <img src="<?= $user->avatar_orig(); ?>"/>
            </div>

            <div class="online_status <?= $user->online ? 'online' : 'offline' ?> hasToolTip" title="Пользователь <?= $user->online ? 'online' : 'offline' ?>"></div>
        </div>

        <?php if ($user->user_id != $this->user()->user_id) : ?>
            <?php $this->renderPartial('//user/_operations', ['user' => $user]); ?>
        <?php endif; ?>
        <?php $this->renderPartial('//user/_menu', ['user' => $user]); ?>
    </div>
    <?php $this->renderPartial('//user/_patron', ['user' => $user]); ?>
    <?php $this->renderPartial('//user/_form', ['user' => $user]); ?>
</div>