<div class="menu f_left cur_d">
    <div class="item p_rel btn_hov">
        <p class="like ff_ssb font_13 al_left">Симпатии</p>
        <span class="count ff_ssb"><?= $user->stat->likes; ?></span>
    </div>
    <div class="item p_rel btn_hov">
        <p class="retinues ff_ssb font_13 al_left">Свиты</p>
        <span class="count ff_ssb"><?= $user->stat->retinue_count; ?></span>
    </div>
    <div class="item p_rel btn_hov">
        <p class="rating ff_ssb font_13 al_left">Рейтинг</p>
        <span class="count ff_ssb"><?= $user->stat->rating; ?></span>
    </div>
    <div class="item p_rel btn_hov">
        <p class="games ff_ssb font_13 al_left">Кол.-во игр</p>
        <span class="count ff_ssb"><?= $user->stat->game_count; ?></span>
    </div>
</div>