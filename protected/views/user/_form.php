<div id="form" class="f_left" style="display: none;">
    <div class="flash flash-error d_none">
        <p>Сообщение об ошибке</p>
    </div>

    <div class="form-box <?= $user->gender(); ?>">
        <div class="form-row">
            <label class="d_block">Имя и фамилия</label>
            <?= CHtml::activeTextField($user, 'username', ['class' => 'from-input username']); ?>
        </div>
        <div class="form-row">
            <label class="d_block">Дата рождения</label>

            <div class="birth_date">
                <input type="text" class="day" value="<?= date('d', strtotime($user->birth_date)) ?>"/>
                <input type="text" class="month" value="<?= date('m', strtotime($user->birth_date)) ?>"/>
                <input type="text" class="year" value="<?= date('Y', strtotime($user->birth_date)) ?>"/>
            </div>
        </div>
        <div class="form-row clearfix flags">
            <label class="d_block">Страна</label>
            <?php foreach (SysCountries::model()->cache(60 * 60 * 5)->findAll('important=1') as $country) : ?>
                <div class="flag <?= strtolower($country->short); ?> f_left cur_p country<?= $country->country_id; ?>"
                     title="<?= $country->name; ?>">
                    <img src="/images/flags/<?= $country->image(); ?>"/>
                    <input type="hidden" value="<?= $country->country_id ?>"/>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="form-row">
            <label class="d_block">Город</label>
            <select class="cityIdSelected">
                <option>Ереван</option>
                <option>Моска</option>
                <option>Ереван</option>
            </select>
        </div>

        <input type="hidden" id="userCountryId" value="<?= $this->user()->city->country->country_id; ?>"/>
    </div>

</div>