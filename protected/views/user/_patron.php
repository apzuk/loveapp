<div class="f_left" id="patron">
    <div class="patron-box p_rel">
        <div class="patron" title="Покровитель">
            <?php if ($user->sponsor) : ?>
                <img class="patron_img cur_p" onclick="sD.profile(<?php echo $user->sponsor->user->user_id ?>)"
                     src="<?= $user->sponsor->user->avatar(); ?>"/>
            <?php endif; ?>

        </div>

        <div class="info-bar f_left">
            <p class="p1 t_shd1 bold cur_d">Мой покровитель</p>

            <p class="cur_d">
                Покровитель является лучшим другом, который не только радует вас превосходным общением, но и одаряет
                прекрасными подарками, ублажает поцелуями, ну и, конечно же больше всего вам симпатизирует.
            </p>

            <span class="cost hasToolTip" title="Стоимость"><?= $user->cost * 10 ?></span>

            <?php if (!$user->sponsor && $user->user_id != $this->user()->user_id) : ?>
                <a href="#" class="btn3" onclick="sD.becomePatron(<?= $user->user_id ?>, this); return false;">Стать
                    покровителем</a>
            <?php endif; ?>
        </div>
    </div>

    <div class="retinues-box p_rel jsp">

        <?php if ($user->retinues) : ?>
            <p class="p2 t_shd1 al_center bold cur_d">Мои свиты</p>

            <?php foreach ($user->retinues as $r) : ?>
                <?php if (!$r->target) : continue; endif; ?>

                <figure class="p_rel f_left">
                    <img class="cur_p btn_hov" src="<?= $r->target->avatar(); ?>"
                         onclick="sD.profile(<?= $r->target->user_id; ?>)"/>
                    <span class="txt1"><?= $r->target->publicData()['firstname']; ?></span>
                </figure>
            <?php endforeach; ?>
        <?php else : ?>
            <p class="p2 t_shd1 al_center bold p3 cur_d">У пользователя нет ни одной свиты :( </p>
        <?php endif; ?>
    </div>


    <div class="op p_rel">
        <a href="<?= $user->socProfile(); ?>" target="_blank" class="vk_icon hasToolTip" title="Профиль ВК"></a>

        <?php if ($user->user_id != $this->user()->user_id) : ?>
            <span class="send-vip-status cur_p hasToolTip popup_link" data-dialog="#become-vip" title="Передать VIP статус"></span>
            <span class="send-money cur_p hasToolTip" title="Передать монеты" onclick="sD.sendMoney();"></span>
        <?php endif; ?>
    </div>

</div>