<div class="buttons operations">
    <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Написать сообщение"
         onclick="IM.compose(<?= $user->user_id ?>); return;">
        <span class="b1 b f_left"></span>
    </div>
    <?php if ($user->liked) : ?>
        <div class="d_block f_left p_rel cur_p btn_hov active hasToolTip" title="Вы уже проявили симпатию">
            <span class="b2 b f_left"></span>
        </div>
    <?php else : ?>
        <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Проявить симпатию"
             onclick="sD.like(<?= $user->user_id ?>, this);">
            <span class="b2 b f_left"></span>
        </div>
    <?php endif; ?>
    <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Поцеливать"
         onclick="sD.kiss(<?= $user->user_id ?>, this)">
        <span class="b3 b f_left"></span>
    </div>
</div>