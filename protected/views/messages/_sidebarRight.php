<?php $users = UserAccount::model()->order('RAND()', 'ASC')->limit(6)->findAll(); ?>
<?php if (!app()->request->isAjaxRequest) : ?>
    <p class="t_shd1 cur_d">Хотят общаться</p>
<?php endif; ?>

    <table id="suggested-users">
        <?php foreach (array_chunk($users, 2) as $row): ?>
            <tr>
                <?php foreach ($row as $r) : ?>
                    <td>
                        <div class="<?= $r->gender(); ?> item" data-id="<?= $r->user_id ?>">
                            <img class="u-avatar" src="<?= $r->avatar() ?>"/>

                            <?php $data = $r->publicData(); ?>
                            <span class="uname"><?= $data['firstname']; ?> , <?= $r->age() ?> </span>
                        </div>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </table>

<?php if (!app()->request->isAjaxRequest) : ?>
    <script id="userInfoBlock" type="text/template">
        <figure class="user-info-block ${gender}">
            <div class="avatar">
                <a href="#" onclick="sD.profile(${user_id})">
                    <img src="${avatar}"/>
                </a>
            </div>

            <div class="buttons operations">
                <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Написать сообщение"
                     onclick="IM.compose(${user_id}); return;">
                    <span class="b1 b f_left"></span>
                </div>
                <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Проявить симпатию"
                     onclick="sD.like(${user_id}, this);">
                    <span class="b2 b f_left"></span>
                </div>
                <div class="d_block f_left p_rel cur_p btn_hov hasToolTip" title="Поцеливать"
                     onclick="sD.kiss(${user_id}, this)">
                    <span class="b3 b f_left"></span>
                </div>
            </div>
        </figure>
    </script>
<?php endif; ?>