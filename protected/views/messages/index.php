<div class="messages " id="messages">
    <div class="f_left jsp sidebar">
        <?php $this->renderPartial('//messages/_sidebar', ['messages' => $messages]) ?>
    </div>
    <div class="f_left ">

        <div class="dialogs">
            <?php ImMessage::icons(); ?>
        </div>

        <div class="textarea-box male p_rel d_none">
            <div class="ta selectable" id="ta_box" contenteditable="true"></div>

            <div class="icons">
                <?php foreach (ImMessage::icons() as $icon) : ?>
                    <a href="#" class="f_left" onclick="return false;">
                        <img src="/images/vk/<?= $icon[0]->object ?>"
                             onclick="emojic.showEmotics(this); return false;"/>
                    </a>
                <?php endforeach; ?>
            </div>

            <a href="#" class="btn3" onclick="IM.send(); return false;">Отправить</a>
            <a href="#" class="<?= !$this->user()->isVip() ? 'd_none' : '' ?> ico ico-upload btn_hov popup_link"
               data-dialog="#upload-photo"></a>

            <div class="image-container btn3 p_rel d_none"></div>

            <input type="hidden" id="attachment"/>

            <div class="overlay-cover">
                <p>Выберите собеседника</p>
            </div>
        </div>
    </div>
</div>