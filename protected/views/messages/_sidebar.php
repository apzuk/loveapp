<?php foreach ($messages as $message) : ?>
    <figure class="<?= $message->companion()->gender();?> p_rel ls_user ls_user_<?= $message->companion()->user_id; ?> cur_p"
            onclick="IM.select(<?= $message->companion()->user_id; ?>)"
            data-userid="<?= $message->companion()->user_id; ?>">
        <div class="avatar radius_5 brd brd_3 skin_<?= $message->companion()->skin(); ?>">
            <img src="<?= $message->companion()->avatar(); ?>"/>
        </div>
        <div class="labels">
            <label class="cur_p"><?= $message->companion()->publicData()['firstname']; ?></label>
            <label class="cur_p"><?= $message->companion()->title(false); ?></label>
        </div>
        <div class="unread-num circle blink count0"></div>
    </figure>
<?php endforeach; ?>

<script id="lsUserTmpl" type="text/template">
    <figure class="${gender} p_rel ls_user ls_user_${user_id} cur_p"
            onclick="IM.select(${user_id})"
            data-userid="${user_id}">
        <div class="avatar ">
            <img class="brd brd_3 skin_${skin}" src="${avatar}"/>
        </div>
        <div class="labels">
            <label class="cur_p">${username}</label>
            <label class="cur_p">${title}</label>
        </div>
        <div class="unread-num circle blink count0"></div>
    </figure>
</script>