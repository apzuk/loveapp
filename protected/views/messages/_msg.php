<?php $user = isset($user) ? $user : $msg->senderUser; ?>

<?php $activeClass = $msg->receiver_status == 'unread' ? 'active' : ''; ?>
<div class="<?= $user->gender(); ?> <?= $activeClass ?> msg-item p_rel" id="msg_<?= $msg->message_id ?>"
     onmouseover="IM.makeAsRead(<?= $msg->message_id ?>, this);" data-owner="<?= $user->user_id ?>">
    <div class="avatar">
        <img src="<?= $user->avatar(); ?>" class="<?= $user->gender(); ?> skin_1 brd_2"/>
    </div>

    <p class="text1">
        <a href="#" onclick="sD.profile(<?= $user->user_id ?>);">
            <?= $user->username; ?>
        </a>
    </p>

    <p class="text2"><?= $msg->message(); ?></p>

    <?php if ($attachment = json_decode($msg->attachment, true)) : ?>
        <?php foreach ($attachment as $att) : ?>
            <?php if (!$att) : continue; endif; ?>
            <img src="/uploads/msg/<?= Files::resize($att, 150, 150); ?>" class="g cur_p"
                 onclick="IM.showOriginal('<?= $att ?>')"/>
        <?php endforeach; ?>
    <?php endif; ?>

    <time><?= $msg->date(); ?></time>
    <a href="javascript:;" class="ico ico5" onclick="IM.deleteMsg(<?= $msg->message_id ?>, this);"></a>
</div>