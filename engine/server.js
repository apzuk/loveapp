// Modules!
var http = require('http');
var sockjs = require('sockjs');

sys = require('sys');
exec = require('child_process').exec;
mysql = require('mysql'); // mysql module
util = require('util'); // utils
md5 = require('MD5'); // MD5 crypt module
_ = require('underscore'); // underscore module
users = require('users'); // users module
game = require('engine'); // users module

// Configuration
var isLocal = require('./config/isLocal');
if (isLocal === true) {
    config = require('./config/localConfig');
} else {
    config = require('./config/remoteConfig');
}

// Init mysql connection!
connection = mysql.createConnection(config.db);
connection.connect(function (err) {
    if (err) {
        throw "Mysql connection error: " + err;
    }
});
pool = mysql.createPool(config.db);

robots = require('robot'); // robots module

// Start socket listening
var engine = sockjs.createServer(config.server_opts);
engine.on('connection', function (conn) {
    conn.on('data', function (message) {
        var data;

        try {
            data = JSON.parse(message);
        } catch (e) {
            return log('Request parsing error: ', e);
        }

        if (data.method == 'connect') {
            users.connect(data.user_id, data.sid, conn, function (user) {
                users.write(data.user_id, {'connected': true});

                game.find({user_id: data.user_id}, function (table) {
                    if (!table.isPlaying()) {
                        var struct = table.struct(),
                            gameInfo = _.extend({}, struct, {module: 'game', method: "setMyTable"});

                        users.write(data.user_id, gameInfo);
                    } else {
                        table.notifyCurrentStepToUser(data.user_id);
                    }

                }, true);
            });
        }

        else {
            if (!users.identify(data.user_id, data.sid)) {
                //return;
            }

            call(data);
        }

        return true;
    });
    conn.on('close', function () {
        users.disconnect(conn.id);
    });
});

var server = http.createServer();
engine.installHandlers(server, {prefix: '/engine'});
server.listen(config.port, config.host);

// logger
log = function (m, e) {
    e = (e) ? e : '';
    console.log(m, e);
};

// http server
var querystring = require('querystring');
http.createServer(function (req, res) {
    var data = querystring.parse(req.url.replace('/?', ''));
    users[data.method](data);

    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('post received');

}).listen(9615);

function call(data) {
    switch (data.module) {
        case 'users':
            log('Module: users called');
            if (typeof(users[data.method]) != 'undefined') {
                return users[data.method](data);
            }
            break;
        case 'game':
            log('Module: engine called');
            if (typeof(game[data.method]) != 'undefined') {
                return game[data.method](data);
            }
            break;
        default :
            break;
    }

    return true;
}