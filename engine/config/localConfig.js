module.exports = {
    port: 8088,
    host: '0.0.0.0',

    server_opts: {
        sockjs_url: 'http://localhost:8080/lib/sockjs.js',
        websocket: true
    },

    db: {
        host: '10.0.0.105',
        user: 'root',
        password: 'prch123',
        database: 'loveapp'
    }
};
